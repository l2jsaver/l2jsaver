package l2jsaver;

import l2jsaver.Features.IssleMisc.VoicedCommands;
import l2jsaver.Features.IssleMisc.StoreSystem.StoreController;
import l2jsaver.Features.StatSystem.RewardController;
import l2jsaver.Features.StatSystem.SpartanInstance;
import l2jsaver.Features.StatSystem.UnlocksHolder;
import l2jsaver.Machine.utils.ReloadHandler;
import l2jsaver.Machine.utils.ScriptLoader;

public class SaverAdapter 
{
	
	public static void load()
	{
		ExtensionsAdapter.load();
		loadController();
		loadVoiceCommands();
		loadNpcBypasses();	
	
		try 
		{
			ScriptLoader.getInstance().executeScriptList("", true);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	/** Loading Controllers */
	public static void loadController()
	{
		SpartanInstance.migrate();
		StoreController.load();
		RewardController.load();
		VoicedCommands.getInstance();
	}
	
	/** Loading Voiced Commands */
	public static void loadVoiceCommands()
	{
		SaverCommandHandler.getInstance().registerHandler(UnlocksHolder.getInstance());
		SaverCommandHandler.getInstance().registerHandler(VoicedCommands.getInstance());
		SaverCommandHandler.getInstance().registerHandler(new ReloadHandler());
	}

	public static void loadNpcBypasses()
	{
	}
}