/*
 * Authors: Issle, Howler, Matim
 * File: pvpInstance.java
 */
package l2jsaver.Features.StatSystem;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.l2jmobius.commons.database.DatabaseFactory;

/**
 * @author Issle
 *
 */
public class SpartanInstance {

	private int previousTargetId=0;
	private int previousSkillId=0;
	private int targetRepetition=0;
	private int skillRepetition=0;
	private int soloPoints=0;
	private int teamPoints=0;
	private int x=0;
	private int y=0;
	private int locationRepetition=0;
	
	public static final String RESTORE_SPARTAN = "SELECT soloPoints,teamPoints from Spartan where playerId=?";
	public static final String STORE_SPARTAN ="INSERT INTO Spartan (playerId, soloPoints, teamPoints) VALUES (?, ?,?) ON DUPLICATE KEY UPDATE soloPoints = ?,teamPoints=?";
	
	public static final String CREATE_SPARTAN = "CREATE TABLE IF NOT EXISTS `Spartan` " +
	"(`playerId`  int NOT NULL ," +
	"`soloPoints`  int NOT NULL DEFAULT 1," +
	"`teamPoints`  int NOT NULL DEFAULT 1," +
	"PRIMARY KEY (`playerId`));";
	
	public static void migrate()
	{
		System.out.println("[Spartan]:Migrating the database.");
		Connection con = null;
		try
		{
			con = DatabaseFactory.getConnection();
			PreparedStatement statement = con.prepareStatement(CREATE_SPARTAN);
			statement.execute();
			statement.close();
			
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			try { con.close(); } catch (Exception e) { e.printStackTrace();}
		}
		
	}
	
	/**
	 * @return the previousTargetId
	 */
	public int getPreviousTargetId() {
		return previousTargetId;
	}

	/**
	 * @param previousTargetId the previousTargetId to set
	 */
	public void setPreviousTargetId(int previousTargetId) {
		this.previousTargetId = previousTargetId;
	}

	/**
	 * @return the previousSkillId
	 */
	public int getPreviousSkillId() {
		return previousSkillId;
	}

	/**
	 * @param previousSkillId the previousSkillId to set
	 */
	public void setPreviousSkillId(int previousSkillId) {
		this.previousSkillId = previousSkillId;
	}

	/**
	 * @return the targetRepetition
	 */
	public int getTargetRepetition() {
		return targetRepetition;
	}

	/**
	 * @param targetRepetition the targetRepetition to set
	 */
	public void setTargetRepetition(int targetRepetition) {
		this.targetRepetition = targetRepetition;
	}

	/**
	 * @return the skillRepetition
	 */
	public int getSkillRepetition() {
		return skillRepetition;
	}

	/**
	 * @param skillRepetition the skillRepetition to set
	 */
	public void setSkillRepetition(int skillRepetition) {
		this.skillRepetition = skillRepetition;
	}

	/**
	 * @return the soloPoints
	 */
	public int getSoloPoints() {
		return soloPoints;
	}

	/**
	 * @param soloPoints the soloPoints to set
	 */
	public void setSoloPoints(int soloPoints) {
		this.soloPoints = soloPoints;
	}

	/**
	 * @return the teamPoints
	 */
	public int getTeamPoints() {
		return teamPoints;
	}

	/**
	 * @param teamPoints the teamPoints to set
	 */
	public void setTeamPoints(int teamPoints) {
		this.teamPoints = teamPoints;
	}

	/**
	 * General purpose constructor.
	 * @param objectId
	 */
	public SpartanInstance(int objectId)
	{
		loadData(objectId);
	}
	
	public void loadData(int playerId)
	{
		Connection con = null;
		try
		{
			con = DatabaseFactory.getConnection();
			
			PreparedStatement statement = con.prepareStatement(RESTORE_SPARTAN);
			statement.setInt(1, playerId);
			ResultSet rset = statement.executeQuery();
			while(rset.next())
			{
				soloPoints =rset.getInt("soloPoints");
				teamPoints =rset.getInt("teamPoints");
			}
			
			rset.close();
			statement.close();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			try { con.close(); } catch (Exception e) {e.printStackTrace();}
		}
	}
	
	public void saveData(int objectId)
	{
		Connection con = null;
		try
		{
			con = DatabaseFactory.getConnection();
			PreparedStatement statement;
			String stat = STORE_SPARTAN;
			
			statement = con.prepareStatement(stat);
			statement.setInt(1, objectId);
			statement.setInt(2, soloPoints);
			statement.setInt(3, teamPoints);
			statement.setInt(4, soloPoints);
			statement.setInt(5, teamPoints);
			
			statement.execute();
			statement.close();
			
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			try { con.close(); } catch (Exception e) { e.printStackTrace();}
		}
	}

	/**
	 * @param x the x to set
	 */
	public void setX(int x) {
		this.x = x;
	}

	/**
	 * @return the x
	 */
	public int getX() {
		return x;
	}

	/**
	 * @param y the y to set
	 */
	public void setY(int y) {
		this.y = y;
	}

	/**
	 * @return the y
	 */
	public int getY() {
		return y;
	}

	/**
	 * @param locationRepetition the locationRepetition to set
	 */
	public void setLocationRepetition(int locationRepetition) {
		this.locationRepetition = locationRepetition;
	}

	/**
	 * @return the locationRepetition
	 */
	public int getLocationRepetition() {
		return locationRepetition;
	}
	
	
}
