/*
 * Authors: Issle, Howler, Matim
 * File: AbstractUnlock.java
 */
package l2jsaver.Features.StatSystem;

import org.l2jmobius.gameserver.model.actor.instance.PlayerInstance;

import l2jsaver.Features.L2JFrame.models.L2JAlign;
import l2jsaver.Features.L2JFrame.models.L2JColor;
import l2jsaver.Features.L2JFrame.models.primitive.L2JText;
import l2jsaver.Model.PcModel;
import l2jsaver.interfaces.ISaverCommandHandler;


/**
 * @author Issle
 *
 */
public abstract class AbstractUnlock implements ISaverCommandHandler{

	public static int TYPE_SOLO = 0;
	public static int TYPE_TEAM = 1;
	
	public AbstractUnlock(String name, int experience, int type)
	{
		this.name = name;
		this.pvpExperience = experience;
		this.type = type;
	}
	
	protected String name;
	protected String[] commands;
	protected String description;
	protected int pvpExperience;
	private int type;
	
	public boolean canUse(PlayerInstance activeChar)
	{
		int points = 0;
		if(this.isSolo())
			points = PcModel.getModel(activeChar).spartanInstance.getSoloPoints();
		else
			points = PcModel.getModel(activeChar).spartanInstance.getTeamPoints();
		
		if(pvpExperience > points)
		{
			activeChar.sendMessage(name + " is locked. You need "+ String.valueOf(pvpExperience - points) +" more points to use it.");
			if(activeChar.isGM())
			{
				activeChar.sendMessage("GM-mode, can use everything even locked.");
				return true;
			}
			return false;
		}
		else
			return true;
	}
	
	public boolean canUse(PlayerInstance activeChar,boolean message)
	{
		if(activeChar.inObserverMode())
			return false;
		
		if(activeChar.isDead())
			return false;
		int points = 0;
		if(this.isSolo())
			points = PcModel.getModel(activeChar).spartanInstance.getSoloPoints();
		else
			points = PcModel.getModel(activeChar).spartanInstance.getTeamPoints();
		
		if(pvpExperience > points)
		{
			if(message)
				activeChar.sendMessage(name + " is locked. You need "+ String.valueOf(-pvpExperience + points) +" more points to use it.");
			if(activeChar.isGM())
			{
				if(message)
					activeChar.sendMessage("GM-mode, can use everything even locked.");
				return true;
			}
			return false;
		}
		else
			return true;
	}
	
	public void setTeam()
	{
		type = TYPE_TEAM;
	}
	
	public void setSolo()
	{
		type = TYPE_SOLO;
	}
	
	public boolean isSolo()
	{
		return type==TYPE_SOLO;
	}

	public abstract double getResult(PlayerInstance activeChar);
	
	@Override
	public boolean useVoicedCommand(String command, PlayerInstance activeChar, String params) 
	{
		
		return false;
	}
	
	@Override
	public boolean forGM()
	{
		return false;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the commands
	 */
	public String[] getCommands() {
		return commands;
	}

	/**
	 * @param commands the commands to set
	 */
	public void setCommands(String[] commands) {
		this.commands = commands;
	}

	/**
	 * @return the pvpExperience
	 */
	public int getPvpExperience() {
		return pvpExperience;
	}

	/**
	 * @param pvpExperience the pvpExperience to set
	 */
	public void setPvpExperience(int pvpExperience) {
		this.pvpExperience = pvpExperience;
	}

	/**
	 * @return the type
	 */
	public int getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(int type) {
		this.type = type;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String[] getVoicedCommandList() {
		// TODO Auto-generated method stub
		return commands;
	}
	
	public String getDescription()
	{
		String s = description+"<br><br>Unlocked at: "+String.valueOf(pvpExperience)+"<br><br>Voiced Commands:<br>";
		if(commands != null)
			for(String command : commands)
				s+="?"+command+"<br>";
		
		return s;
	}

	/**
	 * @param activeChar
	 * @return
	 */
	public L2JText getMini(PlayerInstance activeChar)
	{
		int points = 0;
		if(this.isSolo())
			points = PcModel.getModel(activeChar).spartanInstance.getSoloPoints();
		else
			points = PcModel.getModel(activeChar).spartanInstance.getTeamPoints();
		
		if(pvpExperience > points)
			return new L2JText(L2JColor.Red, L2JAlign.Center,"LOCKED");
		else
			return new L2JText(L2JColor.Green, L2JAlign.Center,"UNLOCKED");
	}
	
}
