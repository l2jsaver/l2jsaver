package l2jsaver.Features.StatSystem;

import l2jsaver.Features.IssleMisc.RandomTips;

import org.l2jmobius.commons.util.Rnd;
import org.l2jmobius.gameserver.data.sql.impl.ClanTable;
import org.l2jmobius.gameserver.model.actor.instance.PlayerInstance;
import org.l2jmobius.gameserver.model.clan.Clan;
import org.l2jmobius.gameserver.model.clan.ClanMember;

public class RewardController 
{
	public static int chance = 6;
	
	public static void load()
	{
		RandomTips.registerTip("If you are a clan leader, check your clan warehouse regularly. Items appear there when your clan members farm for a long time.");
		RandomTips.registerTip("If you are an ally leader, check your clan warehouse regularly. Items appear there when your ally members farm for a long time. The more members, the higher the reward.");
		RandomTips.registerTip("Using your warehouse as a clan leader you can spend the wealth in your gear, or distirbute it to clan members to help them with theirs.");
	}
	
	/**
	 * This function decides weather or not to do allow
	 * a duplication of drop. First it allows any drop
	 * to be duplicated if the looter belongs to a clan.
	 * 
	 * Secondly it allows the drop to be duplicated "again"
	 * if the owner belongs to an allied clan, but the 
	 * leading clan is not his clan. This way we prevent the
	 * leading clan from getting rewards as a clan and as an
	 * ally leader thus giving them the motive to recruit more
	 * clans instead of having a single clan with a single ally.
	 * 
	 * @param activeChar
	 * @param target
	 * @param item
	 */
	public static void addItem(PlayerInstance activeChar, int id, long count)
	{
		Clan clan = activeChar.getClan();
		
		if(clan== null)
			return;
		
		handleBonus(activeChar, id, count, clan);
		addToClanies(activeChar, id, count);
		
		if(clan.getAllyId()== 0)
			return;
		
		Clan allyLeadingClan = getLeadingClan(clan);
			
		if(allyLeadingClan == null || clan.getId()== allyLeadingClan.getId())
			return;
			
		handleBonus(activeChar, id, count, allyLeadingClan);
	}

	public static synchronized void addToClanies(PlayerInstance activeChar, int id, long count)
	{
		Clan clan = activeChar.getClan();
		
		for(ClanMember playerMember: clan.getMembers())
		{
			if(!playerMember.isOnline())
				continue;
			
			PlayerInstance player = playerMember.getPlayerInstance();
			
			if(player.getLevel()<83)
				continue;
			
			if(Rnd.get(500)<= UnlocksHolder.getInstance().getUnlock("Small-Blind").getResult(player))
			{
				playerMember.getPlayerInstance().getWarehouse().addItem("Clan reward", id, count, activeChar, null);
			}
		}
	}
	
	/**
	 * Depending on the chance, we decide weather or not
	 * to spawn the item in the clans warehouse. The chance
	 * can be thought as the percentages of a drop to be added
	 * in the clan warehouse. For example if you do a single
	 * drop of 10 iron ores, then there is a chance% chance
	 * that this drop will be duplicated and added in the clan
	 * warehouse. This way we are taking an extra percentage of
	 * the item and adding it in the clan warehouse as a bonus.
	 * 
	 * This way we prevent players from going out of the clan to farm
	 * or use clan-less characters, since we are not taxing on
	 * a drop, but instead giving it a chance to get duplicated.
	 * 
	 * @param activeChar
	 * @param item
	 * @param ownerClan
	 */
	private static void handleBonus(PlayerInstance activeChar, int id, long count, Clan ownerClan) {

		if(Rnd.get(100)>UnlocksHolder.getInstance().getUnlock("Big-Blind").getResult(activeChar))
			return;
		
		ownerClan.getWarehouse().addItem("Ally reward", id, count, activeChar, null);
	}
	
	/**
	 * Each clan belongs to an ally. Each ally has multiple
	 * clans, but only one clan leads the ally. The clan that
	 * leads the ally, is the clan that gives its id as the 
	 * allyId.
	 * 
	 * This function takes as argument a clan ( that we knows
	 * that belongs to an ally ) and returns the leading clan.
	 * We are going to use that reward the leading clan each 
	 * time an ally member farms an item.
	 * 
	 * @param clan
	 * @return L2Clan
	 */
	public static Clan getLeadingClan(Clan clan)
	{
		int allyId = clan.getAllyId();
		return ClanTable.getInstance().getClan(allyId);
	}
}