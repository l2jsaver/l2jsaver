/*
 * Authors: Issle, Howler, Matim
 * File: UnlocksHolder.java
 */
package l2jsaver.Features.StatSystem;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.l2jmobius.gameserver.model.actor.instance.PlayerInstance;

import l2jsaver.SaverCommandHandler;
import l2jsaver.Features.L2JFrame.AbstractController;
import l2jsaver.Features.L2JFrame.L2JFrame;
import l2jsaver.Features.L2JFrame.models.L2JAlign;
import l2jsaver.Features.L2JFrame.models.L2JColor;
import l2jsaver.Features.L2JFrame.models.primitive.L2JButton;
import l2jsaver.Features.L2JFrame.models.primitive.L2JButton.L2JButtonType;
import l2jsaver.Features.L2JFrame.models.primitive.L2JHorizontalPane;
import l2jsaver.Features.L2JFrame.models.primitive.L2JLink;
import l2jsaver.Features.L2JFrame.models.primitive.L2JRootPane;
import l2jsaver.Features.L2JFrame.models.primitive.L2JText;
import l2jsaver.Features.L2JFrame.models.primitive.L2JVerticalPane;
import l2jsaver.Features.StatSystem.Unlocks.HelpUnlock;
import l2jsaver.Features.StatSystem.Unlocks.Solo.*;
import l2jsaver.Features.StatSystem.Unlocks.Team.ElevationChance;
import l2jsaver.Features.StatSystem.Unlocks.Team.ElevationDamage;
import l2jsaver.Features.StatSystem.Unlocks.Team.ElevationDefense;
import l2jsaver.Model.PcModel;
import l2jsaver.interfaces.ISaverCommandHandler;

/**
 * @author Issle
 * 
 */
public class UnlocksHolder extends AbstractController implements ISaverCommandHandler {

	public static String[] commands = {"stat","stats"};
	
	private Map<String, AbstractUnlock> soloUnlocks;
	private Map<String, AbstractUnlock> teamUnlocks;

	public static int width = 295;
	public static L2JColor back1 = L2JColor.Black;
	public static L2JColor back2 = L2JColor.Grey;

	private UnlocksHolder() {
		soloUnlocks = new ConcurrentHashMap<>();
		teamUnlocks = new ConcurrentHashMap<>();
		L2JFrame.getInstance().registerListener(this);
		
		registerUnlock(new HelpUnlock());
		registerUnlock(new AllyRecall());
		registerUnlock(new BigBlindUnlock());
		registerUnlock(new SmallBlindUnlock());
		registerUnlock(new ClanRecall());
		registerUnlock(new PartyRecall());
		registerUnlock(new DoubleTrouble());
		
		registerUnlock(new ElevationDamage());
		registerUnlock(new ElevationDefense());
		registerUnlock(new ElevationChance());
		
	}
	
	public void registerUnlock(AbstractUnlock unlock) {
		if (unlock.isSolo())
			soloUnlocks.put(unlock.getName(), unlock);
		else
			teamUnlocks.put(unlock.getName(), unlock);

		if (unlock.getCommands() != null)
			SaverCommandHandler.getInstance().registerHandler(unlock);
	}

	public AbstractUnlock getUnlock(String name) {
		if (soloUnlocks.containsKey(name))
			return soloUnlocks.get(name);
		else if (teamUnlocks.containsKey(name))
			return teamUnlocks.get(name);
		else
			return getUnlock("Help");
	}

	public L2JRootPane getWindow(PlayerInstance activeChar) {
		String name = activeChar.getName();
		String soloPoints = String
				.valueOf(PcModel.getModel(activeChar).spartanInstance
						.getSoloPoints());
		String teamPoints = String
				.valueOf(PcModel.getModel(activeChar).spartanInstance
						.getTeamPoints());
		String pvps = String.valueOf(activeChar.getPvpKills());

		L2JRootPane root = new L2JRootPane(activeChar.getName() + " stats");

		L2JVerticalPane headerStats = new L2JVerticalPane(back1, width).toPanel(root);
		L2JVerticalPane contentStats = new L2JVerticalPane(back2, width).toPanel(root);
		
		L2JVerticalPane headerSolo = new L2JVerticalPane(back1, width).toPanel(root);
		L2JVerticalPane contentSolo = new L2JVerticalPane(back2, width).toPanel(root);
		
		L2JVerticalPane headerTeam = new L2JVerticalPane(back1, width).toPanel(root);
		L2JVerticalPane contentTeam = new L2JVerticalPane(back2, width).toPanel(root);

		new L2JText("Main statistics").toPanel(headerStats);
		new L2JText(L2JColor.White, L2JAlign.Center, "Name:" + name).toPanel(contentStats).highlightText(name,L2JColor.Green);
		new L2JText(L2JColor.White, L2JAlign.Center,"Solo Points: " + soloPoints).toPanel(contentStats).highlightText(soloPoints,L2JColor.Green);
		new L2JText(L2JColor.White, L2JAlign.Center,"Team Points: " + teamPoints).toPanel(contentStats).highlightText(teamPoints,L2JColor.Green);
		new L2JText(L2JColor.White, L2JAlign.Center,"PvP Points: " + pvps).toPanel(contentStats).highlightText(pvps,L2JColor.Green);
		new L2JText(L2JColor.White, L2JAlign.Center,"Next Solo Unlock: "+ getNextUnlock(activeChar,AbstractUnlock.TYPE_SOLO)).toPanel(contentStats);
		new L2JText(L2JColor.White, L2JAlign.Center,"Next Team Unlock: "+ getNextUnlock(activeChar,AbstractUnlock.TYPE_TEAM)).toPanel(contentStats);
		new L2JText("Solo Unlock Bonuses").toPanel(headerSolo);
		for (AbstractUnlock unlock : soloUnlocks.values()) {
			L2JHorizontalPane panel = new L2JHorizontalPane(L2JAlign.Left);
			new L2JLink(unlock.getName(), UnlocksHolder.getInstance(),
					L2JAlign.Left, L2JColor.White).toPanel(panel).addArgument(
					unlock.getName());
			unlock.getMini(activeChar).toPanel(panel);

			if (unlock.getCommands() != null && unlock.getCommands()[0] != null) {
				new L2JButton(unlock.getCommands()[0],
						L2JButtonType.GRACIA_SMALL,
						UnlocksHolder.getInstance(), L2JAlign.Right)
						.toPanel(panel).addArgument("UFU"+unlock.getName());
			} else {
				new L2JText("").toPanel(panel);
			}
			panel.toPanel(contentSolo);

		}

		new L2JText("Team Unlock Bonuses").toPanel(headerTeam);
		for (AbstractUnlock unlock : teamUnlocks.values()) {
			L2JHorizontalPane panel = new L2JHorizontalPane(L2JAlign.Left);
			new L2JLink(unlock.getName(), UnlocksHolder.getInstance(),
					L2JAlign.Left, L2JColor.White).toPanel(panel).addArgument(
					unlock.getName());
			unlock.getMini(activeChar).toPanel(panel);

			if (unlock.getCommands() != null && unlock.getCommands()[0] != null) {
				new L2JButton(unlock.getCommands()[0],
						L2JButtonType.GRACIA_SMALL,
						UnlocksHolder.getInstance(), L2JAlign.Right)
						.toPanel(panel).addArgument("UFU"+unlock.getName());
			} else {
				new L2JText("").toPanel(panel);
			}

			panel.toPanel(contentTeam);

		}

		return root;
	}

	/**
	 * @param activeChar
	 * @return
	 */
	private String getNextUnlock(PlayerInstance activeChar, int type) 
	{
		String next="None";
		AbstractUnlock closestUnlock = null;
		if(type == AbstractUnlock.TYPE_SOLO)
		{
			int points = PcModel.getModel(activeChar).spartanInstance.getSoloPoints();
			for(AbstractUnlock unlock: soloUnlocks.values())
			{
				if(closestUnlock == null)
					closestUnlock = unlock;
				else if(unlock.getPvpExperience() > points && unlock.getPvpExperience()< closestUnlock.getPvpExperience());
					closestUnlock = unlock;
			}
		}
		else if(type == AbstractUnlock.TYPE_TEAM)
		{
			int points = PcModel.getModel(activeChar).spartanInstance.getTeamPoints();
			for(AbstractUnlock unlock: teamUnlocks.values())
			{
				if(closestUnlock == null)
					closestUnlock = unlock;
				else if(unlock.getPvpExperience() > points && unlock.getPvpExperience()< closestUnlock.getPvpExperience());
					closestUnlock = unlock;
			}
		}
		
		if(closestUnlock != null)
			next = closestUnlock.getName();
		
		return next;
			
	}

	private volatile static UnlocksHolder singleton;

	public static UnlocksHolder getInstance() {
		if (singleton == null) {
			synchronized (UnlocksHolder.class) {
				if (singleton == null)
					singleton = new UnlocksHolder();
			}
		}
		return singleton;
	}

	@Override
	public void execute(PlayerInstance activeChar, Map<Integer, String> arguments) {

		String argument = arguments.get(0);
		if (argument == null)
		{
			System.out.println("This cant happen.");
			return;
		}
		if (argument.contains("UFU")) {
			argument = argument.replace("UFU", "");
			AbstractUnlock unlock = getUnlock(argument);

			if (unlock == null)
				return;

			if (unlock.getCommands() == null || unlock.getCommands()[0] == null)
				return;

			unlock.useVoicedCommand(unlock.getCommands()[0], activeChar, null);
		} else {
			AbstractUnlock unlock = getUnlock(argument);
			if (unlock == null)
			{
				System.out.println("Didnt find the unlock with argument "+ argument);
				return;
			}

			L2JFrame.getInstance().send(activeChar,
					getDescription(activeChar, unlock));
		}

	}

	@Override
	public void execute(PlayerInstance activeChar) {
		if (activeChar.getTarget() instanceof PlayerInstance)
			L2JFrame.getInstance().send(activeChar, getWindow((PlayerInstance) activeChar.getTarget()));
		else
			L2JFrame.getInstance().send(activeChar, getWindow(activeChar));

	}

	public L2JRootPane getDescription(PlayerInstance activeChar,
			AbstractUnlock unlock) {
		L2JRootPane root = new L2JRootPane(unlock.getName() + " descrption");
		L2JVerticalPane panelHeader = new L2JVerticalPane(back1, width)
				.toPanel(root);
		;
		L2JVerticalPane panelDesc = new L2JVerticalPane(back2, width)
				.toPanel(root);
		;
		new L2JText(unlock.getName()).toPanel(panelHeader);
		new L2JText(unlock.getDescription()).toPanel(panelDesc);
		new L2JButton("Back", L2JButtonType.GRACIA_SMALL,
				UnlocksHolder.getInstance(), L2JAlign.Center)
				.toPanel(panelDesc);

		return root;
	}

	@Override
	public boolean useVoicedCommand(String command, PlayerInstance activeChar,
			String params) {
		
		PlayerInstance target = activeChar;
		
		if(activeChar.getTarget()!= null && (activeChar.getTarget() instanceof PlayerInstance))
			target = (PlayerInstance)activeChar.getTarget();
		
		L2JFrame.getInstance().send(activeChar, getWindow(target));
		
		
		return false;
	}

	@Override
	public String[] getVoicedCommandList() {
	
		return commands;
	}
	
	@Override
	public boolean forGM() {
		// TODO Auto-generated method stub
		return false;
	}

}
