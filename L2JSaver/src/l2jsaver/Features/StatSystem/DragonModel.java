/*
 * Authors: Issle, Howler, Matim
 * File: DragonModel.java
 */
package l2jsaver.Features.StatSystem;

/**
 * @author Issle
 *
 */
public class DragonModel {

	private int ownerId;
	private int locationId;
	private boolean canTake;
	private String name;
	private boolean status=true;
	private String ownerName;
	
	private int x=0;
	private int y=0;
	private int z=0;
	
	public DragonModel(int ownerId, int locationId, String name)
	{
		this.ownerId = ownerId;
		this.locationId = locationId;
		this.name = name;
	}
	
	/**
	 * @param ownerId the ownerId to set
	 */
	public void setOwnerId(int ownerId) {
		this.ownerId = ownerId;
	}
	/**
	 * @return the ownerId
	 */
	public int getOwnerId() {
		return ownerId;
	}
	/**
	 * @param locationId the locationId to set
	 */
	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}
	/**
	 * @return the locationId
	 */
	public int getLocationId() {
		return locationId;
	}
	/**
	 * @param canTake the canTake to set
	 */
	public void setCanTake(boolean canTake) {
		this.canTake = canTake;
	}
	/**
	 * @return the canTake
	 */
	public boolean isCanTake() {
		return canTake;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(boolean status) {
		this.status = status;
	}

	/**
	 * @return the status
	 */
	public boolean isStatus() {
		return status;
	}

	/**
	 * @param ownerName the ownerName to set
	 */
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	/**
	 * @return the ownerName
	 */
	public String getOwnerName() {
		return ownerName;
	}

	/**
	 * @param x the x to set
	 */
	public void setX(int x) {
		this.x = x;
	}

	/**
	 * @return the x
	 */
	public int getX() {
		return x;
	}

	/**
	 * @param y the y to set
	 */
	public void setY(int y) {
		this.y = y;
	}

	/**
	 * @return the y
	 */
	public int getY() {
		return y;
	}

	/**
	 * @param z the z to set
	 */
	public void setZ(int z) {
		this.z = z;
	}

	/**
	 * @return the z
	 */
	public int getZ() {
		return z;
	}
	
}
