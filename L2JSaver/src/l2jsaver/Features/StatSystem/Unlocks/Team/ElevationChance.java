/*
 * Authors: Issle, Howler, Matim
 * File: ElevationChance.java
 */
package l2jsaver.Features.StatSystem.Unlocks.Team;

import l2jsaver.Features.StatSystem.AbstractUnlock;

import org.l2jmobius.gameserver.model.actor.instance.PlayerInstance;

/**
 * @author Issle
 *
 */
public class ElevationChance extends AbstractUnlock{

	/**
	 * @param name
	 * @param experience
	 * @param type
	 */
	public ElevationChance() {
		super("Elevation-Chance", 210000, TYPE_TEAM);
		description = "Elevation chance allows you to gain extra landing chance when attacking someone from above. " +
				"Use the tactical advantage to get the upper hand in combat.";
	}

	@Override
	public double getResult(PlayerInstance activeChar) {

		if(!canUse(activeChar,false))
			return 1;
		if(activeChar.getTarget()==null)
			return 1;
		else if(activeChar.getTarget().getX()< activeChar.getX()+60)
			return 1.03;
		else
			return 1;
			
	}
}
