/*
 * Authors: Issle, Howler, Matim
 * File: ElevationDefense.java
 */
package l2jsaver.Features.StatSystem.Unlocks.Team;

import org.l2jmobius.gameserver.model.actor.instance.PlayerInstance;

import l2jsaver.Features.StatSystem.AbstractUnlock;

/**
 * @author Issle
 *
 */
public class ElevationDefense extends AbstractUnlock{

	
	/**
	 * @param name
	 * @param experience
	 * @param type
	 */
	public ElevationDefense() {
		super("Elevation-Defense", 140000, TYPE_TEAM);
		description = "Elevation defense allows you to gain extra defense when you are above your attacker. " +
				"Use the tactical advantage to get the upper hand in combat.";
	}

	@Override
	public double getResult(PlayerInstance activeChar) {

		if(!canUse(activeChar,false))
			return 1;
		if(activeChar.getTarget()==null)
			return 1;
		else if(activeChar.getTarget().getX()< activeChar.getX()+20)
			return 1.1;
		else
			return 1;
			
	}
}
