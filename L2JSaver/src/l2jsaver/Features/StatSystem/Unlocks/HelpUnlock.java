/*
 * Authors: Issle, Howler, Matim
 * File: HelpUnlock.java
 */
package l2jsaver.Features.StatSystem.Unlocks;


import org.l2jmobius.gameserver.model.actor.instance.PlayerInstance;

import l2jsaver.Features.StatSystem.AbstractUnlock;

/**
 * @author Issle
 *
 */
public class HelpUnlock extends AbstractUnlock{

	/**
	 * @param name
	 * @param experience
	 * @param type
	 */
	public HelpUnlock() {
		super("Help", 0, AbstractUnlock.TYPE_SOLO);
		// TODO Auto-generated constructor stub
		
		description = "The stat system allows you to gain access to multiple bonuses and unlocks by competing in pvp.  " +
				"The system rewards solo and team points during your pvp by checking your actions. It rewards more points " +
				"then you team play (assist) or when you do brave actions ( eg against zergs ). It doesnt count the pvp kill so " +
				"all classes have the chance to compete in the system.";
	}

	@Override
	public double getResult(PlayerInstance activeChar) {
		// TODO Auto-generated method stub
		return 1;
	}

}
