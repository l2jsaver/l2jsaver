/*
 * Authors: Issle, Howler, David
 * File: VisualArmorUnlock.java
 */
package l2jsaver.Features.StatSystem.Unlocks.Solo;

import org.l2jmobius.gameserver.instancemanager.CastleManager;
import org.l2jmobius.gameserver.model.actor.instance.PlayerInstance;

import l2jsaver.Features.StatSystem.AbstractUnlock;

/**
 * @author Issle
 * 
 */
public class BigBlindUnlock extends AbstractUnlock
{
	public BigBlindUnlock()
	{
		super("Big-Blind",20000,TYPE_SOLO);
		description = "The big blind, grants a bonus surprise in the clan leaders clan warehouse every time " +
				"some team member is farming. You get a 3% basic bonus chance for a drop to duplicate inside " +
				"your warehouse when a clan member is farming it. The chance gets x2 if your clan is in an ally " +
				"and another x2 if your clan has a castle. An extra x2 is granted when your clan " +
				"has Aden Castle. Check your warehouse regularly as cool things will appear there when people are " +
				"farming. Remember the more players in the clan, the better the surprise too. Use that items to help " +
				"clan members and better manage your clan.";
	}
	
	
	public static boolean hasClan(PlayerInstance activeChar)
	{
		if(activeChar.getClan()!= null)
			return true;
		return false;
	}
	
	public static boolean hasAlly(PlayerInstance activeChar)
	{
		if(!hasClan(activeChar))
			return false;
		if(activeChar.getClan().getAllyId()==0)
			return false;
		return true;
	}
	
	/**
	 * Returns 0 if no castle is available, 1 if there
	 * is a castle, and 2 if the castle is aden.
	 * @param activeChar
	 * @return
	 */
	public static int hasOrAlliesCastle(PlayerInstance activeChar)
	{
		if(!hasClan(activeChar))
			return 1;
		
		//First see if he has a castle.
		if(activeChar.getClan().getCastleId() != 0 )
		{
			if(CastleManager.getInstance().getCastleById(activeChar.getClan().getCastleId()).getName().contains("Aden"))
				return 4;
			else
				return 2;
		}
		else
			return 1;	
	}

	@Override
	public double getResult(PlayerInstance activeChar) 
	{
		if(!canUse(activeChar,false))
			return 0;
		
		float value = 0;
		
		if(hasClan(activeChar))
			value = (float) 3;
		
		if(hasAlly(activeChar))
			value *=2;
		
		value *= hasOrAlliesCastle(activeChar);
		
		return value;	
	}
}