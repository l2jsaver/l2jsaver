/*
 * Authors: Issle, Howler, David
 * File: VisualArmorUnlock.java
 */
package l2jsaver.Features.StatSystem.Unlocks.Solo;

import org.l2jmobius.gameserver.data.sql.impl.ClanTable;
import org.l2jmobius.gameserver.model.actor.instance.PlayerInstance;
import org.l2jmobius.gameserver.model.clan.Clan;

import l2jsaver.Features.StatSystem.AbstractUnlock;

/**
 * @author Issle
 * 
 */
public class AllyRecall extends AbstractUnlock
{
	public AllyRecall()
	{
		super("Ally-Recall", 60000, TYPE_SOLO);
		description = "The ally recall allows any ally clan-leader to recall his ally in his location. Use this with caution " +
				"or you may start losing clan members.";
		commands = new String[1];
		commands[0]= "allyRecall";
	}
	


	@Override
	public boolean useVoicedCommand(String command, PlayerInstance activeChar,
			String params) {
		if(!canUse(activeChar))
			return false;
		if(command.equalsIgnoreCase("allyRecall"))
		{
			if(recallerConditionsOk(activeChar))
			{
				int count =0;
				for(Clan clan: ClanTable.getInstance().getClans())
				{
					if(!(clan.getAllyId()== activeChar.getClan().getAllyId()) || clan.getAllyId()==0)
						continue;
					
					for(PlayerInstance player: clan.getOnlineMembers(activeChar.getObjectId()))
					{
						if(player.getObjectId()!= activeChar.getObjectId() && calledConditionsOk(player))
						{
							player.teleToLocation(activeChar.getX(), activeChar.getY(), activeChar.getZ(), false);
							player.sendMessage("You are being ally recalled.");
							count++;
						}
					}
				}
				
				activeChar.sendMessage("Recalled "+String.valueOf(count)+"/"+String.valueOf(activeChar.getParty().getMemberCount()));
			}
			else
				activeChar.sendMessage("Conditions are not ok to perform this action.");
		}
		return false;
	}
	
	public boolean recallerConditionsOk(PlayerInstance activeChar)
	{
		
		if(activeChar.getClan()== null)
			return false;
		if(activeChar.getClan().getLeaderId()!= activeChar.getObjectId())
			return false;
		if(activeChar.isInSiege())
			return false;
		if(activeChar.isInOlympiadMode())
			return false;
		if(activeChar.getInstanceId()!=0)
			return false;
		if(activeChar.getPvpFlag()!=0)
			return false;
		if(activeChar.isInCombat())
			return false;
		
		return true;
	}
	
	public boolean calledConditionsOk(PlayerInstance activeChar)
	{
		if(activeChar.isInOlympiadMode())
			return false;
		if(activeChar.getInstanceId()!=0)
			return false;
		if(activeChar.getPvpFlag()!=0)
			return false;
		if(activeChar.isInCombat())
			return false;
		
		return true;
	}


	@Override
	public double getResult(PlayerInstance activeChar) {
		// TODO Auto-generated method stub
		return 0;
	}
}