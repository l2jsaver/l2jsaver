/*
 * Authors: Issle, Howler, David
 * File: VisualArmorUnlock.java
 */
package l2jsaver.Features.StatSystem.Unlocks.Solo;

import org.l2jmobius.gameserver.model.actor.instance.PlayerInstance;

import l2jsaver.Features.StatSystem.AbstractUnlock;

/**
 * @author Issle
 * 
 */
public class PartyRecall extends AbstractUnlock
{
	public PartyRecall()
	{
		super("Party-recall", 16000, TYPE_SOLO);
		description = "The party recall allows the party leader to recall his party in his location.";
		commands = new String[1];
		commands[0]= "partyRecall";
	}



	@Override
	public boolean useVoicedCommand(String command, PlayerInstance activeChar,
			String params) {
		
		if(!canUse(activeChar))
			return false;
		if(command.equalsIgnoreCase("partyRecall"))
		{
			if(recallerConditionsOk(activeChar))
			{
				int count =0;
				for(PlayerInstance player: activeChar.getParty().getMembers())
				{
					if(player.getObjectId()!= activeChar.getObjectId() && calledConditionsOk(player))
					{
						player.teleToLocation(activeChar.getX(), activeChar.getY(), activeChar.getZ(), false);
						player.sendMessage("You are being party recalled.");
						count++;
					}
				}
				activeChar.sendMessage("Recalled "+String.valueOf(count)+"/"+String.valueOf(activeChar.getParty().getMemberCount()));
			}
			else
				activeChar.sendMessage("Conditions are not ok to perform this action.");
		}
		return false;
	}
	
	public boolean recallerConditionsOk(PlayerInstance activeChar)
	{
		if(activeChar.getParty()==null)
			return false;
		if(activeChar.isInSiege())
			return false;
		if(activeChar.isInOlympiadMode())
			return false;
		if(activeChar.getInstanceId()!=0)
			return false;
		if(activeChar.getPvpFlag()!=0)
			return false;
		if(activeChar.isInCombat())
			return false;
		
		return true;
	}
	
	public boolean calledConditionsOk(PlayerInstance activeChar)
	{
		if(activeChar.isInOlympiadMode())
			return false;
		if(activeChar.getInstanceId()!=0)
			return false;
		if(activeChar.getPvpFlag()!=0)
			return false;
		if(activeChar.isInCombat())
			return false;
		
		return true;
	}

	@Override
	public double getResult(PlayerInstance activeChar) {
		// TODO Auto-generated method stub
		return 0;
	}

}