/*
 * Authors: Issle, Howler, David
 * File: VisualArmorUnlock.java
 */
package l2jsaver.Features.StatSystem.Unlocks.Solo;

import org.l2jmobius.gameserver.model.actor.instance.PlayerInstance;
import org.l2jmobius.gameserver.model.clan.Clan;

import l2jsaver.Features.StatSystem.AbstractUnlock;

/**
 * @author Issle
 * 
 */
public class DoubleTrouble extends AbstractUnlock
{
	public DoubleTrouble()
	{
		super("War-Dog", 3000, TYPE_SOLO);
		description = "This unlock allows you to get double drop rate if your clan has accepted all the clan wars that are declared to it.";
	}

	

	@Override
	public double getResult(PlayerInstance activeChar) {

		if(canUse(activeChar,false))
		{
			if(activeChar.getClan()==null)
				return 1;
			
			Clan clan = activeChar.getClan();
			
			if((clan.getAttackerList() == null || clan.getAttackerList().size()<1 ) &&( clan.getWarList()==null || clan.getWarList().size()<1))
				return 1;
			if(clan.getAttackerList()!=null)
				for(int x : clan.getAttackerList())
				{
					if(!clan.isAtWarWith(x))
						return 1;
				}

			activeChar.sendMessage("Boost drop rate due to 100% accepted clan wars.");
			return 2;
		}
		else
			return 1;
	}
}