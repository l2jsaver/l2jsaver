/*
 * Authors: Issle, Howler, David
 * File: VisualArmorUnlock.java
 */
package l2jsaver.Features.StatSystem.Unlocks.Solo;

import org.l2jmobius.gameserver.model.actor.instance.PlayerInstance;

import l2jsaver.Features.StatSystem.AbstractUnlock;

/**
 * @author Issle
 * 
 */
public class ClanRecall extends AbstractUnlock
{
	public ClanRecall()
	{
		super("Clan-Recall",20000,TYPE_SOLO);
		description = "The clan recall allows the clan leader to recall his clan in his location.";
		commands = new String[1];
		commands[0]= "clanRecall";
	}
	

	@Override
	public boolean useVoicedCommand(String command, PlayerInstance activeChar,
			String params) {
		if(!canUse(activeChar))
			return false;
		if(command.equalsIgnoreCase("clanRecall"))
		{
			if(recallerConditionsOk(activeChar))
			{
				int count =0;
				for(PlayerInstance player: activeChar.getClan().getOnlineMembers(activeChar.getObjectId()))
				{
					if(player.getObjectId()!= activeChar.getObjectId() && calledConditionsOk(player))
					{
						player.teleToLocation(activeChar.getX(), activeChar.getY(), activeChar.getZ(), false);
						player.sendMessage("You are being clan recalled.");
						count++;
					}
				}
				activeChar.sendMessage("Recalled "+String.valueOf(count)+"/"+String.valueOf(activeChar.getParty().getMemberCount()));
			}
			else
				activeChar.sendMessage("Conditions are not ok to perform this action.");
		}
		return false;
	}
	
	public boolean recallerConditionsOk(PlayerInstance activeChar)
	{
		if(activeChar.getClan()==null)
			return false;
		if(activeChar.isInSiege())
			return false;
		if(activeChar.isInOlympiadMode())
			return false;
		if(activeChar.getInstanceId()!=0)
			return false;
		if(activeChar.getPvpFlag()!=0)
			return false;
		if(activeChar.isInCombat())
			return false;
		
		return true;
	}
	
	public boolean calledConditionsOk(PlayerInstance activeChar)
	{
		if(activeChar.isInOlympiadMode())
			return false;
		if(activeChar.getInstanceId()!=0)
			return false;
		if(activeChar.getPvpFlag()!=0)
			return false;
		if(activeChar.isInCombat())
			return false;
		
		return true;
	}


	@Override
	public double getResult(PlayerInstance activeChar) {
		// TODO Auto-generated method stub
		return 0;
	}
}