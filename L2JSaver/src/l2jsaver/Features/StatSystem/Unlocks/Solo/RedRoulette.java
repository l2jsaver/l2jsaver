/*
 * Authors: Issle, Howler, David
 * File: VisualArmorUnlock.java
 */
package l2jsaver.Features.StatSystem.Unlocks.Solo;

import org.l2jmobius.gameserver.model.actor.instance.PlayerInstance;

import l2jsaver.Features.StatSystem.AbstractUnlock;

/**
 * @author Issle
 * 
 */
public class RedRoulette extends AbstractUnlock
{
	public RedRoulette()
	{
		super("Red-Roulette", 70, TYPE_SOLO);
		description = "This unlock allows you to get triple drop rate when farming while being with karma.";
	}

	

	@Override
	public double getResult(PlayerInstance activeChar) {

		if(canUse(activeChar,false)&& activeChar.getKarma()>1)
		{
			return 4;
		}
		else
			return 1;
	}
}