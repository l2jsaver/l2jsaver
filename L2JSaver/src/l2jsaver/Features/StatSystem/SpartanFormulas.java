/*
 * Authors: Issle, Howler, Matim
 * File: SpartanFormulas.java
 */
package l2jsaver.Features.StatSystem;

import java.util.Collection;

import org.l2jmobius.commons.util.Rnd;
import org.l2jmobius.gameserver.model.World;
import org.l2jmobius.gameserver.model.WorldObject;
import org.l2jmobius.gameserver.model.actor.Creature;
import org.l2jmobius.gameserver.model.actor.Summon;
import org.l2jmobius.gameserver.model.actor.instance.PlayerInstance;
import org.l2jmobius.gameserver.model.skills.Skill;

import l2jsaver.Model.PcModel;

/**
 * @author Issle
 *
 */
public class SpartanFormulas {

	private static final int MAX_ALLOWED_SKILL_REPETITIONS = 20;
	private static final int MAX_ALLOWED_TARGET_REPETITIONS = 30;
	private static final int MAX_LOCATION_REPETITIONS = 100;
	private static final int MIN_DISTANCE =300;
	private static final int GIVE_CHANCE = 20;
	private static final int REUSE_BASE = 2000;
	
	public static void onSkillUse(Creature actor, Skill skill)
	{
		WorldObject target = actor.getTarget();
		
		if(target == null)
			return;
		
		if(!(target instanceof PlayerInstance) && !(target instanceof Summon))
			return;
		
		PlayerInstance player = null;
		if(actor instanceof PlayerInstance)
		{
			player = (PlayerInstance)actor;
			setMemory(player,skill,PcModel.getModel(player).spartanInstance);
			
		}
		else if(actor instanceof Summon)
		{
			Summon summon = (Summon)actor;
			player = summon.getOwner();
			if(summon.getOwner()!=null)
				setMemory(player,skill,PcModel.getModel(player).spartanInstance);
			
			
		}
		
		if(player != null)
			rewardPoints(player, PcModel.getModel(player).spartanInstance, skill);
		
	}
	
	private static void setMemory(PlayerInstance player, Skill skill, SpartanInstance i)
	{
		setTargetMemory(player,i);
		setSkillMemory(skill,i);
		setLocationMemory(player,i);
	}
	
	private static void setTargetMemory(PlayerInstance player, SpartanInstance i)
	{
		WorldObject target = player.getTarget();
		
		int objectId=0;
		if(target == null)
			objectId =0;
		else if(target != null)
		{
			objectId = target.getObjectId();
		}
			
		if(objectId == i.getPreviousTargetId())
				i.setTargetRepetition(i.getTargetRepetition() + 1);
		else
		{
			i.setPreviousTargetId(objectId);
			i.setTargetRepetition(0);
		}
	}
	
	private static void setLocationMemory(PlayerInstance player, SpartanInstance i)
	{
		int distance = Math.abs(i.getX()-player.getX())+ Math.abs(i.getY()-player.getY());
		if(distance < MIN_DISTANCE)
			i.setLocationRepetition(i.getLocationRepetition()+1);
		else
			i.setLocationRepetition(0);
		
		i.setX(player.getX());
		i.setY(player.getY());
		
	}

	private static void setSkillMemory(Skill skill, SpartanInstance i)
	{
		int id;
		if(skill == null)
			id = -1;
		else
			id = skill.getId();
		
		if(id == i.getPreviousSkillId())
		{
			i.setSkillRepetition(i.getSkillRepetition() + 1);
		}
		else
		{
			i.setPreviousSkillId(id);
			i.setSkillRepetition(0);
		}
	}

	private static void rewardPoints(PlayerInstance player, SpartanInstance i, Skill skill)
	{
		if(!memoryOk(i, player))
			return;
		
		rewardSoloPoints(player,i,skill);
		rewardTeamPoints(player,i,skill);
		
	}
	
	/**
	 * Points += party members.
	 * Points += party clan members.
	 * @param player
	 */
	private static void rewardTeamPoints(PlayerInstance player, SpartanInstance i, Skill skill) 
	{
		int points = 0;
		if(player.getParty()==null)
			return;
		
		points += player.getParty().getMemberCount();
		
		if(player.getClan()!=null)
			for(PlayerInstance member : player.getParty().getMembers())
				if(member.getClan()!= null && member.getClan().getId() == player.getClan().getId())
					points++;
		
		int reuseDelay = 2000;
		if(skill != null)
			reuseDelay = skill.getReuseDelay();
		
		points *=(reuseDelay/REUSE_BASE+1);
		i.setTeamPoints(i.getTeamPoints()+getRandom(points,20));
	}
	
	private static int getRandom(int points, int chance)
	{
		int dice = Rnd.get(100);
		if(chance >= dice)
			return points;
		else
			return 0;
	}

	/**
	 * If the player goes in a mass pvp reward the count.
	 * If player targets a party , reward.
	 * @param player
	 */
	private static void rewardSoloPoints(PlayerInstance player,SpartanInstance i, Skill skill) 
	{
		int points = 0;
		Collection<PlayerInstance> players = World.getInstance().getVisibleObjectsInRange(player, PlayerInstance.class, 1000);
		
		int flagged = 0;
		for(PlayerInstance candidate : players)
		{
			if(candidate.isInCombat() || candidate.getPvpFlag()!=0)
				flagged++;
		}
		
		points += flagged;
		
		if(player.getTarget()!= null && player.getTarget() instanceof PlayerInstance)
		{
			PlayerInstance target = (PlayerInstance)player.getTarget();
			
			if(target.getParty()!= null)
				points += target.getParty().getMemberCount();
		}
		
		int reuseDelay = 2000;
		if(skill != null)
			reuseDelay = skill.getReuseDelay();
		
		points *=(reuseDelay/REUSE_BASE+1);
		
		i.setSoloPoints(i.getSoloPoints()+getRandom(points,GIVE_CHANCE));
	}

	private static boolean memoryOk(SpartanInstance i, PlayerInstance player)
	{
		if(i.getSkillRepetition()< MAX_ALLOWED_SKILL_REPETITIONS && i.getTargetRepetition()< MAX_ALLOWED_TARGET_REPETITIONS && !player.isInsidePeaceZone(player) && i.getLocationRepetition()<MAX_LOCATION_REPETITIONS)
			return true;
		else
			return false;
	}

	
}
