/*
 * Authors: Issle, Howler, Matim
 * File: TokenHolder.java
 */
package l2jsaver.Features.StatSystem;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.l2jmobius.gameserver.model.WorldObject;
import org.l2jmobius.gameserver.model.actor.Summon;
import org.l2jmobius.gameserver.model.actor.instance.PlayerInstance;

/**
 * @author Issle
 *
 */
public class TokenHolder {

	private Map<Integer, DragonModel> tokenByOwner;
	private Map<Integer, Map<Integer,DragonModel>> tokensByDictator;
	
	
	/**
	 * Call this ONLY after the token is formed.
	 * @param dm
	 */
	public void addToken(DragonModel dm)
	{
		tokenByOwner.put(dm.getOwnerId(), dm);
		
		if(!tokensByDictator.containsKey(dm.getLocationId()))
			tokensByDictator.put(dm.getLocationId(), new HashMap<Integer,DragonModel>());
		
		tokensByDictator.get(dm.getLocationId()).put(dm.getOwnerId(),dm);
		
	}
	
	/**
	 * Call this BEFORE the token is transformed.
	 * @param dm
	 */
	public void removeToken(DragonModel dm)
	{
		tokenByOwner.remove(dm.getOwnerId());
		tokensByDictator.get(dm.getLocationId()).remove(dm.getOwnerId());
	}
	
	public Collection<DragonModel> getTokens(PlayerInstance activeChar)
	{
		if(tokensByDictator.containsKey(activeChar.getObjectId()))
			return tokensByDictator.get(activeChar.getObjectId()).values();
		else
			return null;
	}
	
	public DragonModel getToken(PlayerInstance activeChar)
	{
		return tokenByOwner.get(activeChar.getObjectId());
	}
	
	public DragonModel getTokenById(int id)
	{
		return tokenByOwner.get(id);
	}
	
	private volatile static TokenHolder singleton;

	private TokenHolder() {
		tokensByDictator = new HashMap<Integer, Map<Integer,DragonModel>>();
		tokenByOwner = new HashMap<Integer, DragonModel>();
	}

	public static TokenHolder getInstance() {
		if (singleton == null) {
			synchronized (TokenHolder.class) {
				if (singleton == null)
					singleton = new TokenHolder();
			}
		}
		return singleton;
	}

	/**
	 * @return
	 */
	public Collection<DragonModel> getAllTokens() {
		return tokenByOwner.values();
	}

	/**
	 * @param attacker
	 * @param target
	 * @return
	 */
	public boolean canBeAttacked(WorldObject attacker, WorldObject target) {

		PlayerInstance holder=null;
		PlayerInstance attackr = null;
		if(target instanceof PlayerInstance && attacker instanceof PlayerInstance)
		{
			holder = (PlayerInstance)target;
			attackr = (PlayerInstance)attacker;
			if(getTokens(holder)!= null && getTokens(holder).size()>=1 && getToken(attackr)!=null)
				return true;
		}
		else if(target instanceof Summon)
		{
			holder = ((Summon)target).getOwner();
			if(getTokens(holder)!= null && getTokens(holder).size()>=1)
				return true;
		}
		return false;
		
	}

	/**
	 * @param killedId
	 * @return
	 */
	public Collection<DragonModel> getTokens(int killedId) {

		if(tokensByDictator.containsKey(killedId))
			return tokensByDictator.get(killedId).values();
		else
			return null;
	}

}
