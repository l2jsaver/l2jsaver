package l2jsaver.Features.L2JFrame;

import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.logging.Logger;

import org.l2jmobius.gameserver.model.WorldObject;
import org.l2jmobius.gameserver.model.actor.Npc;
import org.l2jmobius.gameserver.model.actor.instance.PlayerInstance;
import org.l2jmobius.gameserver.network.serverpackets.NpcHtmlMessage;
import org.l2jmobius.gameserver.network.serverpackets.ShowBoard;

import l2jsaver.Features.L2JFrame.interfaces.IGraphic;
import l2jsaver.Features.L2JFrame.models.primitive.L2JRootPane;

public class L2JFrame
{
	private static final Logger LOGGER = Logger.getLogger(L2JFrame.class.getName());
	private static final String start = "<html><body>";
	private static final String end = "</body></html>";
	private static final String credits = "<br><br><center>Copyright 2010-2011 by L2JSaver</center><br>";
	private static final String l2jframetext = "<center>Powered by L2JFrame</center>";
	private static final String UNAUTHORIZED = "Not enough priviladges to do that action.";
	
	public Map<Integer, iShowWindow> registeredNpcs = new HashMap<>();
	private Map<Integer, AbstractController> controllers = new HashMap<>();
	private int counter = 0;
	
	public void registerNpc(int id, iShowWindow window)
	{
		registeredNpcs.put(id, window);
	}
	
	public void registerNpcs(int[] ids, iShowWindow window)
	{
		for (int id : ids)
			registeredNpcs.put(id, window);
	}
	
	public void showWindow(PlayerInstance player, int id)
	{
		NpcHtmlMessage message = new NpcHtmlMessage(start+ registeredNpcs.get(id).showWindow(player)+credits+l2jframetext+end);
		player.sendPacket(message);
		
	}
	
	public void send(PlayerInstance activeChar, IGraphic root)
	{
		if(root instanceof L2JRootPane)
		{
			if (((L2JRootPane) root).isBoard)
			{
				String html = start+ root.draw()+credits+l2jframetext+end;
				if (html.length() < 4090)
				{
					activeChar.sendPacket(new ShowBoard(html, "101"));
					activeChar.sendPacket(new ShowBoard(null, "102"));
					activeChar.sendPacket(new ShowBoard(null, "103"));
				}
				else if (html.length() < 8180)
				{
					activeChar.sendPacket(new ShowBoard(html.substring(0, 4090), "101"));
					activeChar.sendPacket(new ShowBoard(html.substring(4090, html.length()), "102"));
					activeChar.sendPacket(new ShowBoard(null, "103"));
				}
				else if (html.length() < 12270)
				{
					activeChar.sendPacket(new ShowBoard(html.substring(0, 4090), "101"));
					activeChar.sendPacket(new ShowBoard(html.substring(4090, 8180), "102"));
					activeChar.sendPacket(new ShowBoard(html.substring(8180, html.length()), "103"));
						
				}
			}
			else
			{
				NpcHtmlMessage message = new NpcHtmlMessage(start+ root.draw()+credits+l2jframetext+end);
				activeChar.sendPacket(message);
			}
		}
	}
	
	private L2JFrame()
	{
		controllers = new HashMap<>();
		counter = 0;
	}
	
	public int registerListener(AbstractController listener)
	{
		int x = generateListenerId();
		listener.setHandlerId(String.valueOf(x));
		controllers.put(x, listener);
		return x;
	}
	

	public void bypassToServer(PlayerInstance activeChar, String commandId)
	{
		LOGGER.info(commandId);
		StringTokenizer st = new StringTokenizer(commandId);
		int x = 0;
		
		if(!st.hasMoreTokens())
			return;
		int key =0;
		
		try{
			key = Integer.parseInt(st.nextToken());
		}catch(Exception e)
		{
			e.printStackTrace();
			return;
		}
		
		Map<Integer,String> arguments = new HashMap<>();

		while(st.hasMoreTokens())
		{
			String arg = st.nextToken();
			LOGGER.info(arg);
			arguments.put(x, arg);
			x++;
		}
		
		if(!controllers.containsKey(key))
		{
			LOGGER.info("No controller found for key: " + key);
			return;
		}
		
		AbstractController controller = controllers.get(key);
			
		if(x>0)
			if(!controller.isAdmin())	
				controller.execute(activeChar, arguments);
			else if(activeChar.isGM())
				controller.execute(activeChar,arguments);
			else
				activeChar.sendMessage(UNAUTHORIZED);
		else
			if(!controller.isAdmin())	
				controller.execute(activeChar);
			else if(activeChar.isGM())
				controller.execute(activeChar);
			else
				activeChar.sendMessage(UNAUTHORIZED);
	}
	
	private int generateListenerId()
	{
		counter ++;
		return counter -1;
	}
	
	private static class SingletonHolder
	{
		public static final L2JFrame INSTANCE = new L2JFrame();
	}

	public static L2JFrame getInstance()
	{
		return SingletonHolder.INSTANCE;
	}

	/**
	 * @param object
	 * @return
	 */
	public boolean hasNpc(WorldObject object)
	{
		Npc npc = (Npc)object;
		if(registeredNpcs.containsKey(npc.getId()))
			return true;
		
		return false;
	}

	/**
	 * @return the controllers
	 */
	public Map<Integer, AbstractController> getControllers() {
		return controllers;
	}

	/**
	 * @param controllers the controllers to set
	 */
	public void setControllers(Map<Integer, AbstractController> controllers) {
		this.controllers = controllers;
	}
	
}