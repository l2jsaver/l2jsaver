/*
 * Authors: Issle, Howler, David
 * File: SampleNpc.java
 */
package l2jsaver.Features.L2JFrame;

import org.l2jmobius.gameserver.model.actor.instance.PlayerInstance;

import l2jsaver.Features.L2JFrame.models.L2JColor;
import l2jsaver.Features.L2JFrame.models.primitive.L2JRootPane;
import l2jsaver.Features.L2JFrame.models.primitive.L2JVerticalPane;

/**
 * @author Issle
 *
 */
public class SampleNpc implements iShowWindow
{

	class Npc extends L2JRootPane
	{
		public Npc()
		{
			super();
			addGraphic( new L2JVerticalPane(L2JColor.White));
		}
	}
	
	@Override
	public String showWindow(PlayerInstance activeChar)
	{
		Npc npc = new Npc();
		return npc.draw();
		
	}

}
