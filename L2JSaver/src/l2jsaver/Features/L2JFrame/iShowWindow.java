/*
 * Authors: Issle, Howler, David
 * File: showWindow.java
 */
package l2jsaver.Features.L2JFrame;

import org.l2jmobius.gameserver.model.actor.instance.PlayerInstance;

/**
 * @author Issle
 *
 */
public interface iShowWindow
{
	public String showWindow(PlayerInstance activeChar);
}
