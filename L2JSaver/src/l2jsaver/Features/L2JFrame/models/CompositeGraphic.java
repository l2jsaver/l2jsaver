package l2jsaver.Features.L2JFrame.models;

import java.util.LinkedList;

import l2jsaver.Features.L2JFrame.interfaces.IGraphic;
import l2jsaver.Features.L2JFrame.models.primitive.L2JHorizontalPane;
import l2jsaver.Features.L2JFrame.models.primitive.L2JNewLine;

public class CompositeGraphic
{
	
	protected LinkedList<IGraphic> graphics;
	
	public CompositeGraphic()
	{
		graphics = new LinkedList<IGraphic>();
	}
	
	public boolean addGraphic(IGraphic graphic)
	{
		if(graphics.contains(graphic))
			return false;
		
		graphics.add(graphic);
		return true;
	}
	
	public boolean addGraphic(IGraphic[] graphic)
	{
		for (IGraphic graph: graphic)
		{
			if (graphics.contains(graph))
				return false;
			
			graphics.add(graph);
		}
		return true;
	}
	
	public void newLine()
	{
		graphics.add(new L2JNewLine());
	}
	
	public L2JHorizontalPane makeRow(IGraphic g, IGraphic g2)
	{
		L2JHorizontalPane pane = new L2JHorizontalPane(L2JAlign.Center);
		pane.addGraphic(g);
		pane.addGraphic(g2);
		
		return pane;
	}
	
	public boolean removeGraphic(IGraphic graphic)
	{
		if(!graphics.contains(graphic))
			return false;
		
		graphics.remove(graphic);
		return true;
	}
}
