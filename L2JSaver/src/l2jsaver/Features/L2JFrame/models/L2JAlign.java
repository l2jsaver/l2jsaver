package l2jsaver.Features.L2JFrame.models;

public enum L2JAlign
{

	Left("<left>","</left>"),
	Right("<right>","</right>"),
	Center("<center>","</center>");
	
	private String align1;
	private String align2;
	
	L2JAlign(String align1, String align2)
	{
		this.align1 = align1;
		this.align2 = align2;
	}

	/**
	 * @return the align1
	 */
	public String getAlign1()
	{
		return align1;
	}
	
	/**
	 * @return the align2
	 */
	public String getAlign2() {
		return align2;
	}
	
}
