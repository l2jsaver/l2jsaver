package l2jsaver.Features.L2JFrame.models;

public enum L2JColor
{
	
	Grey("434341"),
	Red("960000"),
	Green("5EA82E"),
	Blue("1E64CC"),
	Yellow("DEDE21"),
	Pink("DE21AF"),
	Nostalgic("33FF33"),
	Nostalgic1("B0864A"),
	Black("000000"),
	White("FFFFFF"),
	Custom("0099FF"),
	BufferLink("D43D1A");
	
	private String color;
	
	L2JColor(String color)
	{
		this.color = color;
	}

	/**
	 * @return the color
	 */
	public String getColor()
	{
		return color;
	}
}
