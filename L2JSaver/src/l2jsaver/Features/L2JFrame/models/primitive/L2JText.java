package l2jsaver.Features.L2JFrame.models.primitive;

import l2jsaver.Features.L2JFrame.interfaces.IGraphic;
import l2jsaver.Features.L2JFrame.models.CompositeGraphic;
import l2jsaver.Features.L2JFrame.models.L2JAlign;
import l2jsaver.Features.L2JFrame.models.L2JColor;

import java.util.HashMap;
import java.util.Map;

public class L2JText implements IGraphic
{
	private String content;
	private L2JColor background;
	private L2JColor color;
	private L2JAlign align;
	private int width;
	private Map<String, L2JColor> highlights = new HashMap<String,L2JColor>();
	
	private static String startReplacer = "</font><font color=";
	private static String fontColorText = "<font color=";
	
	public L2JText(L2JColor color, L2JAlign align, String content)
	{
		this.color = color;
		this.content = content;
		this.align = align;
	}
	
	public L2JText(L2JColor color, L2JAlign align, String content, int width)
	{
		this.color = color;
		this.content = content;
		this.align = align;
		this.width = width;
	}
	
	/**
	 * Simple white, centered text.
	 * @param color
	 * @param align
	 * @param content
	 */
	public L2JText(String content)
	{
		this.color = L2JColor.White;
		this.content = content;
		this.align = L2JAlign.Center;
	}
	
	public L2JText(String content, int width)
	{
		this.color = L2JColor.White;
		this.content = content;
		this.align = L2JAlign.Center;
		this.width = width;
	}
	
	public L2JText toPanel(CompositeGraphic panel)
	{
		panel.addGraphic(this);
		return this;
	}
	
	public L2JText(L2JColor color, L2JAlign align, String content, L2JColor background)
	{
		this.color = color;
		this.content = content;
		this.align = align;
		this.background = background;
	}
	
	public L2JText(L2JColor color, L2JAlign align, int content)
	{
		this.color = color;
		this.content = String.valueOf(content);
		this.align = align;
	}
	
	/**
	 * @param votes
	 */
	public L2JText(int input) {
		this.color = L2JColor.White;
		this.content = String.valueOf(input);
		this.align = L2JAlign.Center;
	}

	@Override
	public String draw() 
	{
		String output ="<td";
		if (width > 0)
			output += " width="+width;
		if (background != null)
			output += " bgcolor="+background.getColor();
		output += ">"+align.getAlign1()+ fontColorText +color.getColor()+">"+content+"</font>"+align.getAlign2()+"</td>";
		return replaceAllHighlights(output);
	}
	
	private String replaceAllHighlights(String output)
	{
		String temp = output;
		
		for(String text: highlights.keySet())
		{
			temp = temp.replace(text, startReplacer+highlights.get(text).getColor()+">"+text+startReplacer+color.getColor()+">");
		}

		return temp;
	}
	
	public void highlightText(String text, L2JColor color)
	{
		if(!highlights.containsKey(text))
			highlights.put(text, color);
	}
}