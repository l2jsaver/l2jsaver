package l2jsaver.Features.L2JFrame.models.primitive;

import l2jsaver.Features.L2JFrame.interfaces.IGraphic;
import l2jsaver.Features.L2JFrame.models.CompositeGraphic;

public class L2JRootPane extends CompositeGraphic implements IGraphic
{
	public String title;
	public boolean isBoard;

	public L2JRootPane(String title)
	{
		super();
		this.title = title;
	}
	
	// Board's can't take titles.
	public L2JRootPane(boolean isBoard)
	{
		super();
		this.isBoard = isBoard;
	}
	
	public L2JRootPane()
	{
		super();
	}
	
	@Override
	public String draw() 
	{
		String output = "";
		if(title !=null)
			output+= ("<title>"+title+"</title>");
		if (isBoard)
			output += "<center><br>";
		for(IGraphic graphic: graphics)
			output += graphic.draw();
		if (isBoard)
			output += "</center>";
		return output;
	}
}