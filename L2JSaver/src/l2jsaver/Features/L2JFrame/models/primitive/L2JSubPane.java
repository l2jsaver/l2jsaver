package l2jsaver.Features.L2JFrame.models.primitive;

import l2jsaver.Features.L2JFrame.interfaces.IGraphic;
import l2jsaver.Features.L2JFrame.models.CompositeGraphic;
import l2jsaver.Features.L2JFrame.models.L2JColor;

public class L2JSubPane extends CompositeGraphic implements IGraphic
{
	private L2JColor background;
	private int width = 284;
	
	/************************
	 * 
	 *--------item1----------
	 *
	 *--------item2----------
	 *
	 *--------item3----------
	 *
	 ************************/
	
	public L2JSubPane(L2JColor background, int width)
	{
		super();
		this.background = background;
		this.width = width;
	}
	
	public L2JSubPane(L2JColor background)
	{
		super();
		this.background = background;
	}
	
	public L2JSubPane(int width)
	{
		super();
		this.width = width;
	}
	
	public L2JSubPane()
	{
		super();
	}
	
	@Override
	public String draw() 
	{
		String output="";
		if(background == null)
			output += "<td><table width="+width+">";
		else
			output += "<table width="+width+" bgcolor="+background.getColor()+">";
		
		for(IGraphic graphic: graphics)
		{
			output+= ("<tr>"+graphic.draw()+"</tr>");
		}
		output +="</table></td>";
		return output;
	}
}