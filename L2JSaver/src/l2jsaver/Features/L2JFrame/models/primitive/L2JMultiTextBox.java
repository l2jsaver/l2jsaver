package l2jsaver.Features.L2JFrame.models.primitive;

import l2jsaver.Features.L2JFrame.interfaces.IGraphic;
import l2jsaver.Features.L2JFrame.interfaces.IRegistrable;

public class L2JMultiTextBox implements IGraphic, IRegistrable
{
	private String name;
	private int width = 240;
	private int height = 40;
	
	public L2JMultiTextBox( String name)
	{
		this.name = name;
	}
	
	public L2JMultiTextBox( String name, int width, int height)
	{
		this.name = name;
		this.width = width;
		this.height = height;
	}
	
	@Override
	public String draw() 
	{
		String output ="<td><multiedit var=\""+ name +"\" width=" + width + " height=" + height + "></td>";
		return output;
	}
	
	@Override
	public String getName()
	{
		// TODO Auto-generated method stub
		return name;
	}

	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return "$";
	}
}