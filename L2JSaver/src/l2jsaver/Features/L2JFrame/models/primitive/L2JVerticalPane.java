package l2jsaver.Features.L2JFrame.models.primitive;

import l2jsaver.Features.L2JFrame.interfaces.IGraphic;
import l2jsaver.Features.L2JFrame.models.CompositeGraphic;
import l2jsaver.Features.L2JFrame.models.L2JAlign;
import l2jsaver.Features.L2JFrame.models.L2JColor;

public class L2JVerticalPane extends CompositeGraphic implements IGraphic
{
	private L2JColor background;
	private L2JAlign align;
	private int width=284;
	
	/************************
	 * 
	 *--------item1----------
	 *
	 *--------item2----------
	 *
	 *--------item3----------
	 *
	 ************************/
	
	public L2JVerticalPane(L2JColor background, int width)
	{
		super();
		this.background = background;
		this.width = width;
	}
	
	public L2JVerticalPane(L2JColor background)
	{
		super();
		this.background = background;
	}
	
	public L2JVerticalPane(int width)
	{
		super();
		this.width = width;
	}
	
	public L2JVerticalPane(int width, L2JAlign align)
	{
		super();
		this.width = width;
	}
	
	public L2JVerticalPane()
	{
		super();
		this.width = 270;
	}
	
	@Override
	public String draw() 
	{
		String output="";
		if(align != null)
			output +=align.getAlign1();
		if(background == null)
			output += "<table width="+width+">";
		else
			output += "<table width="+width+" bgcolor="+background.getColor()+">";
		
		for(IGraphic graphic: graphics)
		{
			output+= ("<tr>"+graphic.draw()+"</tr>");
		}
		output +="</table>";
		if(align != null)
			output +=align.getAlign2();
		return output;
	}
	
	/**
	 * Adds this object to the input panel.
	 * @param panel
	 * @return
	 */
	public L2JVerticalPane toPanel(L2JRootPane panel)
	{
		panel.addGraphic(this);
		return this;
	}
	
	public void setWidth(int width)
	{
		this.width = width;
	}
}