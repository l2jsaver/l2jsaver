package l2jsaver.Features.L2JFrame.models.primitive;

import java.util.ArrayList;
import java.util.List;

import l2jsaver.Features.L2JFrame.AbstractController;
import l2jsaver.Features.L2JFrame.interfaces.IGraphic;
import l2jsaver.Features.L2JFrame.interfaces.IRegistrable;
import l2jsaver.Features.L2JFrame.models.CompositeGraphic;
import l2jsaver.Features.L2JFrame.models.L2JAlign;
import l2jsaver.Features.L2JFrame.models.L2JColor;

public class L2JButton implements IGraphic
{	
	private String name;
	private L2JButtonType buttonType;
	private String handlerId="";
	private L2JAlign align;
	private String tdAlign;
	private int tdWidth;
	private List<IRegistrable> registrables = new ArrayList<IRegistrable>();
	
	private int width = 32;
	private int height = 32;
	private String icon;
	
	public L2JButton(String _name, L2JButtonType _buttonType, AbstractController handler, L2JAlign align)
	{
		name = _name;
		buttonType = _buttonType;
		if(handler != null)
		handlerId = handler.getHandlerId();
		
		if(align == null)
		{
			this.align = L2JAlign.Left;
		}
		else
		{
			this.align = align;
		}
	}
	
	public L2JButton(String _name, L2JButtonType _buttonType, AbstractController handler, L2JAlign align, String tdAlign, int tdWidth)
	{
		name = _name;
		buttonType = _buttonType;
		if(handler != null)
		handlerId = handler.getHandlerId();
		
		if(align == null)
		{
			this.align = L2JAlign.Left;
		}
		else
		{
			this.align = align;
		}
		this.tdAlign = tdAlign;
		this.tdWidth = tdWidth;
	}
	
	/**
	 * Constructs a button with the given name and controller.
	 * Pre-built arguments are Gracia button and center align.
	 * @param _name
	 * @param handler
	 */
	public L2JButton(String _name, AbstractController handler)
	{
		name = _name;
		buttonType = L2JButtonType.GRACIA;
		if(handler != null)
		handlerId = handler.getHandlerId();
		this.align = L2JAlign.Center;
	}
	
	/**
	 * Constructs a button with the given name,controller and argument.
	 * Pre-built arguments are Gracia button and center align.
	 * @param _name
	 * @param handler
	 */
	public L2JButton(String _name, AbstractController handler, String argument)
	{
		name = _name;
		buttonType = L2JButtonType.GRACIA;
		if(handler != null)
		handlerId = handler.getHandlerId();
		this.align = L2JAlign.Center;
		addArgument(argument);
	}
	
	/**
	 * Registers this graphic to the given panel input.
	 * @param panel
	 */
	public L2JButton toPanel(CompositeGraphic panel)
	{
		panel.addGraphic(this);
		return this;
	}
	
	public L2JButton(String _name, L2JButtonType _buttonType, AbstractController handler, L2JAlign align,L2JColor background)
	{
		name = _name;
		buttonType = _buttonType;
		if(handler != null)
		handlerId = handler.getHandlerId();
		
		if(align == null)
		{
			this.align = L2JAlign.Left;
		}
		else
		{
			this.align = align;
		}
	}

	public L2JButton addRegistrable(IRegistrable registrable)
	{
		registrables.add(registrable);
		return this;
	}
	
	public L2JButton addRegistrable(IRegistrable[] registrable)
	{
		for (IRegistrable reg : registrable)
			registrables.add(reg);
		return this;
	}
	
	@Override
	public String draw() 
	{
		String output = "<td";
		
		if (tdWidth != 0)
			output += " width=" + tdWidth + " ";
		
		if (tdAlign != null || tdAlign != "")
			output += " align=" + tdAlign + " ";

		output += ">";
		output +=align.getAlign1();
		
		output +="<button value=\""+name+"\" action=\"bypass -h L2JFrame_"+ handlerId;
		
		for(IRegistrable registrable: registrables)
		{
			output +=" "+registrable.getType()+registrable.getName();
		}
		if(buttonType == L2JButtonType.OTHER)
		{
			output +="\" width="+String.valueOf(width)+" height="+String.valueOf(height)+" back=\""+icon+"\" fore=\""+icon+"\">"; 	
		}
		else
		{
			output +="\" width="+buttonType.getWidth()+" height="+buttonType.getHeight()+" back=\""+buttonType.getFrontType()+"\" fore=\""+buttonType.getType()+"\">"; 	
		}
		output += align.getAlign2();
		output +="</td>";
		
		
		return output;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public L2JButton addArgument(String s)
	{
		registrables.add(new L2JArgument(s));
		return this;
	}
	
	/**
	 * @param width the width to set
	 */
	public void setWidth(int width) {
		this.width = width;
	}

	/**
	 * @return the width
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * @param height the height to set
	 */
	public void setHeight(int height) {
		this.height = height;
	}

	/**
	 * @return the height
	 */
	public int getHeight() {
		return height;
	}

	/**
	 * @param icon the icon to set
	 */
	public void setIcon(String icon) {
		this.icon = icon;
	}

	/**
	 * @return the icon
	 */
	public String getIcon() {
		return icon;
	}

	public enum L2JButtonType 
	{ 
		DEFAULT("66", "21", "L2UI.DefaultButton","L2UI.DefaultButton_click"), 
		GRACIA("135", "21", "L2UI_ct1.button_df","L2UI_ct1.button_df"),
		GRACIA_SMALL("82", "20", "L2UI_ct1.button_df","L2UI_ct1.button_df"),
		GRACIA_SMALL2("66", "21", "L2UI_ct1.button_df","L2UI_ct1.button_df"),
		GRACIA_TOOSMALL("33", "20", "L2UI_ct1.button_df","L2UI_ct1.button_df"),
		QUESTION_MARK_ICON("16", "16", "L2UI_CH3.aboutotpicon", "L2UI_CH3.aboutotpicon_over"),
		OTHER("66", "21", "L2UI.DefaultButton","L2UI.DefaultButton_click");

		String width;
		String height;
		String type;
		String frontType;
		
		L2JButtonType(String _width, String _height , String _type, String _frontType)
		{
			width = _width;
			height = _height;
			type = _type;
			frontType = _frontType;
		}
		
		public String getWidth()
		{
			return width;
		}
		
		public String getHeight()
		{
			return height;
		}
		
		public String getType()
		{
			return type;
		}
		
		public String getFrontType()
		{
			return frontType;
		}
	}
}