package l2jsaver.Features.L2JFrame.models.primitive;

import l2jsaver.Features.L2JFrame.interfaces.IGraphic;
import l2jsaver.Features.L2JFrame.models.L2JAlign;
import l2jsaver.Features.L2JFrame.models.L2JColor;

public class L2JMiniBar implements IGraphic 
{
	private long current;
	private long max;
	private L2JColor backColor;
	private L2JAlign align;
	private int width;
	private String type = "|";
	
	public L2JMiniBar(L2JColor _backColor, long _current, long _max, L2JAlign _align)
	{
		backColor = _backColor;
		current = _current;
		max = _max;
		align = _align;
		
		if(max == 0)
			max = 1;
		if(current > max)
			current = max;
	}
	
	public L2JMiniBar(L2JColor _backColor, long _current, long _max, L2JAlign _align, int width)
	{
		backColor = _backColor;
		current = _current;
		max = _max;
		align = _align;
		this.width = width;
		if(max == 0)
			max = 1;
		if(current > max)
			current = max;
	}
	
	public L2JMiniBar(L2JColor _backColor, long _current, long _max, L2JAlign _align, String type)
	{
		backColor = _backColor;
		current = _current;
		max = _max;
		align = _align;
		this.type = type;
		
		if(max == 0)
			max = 1;
		if(current > max)
			current = max;
	}
	
	public L2JColor getColor(long current , long max )
	{
		if (current/max < 0.5)
		{
			return L2JColor.Red;
		}
		else if(current/max<1)
		{
			return L2JColor.Yellow;
		}
		else 
		{
			return L2JColor.Green;
		}
	}
	
	@Override
	public String draw() 
	{
		String output ="";
		String frontBar="";
		String backBar="";
		int front =(int)( current*8/max);
		int back = (int) ((max - current)*8/max);
		
		for(int i=0;i < front; i++)
		{
			frontBar+=type;
		}
		
		for(int i =0; i<back; i++)
		{
			backBar +=type;
		}
		
		String frontBarFinal = "<font color="+getColor(current, max).getColor()+">"+frontBar+"</font>";
		String backBarFinal = "<font color="+backColor.getColor()+">"+backBar+"</font>";
		
		if(align ==null)
		{
			output = "<td";
			if (width > 0)
				output += " width="+width;
			output += ">"+frontBarFinal + backBarFinal+"</td>";
		}
		else
		{
			output = "<td";
			if (width > 0)
				output += " width="+width;
			output += ">"+align.getAlign1()+frontBarFinal + backBarFinal+align.getAlign2()+"</td>";
		}
		
		return output;
	}
}