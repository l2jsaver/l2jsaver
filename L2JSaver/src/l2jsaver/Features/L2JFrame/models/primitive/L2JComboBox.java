package l2jsaver.Features.L2JFrame.models.primitive;

import java.util.LinkedList;

import l2jsaver.Features.L2JFrame.interfaces.IGraphic;
import l2jsaver.Features.L2JFrame.interfaces.IRegistrable;
import l2jsaver.Features.L2JFrame.models.CompositeGraphic;

public class L2JComboBox implements IGraphic, IRegistrable
{
	private String name;
	private LinkedList<String> values;
	private static int PIXELS_PER_CHAR = 8;
	
	public L2JComboBox( String name)
	{
		this.name = name;
		values = new LinkedList<String>();
	}
	
	public L2JComboBox addChoice(String s)
	{
		values.add(s);
		return this;
	}
	
	@Override
	public String draw() 
	{
		String output ="<td><combobox ";
		output +=getWidth();
		output += ("height=17 var="+name+ " ");
		output += "list=";
		
		for(String s: values)
		{
			output+= (s+";");
		}
		output += "></td>";

		return output;
	}
	
	public String getWidth()
	{
		int maxSize =7;
		for(String s: values)
		{
			if(s.length()*PIXELS_PER_CHAR >maxSize)
				maxSize = s.length()*PIXELS_PER_CHAR;
		}
		maxSize +=14;
		return ("width="+String.valueOf(maxSize)+ " ");
	}
	
	public L2JComboBox toPanel(CompositeGraphic panel)
	{
		panel.addGraphic(this);
		return this;
	}
	
	@Override
	public String getName()
	{
		// TODO Auto-generated method stub
		return name;
	}

	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return "$";
	}
}