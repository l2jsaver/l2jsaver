package l2jsaver.Features.L2JFrame.models.primitive;

import l2jsaver.Features.L2JFrame.interfaces.IRegistrable;

public class L2JArgument implements IRegistrable{

	public String name;
	
	public L2JArgument(String s)
	{
		name = s;
	}
	
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return name;
	}

	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return "";
	}
}