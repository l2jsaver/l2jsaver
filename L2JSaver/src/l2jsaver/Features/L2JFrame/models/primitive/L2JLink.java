package l2jsaver.Features.L2JFrame.models.primitive;

import java.util.ArrayList;
import java.util.List;

import l2jsaver.Features.L2JFrame.AbstractController;
import l2jsaver.Features.L2JFrame.interfaces.IGraphic;
import l2jsaver.Features.L2JFrame.interfaces.IRegistrable;
import l2jsaver.Features.L2JFrame.models.CompositeGraphic;
import l2jsaver.Features.L2JFrame.models.L2JAlign;
import l2jsaver.Features.L2JFrame.models.L2JColor;

public class L2JLink implements IGraphic
{	
	private String name;
	private L2JColor background;
	private L2JColor fontColor;
	private String handlerId="";
	private L2JAlign align;
	private List<IRegistrable> registrables = new ArrayList<IRegistrable>();
	
	public L2JLink(String _name, AbstractController handler, L2JAlign align)
	{
		name = _name;
		if(handler != null)
		handlerId = handler.getHandlerId();
		
		if(align == null)
		{
			this.align = L2JAlign.Left;
		}
		else
		{
			this.align = align;
		}
	}
	
	public L2JLink(String _name, AbstractController handler, L2JAlign align,L2JColor fontColor)
	{
		name = _name;
		this.fontColor = fontColor;
		if(handler != null)
		handlerId = handler.getHandlerId();
		
		if(align == null)
		{
			this.align = L2JAlign.Left;
		}
		else
		{
			this.align = align;
		}
	}
	
	public L2JLink toPanel(CompositeGraphic panel)
	{
		panel.addGraphic(this);
		return this;
	}
	
	@Override
	public String draw() 
	{
		String output = "<td bgcolor="+background+">";
		output +=align.getAlign1();
		
		if(fontColor != null)
			output +="<font color="+fontColor.getColor()+">";
		
		output +="<a action=\"bypass -h L2JFrame_"+ handlerId;
		
		for(IRegistrable registrable: registrables)
		{
			output +=" "+registrable.getType()+registrable.getName();
		}
		
		output +="\">"; 
		
		output += name;
		
		if(fontColor != null)
			output +="</font>";
		output += align.getAlign2();
		output +="</td>";
		
		return output;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public void addRegistrable(IRegistrable registrable)
	{
		registrables.add(registrable);
	}
	
	
	public void addArgument(String s)
	{
		registrables.add(new L2JArgument(s));
	}
	
	public enum L2JButtonType 
	{ 
		DEFAULT("66", "21", "L2UI.DefaultButton","L2UI.DefaultButton_click"), 
		GRACIA("135", "21", "L2UI.CT1Button","L2UI.CT1DefaultButton"); 
		
		String width;
		String height;
		String type;
		String frontType;
		
		L2JButtonType(String _width, String _height , String _type, String _frontType)
		{
			width = _width;
			height = _height;
			type = _type;
			frontType = _frontType;
		}
		
		public String getWidth()
		{
			return width;
		}
		
		public String getHeight()
		{
			return height;
		}
		
		public String getType()
		{
			return type;
		}
		
		public String getFrontType()
		{
			return frontType;
		}
	}
}