package l2jsaver.Features.L2JFrame.models.primitive;

import l2jsaver.Features.L2JFrame.interfaces.IGraphic;

public class L2JImage implements IGraphic{

	public String width;
	public String height;
	public String source;
	
	public L2JImage(String source, String width, String height)
	{
		this.source = source;
		this.width = width;
		this.height = height;
	}
	
	@Override
	public String draw() 
	{
		String output = "<td>";
		output += "<img src="+source+" width="+width+" height="+height+"></td>";
		return output;
	}
}