package l2jsaver.Features.L2JFrame.models.primitive;

import l2jsaver.Features.L2JFrame.interfaces.IGraphic;
import l2jsaver.Features.L2JFrame.interfaces.IRegistrable;
import l2jsaver.Features.L2JFrame.models.CompositeGraphic;

public class L2JTextBox implements IGraphic, IRegistrable
{
	private String name;
	private boolean isNumber;
	
	public L2JTextBox( String name)
	{
		this.name = name;
		this.isNumber = false;
	}
	
	public L2JTextBox( String name, boolean isNumber)
	{
		this.name = name;
		this.isNumber = isNumber;
	}
	
	public L2JTextBox toPanel(CompositeGraphic panel)
	{
		panel.addGraphic(this);
		return this;
	}
	
	@Override
	public String draw() 
	{
		String output ="<td><edit var=\""+ name +"\" ";
		if (isNumber)
			output += "type=number";
		output += "></td>";
		
		return output;
	}
	
	@Override
	public String getName()
	{
		// TODO Auto-generated method stub
		return name;
	}

	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return "$";
	}
}