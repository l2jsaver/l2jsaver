/*
 * Authors: Issle, Howler, David
 * File: L2JNewLine.java
 */
package l2jsaver.Features.L2JFrame.models.primitive;

import l2jsaver.Features.L2JFrame.interfaces.IGraphic;

/**
 * @author Matim
 * Simply html <br> 
 */
public class L2JNewLine implements IGraphic
{
	public L2JNewLine()
	{
		
	}
	
	@Override
	public String draw()
	{
		String output ="<br>";
		return output;
	}
}