package l2jsaver.Features.L2JFrame.models.primitive;

import l2jsaver.Features.L2JFrame.interfaces.IGraphic;
import l2jsaver.Features.L2JFrame.models.L2JAlign;
import l2jsaver.Features.L2JFrame.models.L2JColor;

public class L2JProgressBar implements IGraphic 
{
	private int current;
	private int max;
	private L2JColor frontColor;
	private L2JColor backColor;
	private L2JAlign align;
	private int width;
	private String type = "|";
	public L2JProgressBar(L2JColor _frontColor, L2JColor _backColor, int _current, int _max, L2JAlign _align)
	{
		frontColor = _frontColor;
		backColor = _backColor;
		current = _current;
		max = _max;
		align = _align;
		
		if(max == 0)
			max = 1;
		if(current > max)
			current = max;
	}
	
	public L2JProgressBar(L2JColor _frontColor, L2JColor _backColor, int _current, int _max, L2JAlign _align, int width)
	{
		frontColor = _frontColor;
		backColor = _backColor;
		current = _current;
		max = _max;
		align = _align;
		this.width = width;
		
		if(max == 0)
			max = 1;
		if(current > max)
			current = max;
	}
	
	public L2JProgressBar(L2JColor _frontColor, L2JColor _backColor, int _current, int _max, L2JAlign _align, String type)
	{
		frontColor = _frontColor;
		backColor = _backColor;
		current = _current;
		max = _max;
		align = _align;
		this.type = type;
		
		if(max == 0)
			max = 1;
		if(current > max)
			current = max;
	}
	
	@Override
	public String draw() 
	{
		String output ="";
		String frontBar="";
		String backBar="";
		int front = current*40/max;
		int back = (max - current)*40/max;
		
		for(int i=0;i < front; i++)
		{
			frontBar+=type;
		}
		
		for(int i =0; i<back; i++)
		{
			backBar +=type;
		}
		
		String frontBarFinal = "<font color="+frontColor.getColor()+">"+frontBar+"</font>";
		String backBarFinal = "<font color="+backColor.getColor()+">"+backBar+"</font>";
		
		if(align ==null)
		{
			output = "<td";
			if (width > 0)
				output += " width="+width;
			output += ">"+frontBarFinal + backBarFinal+"</td>";
		}
		else
		{
			output = "<td";
			if (width > 0)
				output += " width="+width;
			output += ">"+align.getAlign1()+frontBarFinal + backBarFinal+align.getAlign2()+"</td>";
		}
		
		return output;
	}
}