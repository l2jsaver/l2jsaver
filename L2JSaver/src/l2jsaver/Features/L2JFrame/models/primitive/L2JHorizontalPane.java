package l2jsaver.Features.L2JFrame.models.primitive;

import l2jsaver.Features.L2JFrame.interfaces.IGraphic;
import l2jsaver.Features.L2JFrame.models.CompositeGraphic;
import l2jsaver.Features.L2JFrame.models.L2JAlign;

public class L2JHorizontalPane extends CompositeGraphic implements IGraphic{

	L2JAlign align;
	
	public L2JHorizontalPane(L2JAlign align)
	{
		super();
		this.align = align;
	}
	@Override
	public String draw() 
	{
		String output ="";
		if(align != null)
			output +=align.getAlign1();
		for(IGraphic graphic: graphics)
		{
			output+= (graphic.draw());
		}
		
		if(align != null)
			output +=align.getAlign2();
		return output;
	}
	
	/**
	 * Adds this object to the input panel.
	 * @param panel
	 * @return
	 */
	public L2JHorizontalPane toPanel(L2JVerticalPane panel)
	{
		panel.addGraphic(this);
		return this;
	}
}