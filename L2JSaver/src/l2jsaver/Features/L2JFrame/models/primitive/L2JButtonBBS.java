/*
 * Authors: Issle, Howler, Matim
 * File: L2JButtonBBS.java
 */
package l2jsaver.Features.L2JFrame.models.primitive;

import java.util.ArrayList;
import java.util.List;

import l2jsaver.Features.L2JFrame.AbstractController;
import l2jsaver.Features.L2JFrame.interfaces.IGraphic;
import l2jsaver.Features.L2JFrame.interfaces.IRegistrable;
import l2jsaver.Features.L2JFrame.models.CompositeGraphic;
import l2jsaver.Features.L2JFrame.models.L2JAlign;
import l2jsaver.Features.L2JFrame.models.L2JColor;

/**
 * @author Howler
 *
 */
public class L2JButtonBBS implements IGraphic 
{

	private String name;
	private L2JColor background;
	private L2JButtonBBSType buttonType;
	private String handlerId="";
	private L2JAlign align;
	private int width;
	private List<IRegistrable> registrables = new ArrayList<IRegistrable>();
	
	public L2JButtonBBS(String _name, L2JButtonBBSType _buttonType, AbstractController handler, L2JAlign align, int width)
	{
		name = _name;
		buttonType = _buttonType;
		if(handler != null)
		handlerId = handler.getHandlerId();
		this.width = width;
		
		if(align == null)
		{
			this.align = L2JAlign.Left;
		}
		else
		{
			this.align = align;
		}
	}
	
	public L2JButtonBBS(String _name, L2JButtonBBSType _buttonType, AbstractController handler, L2JAlign align)
	{
		name = _name;
		buttonType = _buttonType;
		if(handler != null)
		handlerId = handler.getHandlerId();
		
		if(align == null)
		{
			this.align = L2JAlign.Left;
		}
		else
		{
			this.align = align;
		}
	}
	
	/**
	 * Constructs a button with the given name and controller.
	 * Pre-built arguments are Gracia button and center align.
	 * @param _name
	 * @param handler
	 */
	public L2JButtonBBS(String _name, AbstractController handler, int width)
	{
		name = _name;
		buttonType = L2JButtonBBSType.GRACIA;
		this.width = width;
		if(handler != null)
		handlerId = handler.getHandlerId();
		this.align = L2JAlign.Center;
	}
	
	/**
	 * Constructs a button with the given name,controller and argument.
	 * Pre-built arguments are Gracia button and center align.
	 * @param _name
	 * @param handler
	 */
	public L2JButtonBBS(String _name, AbstractController handler, String argument, int width)
	{
		name = _name;
		buttonType = L2JButtonBBSType.GRACIA;
		this.width = width;
		if(handler != null)
		handlerId = handler.getHandlerId();
		this.align = L2JAlign.Center;
		addArgument(argument);
	}
	
	/**
	 * Registers this graphic to the given panel input.
	 * @param panel
	 */
	public L2JButtonBBS toPanel(CompositeGraphic panel)
	{
		panel.addGraphic(this);
		return this;
	}
	
	public L2JButtonBBS(String _name, L2JButtonBBSType _buttonType, AbstractController handler, L2JAlign align,L2JColor background, int width)
	{
		name = _name;
		this.background = background;
		buttonType = _buttonType;
		this.width = width;
		if(handler != null)
		handlerId = handler.getHandlerId();
		
		if(align == null)
		{
			this.align = L2JAlign.Left;
		}
		else
		{
			this.align = align;
		}
	}

	public L2JButtonBBS addRegistrable(IRegistrable registrable)
	{
		registrables.add(registrable);
		return this;
	}
	
	public L2JButtonBBS addRegistrable(IRegistrable[] registrable)
	{
		for (IRegistrable reg : registrable)
			registrables.add(reg);
		return this;
	}
	
	@Override
	public String draw() 
	{
		String output = "";
		if (width >= 0)
			output += "<td width="+width;
		else
			output += "<td";
		if (background != null)
			output += " bgcolor="+background.getColor();
		output += ">";
		output +=align.getAlign1();
		
		output +="<button value=\""+name+"\" action=\"bypass -h _bbsFrame-"+ handlerId;
		
		for(IRegistrable registrable: registrables)
		{
			output +=" "+registrable.getType()+registrable.getName();
		}
		
		output +="\" width="+buttonType.getWidth()+" height="+buttonType.getHeight()+" back=\""+buttonType.getFrontType()+"\" fore=\""+buttonType.getType()+"\">"; 	
		output += align.getAlign2();
		output +="</td>";
		
		return output;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public L2JButtonBBS addArgument(String s)
	{
		registrables.add(new L2JArgument(s));
		return this;
	}
	
	public enum L2JButtonBBSType 
	{ 
		DEFAULT("66", "21", "L2UI.DefaultButton","L2UI.DefaultButton_click"), 
		GRACIA("135", "21", "L2UI_ct1.button_df","L2UI_ct1.button_df"),
		GRACIA_SMALL("82", "20", "L2UI_ct1.button_df","L2UI_ct1.button_df"),
		GRACIA_SMALL2("66", "21", "L2UI_ct1.button_df","L2UI_ct1.button_df"),
		GRACIA_TOOSMALL("33", "20", "L2UI_ct1.button_df","L2UI_ct1.button_df"),
		QUESTION_MARK_ICON("16", "16", "L2UI_CH3.aboutotpicon", "L2UI_CH3.aboutotpicon_over");

		String width;
		String height;
		String type;
		String frontType;
		
		L2JButtonBBSType(String _width, String _height , String _type, String _frontType)
		{
			width = _width;
			height = _height;
			type = _type;
			frontType = _frontType;
		}
		
		public String getWidth()
		{
			return width;
		}
		
		public String getHeight()
		{
			return height;
		}
		
		public String getType()
		{
			return type;
		}
		
		public String getFrontType()
		{
			return frontType;
		}
	}

}
