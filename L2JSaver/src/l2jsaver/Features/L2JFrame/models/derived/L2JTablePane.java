package l2jsaver.Features.L2JFrame.models.derived;

import java.util.Collection;

import l2jsaver.Features.L2JFrame.AbstractController;
import l2jsaver.Features.L2JFrame.models.L2JAlign;
import l2jsaver.Features.L2JFrame.models.primitive.L2JButton;
import l2jsaver.Features.L2JFrame.models.primitive.L2JButton.L2JButtonType;
import l2jsaver.Features.L2JFrame.models.primitive.L2JButtonBBS;
import l2jsaver.Features.L2JFrame.models.primitive.L2JButtonBBS.L2JButtonBBSType;
import l2jsaver.Features.L2JFrame.models.primitive.L2JHorizontalPane;
import l2jsaver.Features.L2JFrame.models.primitive.L2JRootPane;
import l2jsaver.Features.L2JFrame.models.primitive.L2JText;
import l2jsaver.Features.L2JFrame.models.primitive.L2JVerticalPane;

/**
 * Classes that wanna implement the paging system
 * should extend this class. Do NOT use this without
 * extending it first.
 * @author Issle
 *
 */
public class L2JTablePane extends L2JVerticalPane
{
	private L2JVerticalPane navigationMenu = new L2JVerticalPane();
	
	public L2JTablePane(Collection<L2JHorizontalPane> list, int rowsPerPage, int page, AbstractController controller)
	{
		super();
		
		if(list.size()==0)
			return;
		int _page = getCanonicalPage(page, rowsPerPage, list);
		int offset = rowsPerPage *_page;
		
		L2JHorizontalPane[] panels = new L2JHorizontalPane[list.size()];
		panels = list.toArray(panels);
		
		for(int i = offset; i< offset + rowsPerPage; i++ )
		{
			if(i>= panels.length)
				break;
			addGraphic(panels[i]);
		}
		
		L2JHorizontalPane navigation = new L2JHorizontalPane(L2JAlign.Center);
		L2JButton previous = new L2JButton("<<", L2JButtonType.GRACIA_SMALL2, controller, L2JAlign.Center);
		previous.addArgument("Previous");
		previous.addArgument(String.valueOf(_page));
		L2JButton next = new L2JButton(">>", L2JButtonType.GRACIA_SMALL2, controller, L2JAlign.Center);
		next.addArgument("Next");
		next.addArgument(String.valueOf(_page));
		L2JText pageCount = new L2JText("Page "+ String.valueOf(_page+1)+" of "+String.valueOf(getMaxPage(list.size(), rowsPerPage)+1));
		
		navigation.addGraphic(previous);
		navigation.addGraphic(pageCount);
		navigation.addGraphic(next);
		navigationMenu.addGraphic(navigation);
	}
	
	public L2JTablePane(Collection<L2JHorizontalPane> list, int rowsPerPage, int page, AbstractController controller, int width)
	{
		super(width);
		
		if(list.size()==0)
			return;
		int _page = getCanonicalPage(page, rowsPerPage, list);
		int offset = rowsPerPage *_page;
		
		L2JHorizontalPane[] panels = new L2JHorizontalPane[list.size()];
		panels = list.toArray(panels);
		
		for(int i = offset; i< offset + rowsPerPage; i++ )
		{
			if(i>= panels.length)
				break;
			addGraphic(panels[i]);
		}
		
		L2JHorizontalPane navigation = new L2JHorizontalPane(L2JAlign.Center);
		L2JButtonBBS previous = new L2JButtonBBS("<<", L2JButtonBBSType.GRACIA_SMALL2, controller, L2JAlign.Center);
		previous.addArgument("Previous");
		previous.addArgument(String.valueOf(_page));
		L2JButtonBBS next = new L2JButtonBBS(">>", L2JButtonBBSType.GRACIA_SMALL2, controller, L2JAlign.Center);
		next.addArgument("Next");
		next.addArgument(String.valueOf(_page));
		L2JText pageCount = new L2JText("Page "+ String.valueOf(_page+1)+" of "+String.valueOf(getMaxPage(list.size(), rowsPerPage)+1));
		
		navigation.addGraphic(previous);
		navigation.addGraphic(pageCount);
		navigation.addGraphic(next);
		navigationMenu.addGraphic(navigation);
	}
	
	public L2JTablePane toPanel(L2JRootPane pane)
	{
		pane.addGraphic(this);
		pane.addGraphic(getNavigationMenu());
		
		return this;
	}
	/**
	 * Filters the page incase its out of the limmits.
	 * @param page
	 * @param rowsPerPage
	 * @param list
	 * @return
	 */
	public int getCanonicalPage(int page, int rowsPerPage, Collection<L2JHorizontalPane> list)
	{
		int canonicalPage = page;
		int maxPage = getMaxPage(list.size(), rowsPerPage);
		if(page < 0)
			canonicalPage =maxPage;
		else if(page> maxPage)
			canonicalPage = 0;
		
		return canonicalPage;
		
	}
	
	/**
	 * This returns the biggest possible page
	 * counting from zero. So if there are 10 pages
	 * max page is 9.
	 * @param listSize
	 * @param rowsPerPage
	 * @return
	 */
	public int getMaxPage(int listSize, int rowsPerPage)
	{
		int numOfPages = listSize/rowsPerPage;
		
		if(listSize%rowsPerPage != 0)
			numOfPages++;
		if(numOfPages < 1)
			numOfPages = 1;
		return numOfPages-1;
	}

	/**
	 * @param navigationMenu the navigationMenu to set
	 */
	public void setNavigationMenu(L2JVerticalPane navigationMenu) {
		this.navigationMenu = navigationMenu;
	}

	/**
	 * @return the navigationMenu
	 */
	public L2JVerticalPane getNavigationMenu() {
		return navigationMenu;
	}
}