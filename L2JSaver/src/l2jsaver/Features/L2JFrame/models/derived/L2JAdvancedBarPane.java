package l2jsaver.Features.L2JFrame.models.derived;

import l2jsaver.Features.L2JFrame.models.L2JAlign;
import l2jsaver.Features.L2JFrame.models.L2JColor;
import l2jsaver.Features.L2JFrame.models.primitive.L2JProgressBar;
import l2jsaver.Features.L2JFrame.models.primitive.L2JText;
import l2jsaver.Features.L2JFrame.models.primitive.L2JVerticalPane;

public class L2JAdvancedBarPane extends L2JVerticalPane
{
	public L2JText text;
	public L2JProgressBar bar;
	public L2JText afterText;
	
	public L2JAdvancedBarPane(L2JColor background, L2JColor _text, L2JColor _bar, L2JAlign align, int current, int max, String message)
	{
		super(background);
		if(max ==0)
			max =1;
		text = new L2JText(_text, align, message);
		bar = new L2JProgressBar(_bar, _text, current, max, align);
		afterText = new L2JText(_text,align,"Progress :"+ String.valueOf((100*current/max))+"% ("+String.valueOf(current)+"/"+String.valueOf(max)+")");
		afterText.highlightText(String.valueOf(max), L2JColor.White);
		afterText.highlightText("%", L2JColor.White);
		addGraphic(text);
		addGraphic(bar);
		addGraphic(afterText);
	}
}