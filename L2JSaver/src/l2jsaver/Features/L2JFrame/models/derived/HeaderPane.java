package l2jsaver.Features.L2JFrame.models.derived;

import l2jsaver.Features.L2JFrame.models.L2JColor;
import l2jsaver.Features.L2JFrame.models.primitive.L2JRootPane;
import l2jsaver.Features.L2JFrame.models.primitive.L2JVerticalPane;

public class HeaderPane extends L2JRootPane
{
	public L2JVerticalPane headerPane;
	public L2JVerticalPane buttonPane;
	//public Header header;
	
	public HeaderPane(String charName)
	{
		headerPane = new L2JVerticalPane(L2JColor.Black);
		buttonPane = new L2JVerticalPane(L2JColor.Black);
		//header = new Header(charName);
		
		//headerPane.addGraphic(header);
		//buttonPane.addGraphic(ButtonPane1.getInstance());
		//buttonPane.addGraphic(ButtonPane2.getInstance());
		
		addGraphic(headerPane);
		addGraphic(buttonPane);
	}
}