/*
 * Authors: Issle, Howler, David
 * File: PagingController.java
 */
package l2jsaver.Features.L2JFrame.models.derived;

import java.util.Map;

import org.l2jmobius.gameserver.model.actor.instance.PlayerInstance;

import l2jsaver.Features.L2JFrame.AbstractController;
import l2jsaver.Features.L2JFrame.L2JFrame;
import l2jsaver.Features.L2JFrame.models.primitive.L2JRootPane;

/**
 * @author Issle
 *
 * This abstract controller handles the appearance of a set
 * of windows that share the same structure ( L2JRootPane )
 * but contain multiple pages.
 */
public abstract class PagingController extends AbstractController{

	
	@Override
	public void execute(PlayerInstance activeChar, Map<Integer, String> arguments) 
	{
		String s = arguments.get(0);
		int currentPage=0;
		try{currentPage = Integer.parseInt(arguments.get(1));}catch(Exception e){e.printStackTrace();}
		int targetPage = currentPage;
		
		if(s == null)
		{
			System.out.println("Something bad happened.");
			return;
		}
		
		if(s.equals("Previous"))
		{
			targetPage--;
		}
		else if(s.equals("Next"))
		{
			targetPage++;
		}
		
		L2JFrame.getInstance().send(activeChar, getNewWindow(targetPage, activeChar));
	}

	@Override
	public void execute(PlayerInstance activeChar) 
	{
		
	}
	
	/**
	 * This should reconstruct a new window based on the information
	 * it gets.
	 * @param page
	 * @param rowsPerPage
	 * @param list
	 * @param args
	 * @return
	 */
	public abstract L2JRootPane getNewWindow(int page, PlayerInstance activeChar);
	
	/**
	 * If a character can get a window of this type.
	 * @param activeChar
	 * @return
	 */
	public abstract boolean conditionsOk(PlayerInstance activeChar);

}
