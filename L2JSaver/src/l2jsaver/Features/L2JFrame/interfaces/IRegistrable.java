package l2jsaver.Features.L2JFrame.interfaces;

public interface IRegistrable {
	
	public String getName();
	
	public String getType();
}
