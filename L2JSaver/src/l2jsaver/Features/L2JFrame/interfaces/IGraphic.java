package l2jsaver.Features.L2JFrame.interfaces;

/**
 * All components of the L2JFrame should 
 * implement this interface in order to be
 * able to get drawn.
 * @author Issle
 *
 */
public interface IGraphic 
{
	public String draw();
}