package l2jsaver.Features.L2JFrame;


import java.util.Map;

import org.l2jmobius.gameserver.model.actor.instance.PlayerInstance;


public abstract class AbstractController 
{
	private String handlerId;
	private boolean isAdmin=false;
	
	public abstract void execute(PlayerInstance activeChar, Map<Integer, String> arguments);

	public abstract void execute(PlayerInstance activeChar);
	
	/**
	 * @param handlerId the handlerId to set
	 */
	public void setHandlerId(String handlerId)
	{
		this.handlerId = handlerId;
	}

	/**
	 * @return the handlerId
	 */
	public String getHandlerId()
	{
		return handlerId;
	}

	/**
	 * @param isAdmin the isAdmin to set
	 */
	public void setAdmin() {
		this.isAdmin = true;
	}

	/**
	 * @return the isAdmin
	 */
	public boolean isAdmin() {
		return isAdmin;
	}
}