/*
 * Authors: Issle, Howler, Matim
 * File: AntifeedManager.java
 */
package l2jsaver.Features.Olympiad;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.l2jmobius.gameserver.model.World;
import org.l2jmobius.gameserver.model.actor.instance.PlayerInstance;

/**
 * @author Issle
 *
 */
public class AntifeedManager {

	private static Map<Integer, Map<Integer,Integer>> priorities;
	private volatile static AntifeedManager singleton;
	
	private static final int PRIORITY_DOWN =100;

	public void onOlyStart()
	{
		priorities.clear();
	}
	
	private void updatePriorities(PlayerInstance player1, PlayerInstance player2)
	{
		if(!priorities.containsKey(player1.getObjectId()))
			priorities.put(player1.getObjectId(), new HashMap<Integer,Integer>());
		else
		{
			Map<Integer,Integer> myPriorities = priorities.get(player1.getObjectId());
			
			if(myPriorities.containsKey(player2.getObjectId()))
			{
				myPriorities.put(player2.getObjectId(), myPriorities.get(player2.getObjectId()+1));
			}
			else
				myPriorities.put(player2.getObjectId(), 0);
		}
	}
	
	public void onMatchStart(PlayerInstance player1, PlayerInstance player2)
	{
		updatePriorities(player1, player2);
		updatePriorities(player2, player1);
	}
	
	/**
	 * The priority is counted from zero to ...
	 * Zero is the best priority. For every match that our
	 * player has done with this player he gets a +1 penalty 
	 * on priority. If they have the same ip they get a PRIORITY_DOWN
	 * penalty also.
	 * 
	 * TODO: Check the enemy priority list for penalizing. A big priority
	 * list means this player is an active competitor cause he has done
	 * lots of participants.
	 * 
	 * @param activeChar
	 * @param enemy
	 * @return
	 */
	private int getPriority(PlayerInstance activeChar, PlayerInstance enemy)
	{
		int priority = 0;
		String ip1;
		String ip2;
		
		ip1 = activeChar.getClient().getConnectionAddress().toString();
		ip2 = enemy.getClient().getConnectionAddress().toString();
		if(ip1.equals(ip2))
			priority +=PRIORITY_DOWN;
		
		if(priorities.containsKey(activeChar.getObjectId()))
		{
			Map<Integer,Integer> myPriorities = priorities.get(activeChar.getObjectId());
			if(myPriorities.containsKey(enemy.getObjectId()))
				priority += myPriorities.get(enemy.getObjectId());
		}
		
		return priority;
		
	}
	
	/*
	 * Returns the player id from the list that has the best priority
	 * which is counted by the times you played with him and his IP
	 * status.
	 */
	public int getPlayerTwo(List<Integer> players, PlayerInstance activeChar)
	{
		int bestObjectId =0;
		int bestPriority =9999999;
		for(int objectId: players)
		{
			PlayerInstance player = World.getInstance().getPlayer(objectId);
			if(player == null)
			{
				players.remove(objectId);
				continue;
			}
			
			int playerPriority = getPriority(activeChar, player);
			
			if(playerPriority< bestPriority)
			{
				bestObjectId = player.getObjectId();
				bestPriority = playerPriority;
			}
			
		}
		
		return bestObjectId;
	}
	
	private AntifeedManager() {
		priorities = new HashMap<Integer, Map<Integer,Integer>>();
	}

	public static AntifeedManager getInstance() {
		if (singleton == null) {
			synchronized (AntifeedManager.class) {
				if (singleton == null)
					singleton = new AntifeedManager();
			}
		}
		return singleton;
	}

}
