/*
 * Authors: Issle, Howler, Matim
 * File: ServerInfo.java
 */
package l2jsaver.Features.IssleMisc;

import org.l2jmobius.gameserver.model.actor.instance.PlayerInstance;

import l2jsaver.Features.L2JFrame.models.L2JAlign;
import l2jsaver.Features.L2JFrame.models.L2JColor;
import l2jsaver.Features.L2JFrame.models.primitive.L2JRootPane;
import l2jsaver.Features.L2JFrame.models.primitive.L2JText;
import l2jsaver.Features.L2JFrame.models.primitive.L2JVerticalPane;

/**
 * @author Issle
 *
 */
public class ServerInfo extends L2JRootPane{

	public ServerInfo(String title, String content, PlayerInstance player, String sender)
	{
		super(title);
		L2JVerticalPane panel = new L2JVerticalPane();
		new L2JText(L2JColor.Green,L2JAlign.Center,"-----------------------------------------------------").toPanel(panel);
		new L2JText(content).toPanel(panel);
		new L2JText(L2JColor.Green,L2JAlign.Center,"-----------------------------------------------------").toPanel(panel);
		new L2JText("        Sender: "+sender).toPanel(panel);
		addGraphic(panel);
	}
	
}
