package l2jsaver.Features.IssleMisc;

import java.util.HashMap;
import java.util.Map;

import org.l2jmobius.gameserver.model.actor.instance.PlayerInstance;

import l2jsaver.interfaces.ISaverCommandHandler;

/**
 * @author Issle
 *
 */
public class VoicedCommands implements ISaverCommandHandler
{
	public  Map<String, Integer> commands = new HashMap<String, Integer>();
	
	public void reload()
	{
		commands = new HashMap<String, Integer>();
	}
	
	private volatile static VoicedCommands singleton;
	
	private VoicedCommands()
	{
		commands = new HashMap<String, Integer>();
	}
	
	public static VoicedCommands getInstance()
	{
		if (singleton == null)
		{
			synchronized (VoicedCommands.class)
			{
				if (singleton == null)
					singleton = new VoicedCommands();
			}
		}
		return singleton;
	}

	@Override
	public boolean useVoicedCommand(String command, PlayerInstance activeChar, String params)
	{
		String output = "Available commands:";
		for(String s: commands.keySet())
		{
			output = (" ?"+s);
		}
		
		activeChar.sendMessage(output);
		return false;
	}

	@Override
	public String[] getVoicedCommandList()
	{
		String[] command = new String[1];
		command[0] = "commandhelp";
		return command;
	}

	@Override
	public boolean forGM() 
	{
		return false;
	}
}