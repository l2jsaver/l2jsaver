package l2jsaver.Features.IssleMisc.CraftSystem;

import org.l2jmobius.gameserver.model.items.instance.ItemInstance;

public class CraftController 
{
	private static int armorEnchant = 15;
	private static int weaponEnchant = 20;
	
	//For masterwork it can get bonus.
	private static int bonus = 5;
	public static boolean fullChance = true;
	
	/**
	 * Adds a feature which allows a crafter item to be crafted with pre-enchanted attributes.
	 * @param item the crafted item
	 */
	public static void handleCraft(ItemInstance item, boolean isRare)
	{
		int enchant =0;
		if(item.isArmor())
			enchant += armorEnchant;
		else if(item.isWeapon())
			enchant += weaponEnchant;
		if(isRare)
			enchant += bonus ;
		if(item.isArmor() || item.isWeapon())	
			item.setEnchantLevel(enchant);
	}
}