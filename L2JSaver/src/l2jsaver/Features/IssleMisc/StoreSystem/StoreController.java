package l2jsaver.Features.IssleMisc.StoreSystem;

import java.util.Collection;

import org.l2jmobius.gameserver.model.World;
import org.l2jmobius.gameserver.model.actor.instance.NpcInstance;
import org.l2jmobius.gameserver.model.actor.instance.PlayerInstance;

import l2jsaver.Features.IssleMisc.RandomTips;

public class StoreController 
{
	private static final int radiusPlayers = 70;
	private static final int radiusNpc = 300;
	
	public static void load()
	{
		RandomTips.registerTip("[Stores:]To open a private shop, you need to be in some distance from other shops, to prevent overcrowded area lags.");
	}
	
	/**
	 * If there is
	 *  a player in radius that has a store,
	 * do not allow this player to open a new one.
	 * @param activeChar
	 * @return
	 */
	public static boolean blockShop(PlayerInstance activeChar)
	{
		Collection<PlayerInstance> players = World.getInstance().getVisibleObjectsInRange(activeChar, PlayerInstance.class, radiusPlayers);
		
		for(PlayerInstance player: players)
		{
			if(player.isInStoreMode())
			{
				activeChar.sendMessage("Your store is too close to another store, move a bit further away.");
				return true;
			}
		}
		
		Collection<NpcInstance> npcs = World.getInstance().getVisibleObjectsInRange(activeChar, NpcInstance.class, radiusNpc);
		
		if (!npcs.isEmpty())
		{
			activeChar.sendMessage("Your shop is too close to an NPC, move a bit further away.");
			return true;
		}
		
		return false;	
	}
}