package l2jsaver.Features.IssleMisc;

import java.util.ArrayList;
import java.util.List;

import org.l2jmobius.commons.util.Rnd;
import org.l2jmobius.gameserver.model.actor.Playable;
import org.l2jmobius.gameserver.model.actor.instance.PlayerInstance;

import l2jsaver.Features.L2JFrame.L2JFrame;
import l2jsaver.Features.L2JFrame.models.primitive.L2JRootPane;

public class RandomTips
{
	
	public static void sendInfoOnLevelUp(Playable playable, int value)
	{
		L2JRootPane panel = null;
		PlayerInstance player = null;
		
		if(!(playable instanceof PlayerInstance))
				return;
		
		player = (PlayerInstance)playable;
		
		
		if(crossingLevel(player,30,value))
		{
			panel = new ServerInfo("Meeting the stranger ...","" +
					"Long, long ago, in a time all but lost to memory, he whos name must not be spoken," +
					"developed a spell. A chant that was able to transform the money into magical treasure boxes. " +
					"Those magically transformed chests would increase the money " +
					"through the lapse of the vast time and make thy prosper and rich. And for thy, it was spelled ?hide ... ", player, "");
		}
		else if(crossingLevel(player,40,value))
		{
			panel = new ServerInfo("The influence to the mass ...","" +
					"This is how the hunt for the treasures started. Aspiring individuals started diving into the deepest " +
					"caves, climbing the highest mountains to hide their treasure or to steal other individuals treasures. " +
					"To inspect the treasure, the treasure hunters by the help of him,  developed a new spell that could inform them about the status " +
					"of their precious treasure. And its name ..... ?treasure", player, "Admin");
		}
		else if(crossingLevel(player,50,value))
		{
			panel = new ServerInfo("The mythical entities, Dragons","" +
					"The hunting for treasures unstabilized the world. " +
					"Mythical beings long hidden in the caves got awakened by the wreckless " +
					"treasure hunters. And their names, Dragons. From then on, dragons vandalize " +
					"our world, destroy our towns and spread " +
					"fear and terror in the land ...", player, "Admin");
		}
		else if(crossingLevel(player,60,value))
		{
			panel = new ServerInfo("To defeat the dragons.","" +
					"And that kept going on and on and people started believing that it is through evolution and combat prime that " +
					"the dragons defeat would happen. So he developed a spell to help the people. A spell that would grant greatness and wealth to the one that would " +
					"excel in combat. And the name of the spell was ?stat" +
					"",player, "Admin");
		}
		else if(crossingLevel(player,64,value))
		{
			panel = new ServerInfo("And he slayed the dragon ...","Using ?stat many individuals exceled in combat. And it was decided " +
					"that the dragons that invade the city every now and then, should be eliminated. The first encounter happened a year " +
					"later. Three thousand warriors attacked the dragon. And they all fall by the sharpness of his teeth and the glory of his flame. " +
					"All but one. He who mastered the spells fought back the first dragon with a legendary heart and a firey spirit. And when " +
					"the dragon died, it revealed that dragons skin is a very expensive material. And thus many started hunting the dragons whenever " +
					"a dragon invasion happened. And myth has it that it happens three times every day ...",player,"Admin");
		}
		else if(crossingLevel(player,68,value))
		{
			panel = new ServerInfo("The keeper of the past: dragons","Randomly every couple of hours dragons invade the surroundings of the " +
					"main city. Some say killing the dragons brings one wealth and prosperity, but revenge and sin... Lore has it that dragons move in packs " +
					"of three. A leader, a kicker and a sidekick with different powers.",player,"Admin");
		}
		else if(crossingLevel(player,70,value))
		{
			panel = new ServerInfo("This is your world","Warriros that had achieved a high rank in the ?stat system started influencing the balance of the world with the approval of the kings. Those " +
					"people sometimes overused their authority cause they were strong and noone could stand on their path. ",player,"Admin");
		}
		else if(crossingLevel(player,74,value))
		{
			panel = new ServerInfo("Farming Zone Event","Those that had the power were called the Elites. A year after their " +
					"ascension to the authority a revolution took place by the villagers and farmers of the farming zones. " +
					"And it came clear to them that this revolution should be stopped. They attacked the farming zones during " +
					"the night and fought against the farmer clans and smashed them to the ground. Since then, farmers try to revolt " +
					"every now and then, and thus the farming zone domination event was born ...",player,"Admin");
		}
		else if(crossingLevel(player,76,value))
		{
			panel = new ServerInfo("Flying Ship-halls", "Supressing the revolution was easy for the elites. And after they managed to subdue the farmers and " +
					"having already defeated the dragons there was noone that would question the authority of the elites. But, " +
					"there is one they fear. He who created the spells turned against the corrupted and abusive elites. " +
					"He created flying magical ships for farmers to live in peacefully away from the elites. And he created a spell " +
					"that would allow the farmers to board those flying ships that could provide for them.",player,"Admin");
		}
		else if(crossingLevel(player,77,value))
		{
			panel = new ServerInfo("He vs the elites. ", 
					"The elites found out about His deeds and were very angry. They decided that it was time they put a stop to his actions " +
					"and confronted him. The battle lasted for three days. This is the battle that the future generations will remember. One stood " +
					"against many and his courage and bravery exceeded any imaginable limit. It was coming clear to the elites that it would be impossible " +
					"to kill Him. So they made a pact with the dragons. And the dragons tought the elites an ancient forbidden spell that could seal someone" +
					"to the depths of the seas. ",player,"Admin");
		}
		else if(crossingLevel(player,78,value))
		{
			panel = new ServerInfo("This is your world","At the conclusion of the battle the elites casted alltogether the forbidden spell and managed to seal Him " +
					"away. Rumors has it that the battle took place at the furthest reaches of the Wastelands and that once again He, will " +
					"get reborn through the flesh of a warrior and take down the Elites and those that go against the free will of man... ",player,"Admin");
		}
		else if(crossingLevel(player,79,value))
		{
			panel = new ServerInfo("Continuation story","And the prophecy has his name to be .... "+ player.getName(),player,"Admin");
		}
		
		if(panel != null)
			L2JFrame.getInstance().send(player,panel);
	}
	
	private static boolean crossingLevel(PlayerInstance player, int level, int value)
	{
		boolean result = false;
		
		if(player.getLevel()<level && player.getLevel()+value >= level)
			result = true;
		
		return result;
	}
	
	public static List<String> tips = new ArrayList<String>();
	
	public static void registerTip(String s)
	{
		tips.add(s);
	}
	
	public static String getRandomTip()
	{
		if(tips.size()==0)
			tips.add("null");
		
		int x = Rnd.get(tips.size());
		
		return tips.get(x);
	}
}