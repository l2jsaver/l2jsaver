/*
 * Authors: Issle, Howler, Matim
 * File: RebuffTask.java
 */
package l2jsaver.Features.IssleMisc;

import java.util.Map;

import org.l2jmobius.gameserver.data.xml.impl.SkillData;
import org.l2jmobius.gameserver.model.World;
import org.l2jmobius.gameserver.model.actor.instance.PlayerInstance;
import org.l2jmobius.gameserver.model.skills.Skill;

import l2jsaver.Model.PcModel;

/**
 * @author Issle
 *
 */
public class RebuffTask implements Runnable{

	private int objid;
	
	public RebuffTask(int id)
	{
		objid = id;
	}
	@Override
	public void run() {
		
		
		PlayerInstance player = World.getInstance().getPlayer(objid);
		
		if(player == null)
			return;
		
		if(player.isInOlympiadMode())
			return;
		
		Map<Integer, Integer> list = PcModel.getModel(player).canceledBuffs;
		
		if(list == null)
			return;
		
		for(int id: list.keySet())
		{
			Skill skill = SkillData.getInstance().getSkill(id, list.get(id));
			skill.applyEffects(player, player);
		}
		
		list.clear();
	}

}
