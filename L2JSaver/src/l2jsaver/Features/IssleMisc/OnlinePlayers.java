package l2jsaver.Features.IssleMisc;

import org.l2jmobius.commons.concurrent.ThreadPool;
import org.l2jmobius.gameserver.model.World;
import org.l2jmobius.gameserver.util.Broadcast;

public class OnlinePlayers implements Runnable
{
	private int delay = 1800; // Setted to 30 minutes.
	
	public void run()
	{
		int online = World.getInstance().getPlayers().size();
		String a, b;
		
		if (online == 1)
		{
			a = "There is " + online + " online player";
			b = online + " online player";
		}
		else
		{
			a = "There are " + online + " online players";
			b = online + " online players";
		}
		
		Broadcast.toAllOnlinePlayers(a, false);
		System.out.println(b);
		ThreadPool.schedule(new OnlinePlayers(), delay*1000);
	}
}