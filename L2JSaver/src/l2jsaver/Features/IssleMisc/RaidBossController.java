/*
 * Authors: Issle, Howler, Matim
 * File: RaidBossController.java
 */
package l2jsaver.Features.IssleMisc;

import org.l2jmobius.commons.util.Rnd;
import org.l2jmobius.gameserver.geoengine.GeoEngine;
import org.l2jmobius.gameserver.instancemanager.ZoneManager;
import org.l2jmobius.gameserver.model.zone.type.PeaceZone;
import org.l2jmobius.gameserver.model.zone.type.WaterZone;

/**
 * @author Issle
 *
 */
public class RaidBossController {

	public static int x = -98422;
	public static int y = 233739;
	public static int z = -3375;
	public static int radius = 25000;
	
	public static int[] getRandomCoordinates()
	{
		int[] coord = new int[3];
		
		WaterZone zone=null;
		PeaceZone zone1=null;
		
		int _x = Rnd.get(x -radius , x + radius); 
		int _y = Rnd.get(y -radius , y + radius);
		
		int _z = GeoEngine.getInstance().getHeight(_x,_y,0);
		zone = ZoneManager.getInstance().getZone(_x, _y, _z, WaterZone.class);
		zone1 = ZoneManager.getInstance().getZone(_x, _y, _z, PeaceZone.class);
		
		coord[0]=_x;
		coord[1]=_y;
		coord[2]=_z;
		int count = 0;
		while((zone != null || zone1!=null) && count < 1000)
		{
			int _xx = Rnd.get(x -radius , x + radius); 
			int _yy = Rnd.get(y -radius , y + radius);
			
			int _zz = GeoEngine.getInstance().getHeight(_xx,_yy,0);
			zone = ZoneManager.getInstance().getZone(_xx, _yy, _zz, WaterZone.class);
			zone1 = ZoneManager.getInstance().getZone(_xx, _yy, _zz, PeaceZone.class);
			count++;
			
			coord[0]=_xx;
			coord[1]=_yy;
			coord[2]=_zz;
			
		}
		
		return coord;
		
	}
	
}
