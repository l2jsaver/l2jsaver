/*
 * Authors: Issle, Howler, Matim
 * File: Config.java
 */
package l2jsaver.Features.FarmingZoneDomination;

/**
 * @author Issle
 *
 */
public class Config {

	public static final int X=185730;
	public static final int Y=20311;
	public static final int RADIUS=3000;
	public static final int duration=1800000;
	
	public static final int patkBonus=6;
	public static final int pdefBonus=5;
	public static final int matkBonus=6;
	public static final int mdefBonus=5;
	public static final int speedBonus=5;
	
	public static int clanId = 0;
	
}
