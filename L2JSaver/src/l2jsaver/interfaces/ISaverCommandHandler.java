/*
 * Authors: Issle, Howler, Matim
 * File: ISaverCommandHandler.java
 */
package l2jsaver.interfaces;

import org.l2jmobius.gameserver.handler.IVoicedCommandHandler;

/**
 * @author Spunky
 *
 */
public interface ISaverCommandHandler extends IVoicedCommandHandler
{
	boolean forGM();
}
