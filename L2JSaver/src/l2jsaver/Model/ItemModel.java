/*
 * Authors: Issle, Howler, David
 * File: ItemModel.java
 */
package l2jsaver.Model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.l2jmobius.gameserver.model.holders.DropHolder;
import org.l2jmobius.gameserver.model.items.Item;

/**
 * Adds additional variables to items.
 * @author Davide
 *
 */
public class ItemModel
{
	public static Map<Integer, ItemModel> model = new ConcurrentHashMap<Integer, ItemModel>();
	
	public static ItemModel getModel(Item item)
	{
		int itemId = item.getId();
		
		if(model.containsKey(itemId))
			return model.get(itemId);
		else
			return null;
	}
	
	public static void generateModel(Item item)
	{
		model.put(item.getId(), new ItemModel());
	}
	
	/**
	 * Each item has"drop data" wich represent all needed infos
	 * to locate an item in the droplist of all mobs.
	 * It is to be considered as a "memory database".
	 */
	public List<DropHolder> dropData = new ArrayList<DropHolder>();
}