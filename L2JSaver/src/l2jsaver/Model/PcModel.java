package l2jsaver.Model;

import l2jsaver.Features.StatSystem.SpartanInstance;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.l2jmobius.gameserver.model.actor.instance.PlayerInstance;

import com.google.api.translate.Language;

/**
 * This is to be considered an "extension" of the L2PcInstance fields.
 * Its purpose is to provide additional L2PcInstance parameters to be used as statistics
 * or anything else.
 *
 */
public class PcModel 
{
	private static Map<Integer, PcModel> models = new ConcurrentHashMap<>();
	
	public static PcModel getModel(PlayerInstance player)
	{
		if(models.containsKey(player.getObjectId()))
			return models.get(player.getObjectId());
		else
			generateModel(player);
			return models.get(player.getObjectId());
	}
	
	public static synchronized void generateModel(PlayerInstance activeChar)
	{
		models.put(activeChar.getObjectId(), new PcModel(activeChar));
	}
	
	public static synchronized void removeModel(PlayerInstance activeChar)
	{
		models.remove(activeChar.getObjectId());
	}
	
	public int kills;
	public boolean isAI = false;
	//Davide's variables///////////
	public int mobKills = 0;
	public int closestMobKills = 0;
	public boolean isInSession = false;
	public SpartanInstance spartanInstance;
	public boolean balancerMode = false;
	///////////////////////////////
	
	//Kill's Variable that show stat's.
	public int totalKillingSpree = 0;
	public int timesGotFB = 0;
	
	// AssainationContract variables.
	public boolean isAssasin = false;
	public int repPoints = 0;
	public int targetObjId = 0;
	
	public Map<Integer, Integer> canceledBuffs = new HashMap<>();
	
	// Unlock System's Variable
	public int timesBleedUsed = 0;
	
	//Used for chat translation.
	public Language language = Language.ENGLISH;
	
	//Called when the character is created.
	public PcModel(PlayerInstance activeChar)
	{
		spartanInstance = new SpartanInstance(activeChar.getObjectId());
	}
	
	public Language getLanguage()
	{
		return language;
	}
	
	public void setLanguage(Language l)
	{
		language = l;
		
	}
	
	public void setAssasin(boolean b)
	{
		isAssasin = b;
	}
	
	public boolean isAssasin()
	{
		return isAssasin;
	}
	
	public void setTargetObjectId(int o)
	{
		targetObjId = o;
	}
	
	public int getTargetObjectId()
	{
		return targetObjId;
	}
	
	public void addRepPoints(int p)
	{
		repPoints = repPoints + p;
	}
	
	public void remRepPoints(int p)
	{
		repPoints = repPoints - p;
	}
	
	public int getRepPoints()
	{
		return repPoints;
	}

	/**
	 * @param id
	 */
	public void addCancelId(int id, int level) {
		canceledBuffs.put(id, level);
		
	}
}