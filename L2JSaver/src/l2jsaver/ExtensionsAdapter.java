/*
 * Authors: Issle, Howler, David
 * File: ExtensionsAdapter.java
 */
package l2jsaver;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Future;

import org.l2jmobius.gameserver.model.actor.Creature;
import org.l2jmobius.gameserver.model.actor.Npc;
import org.l2jmobius.gameserver.model.actor.instance.PlayerInstance;

import l2jsaver.Machine.Interfaces.IActor;
import l2jsaver.Machine.Interfaces.IBoat;
import l2jsaver.Machine.Interfaces.IExtension;
import l2jsaver.Machine.Interfaces.INpc;

/**
 * @author Issle
 *
 */
public class ExtensionsAdapter 
{
	private static Map<String, IActor> actorImplementors;
	private static Map<String, IBoat> boatImplementors;
	private static Map<String, Future<?>> tasks;
	private static Map<String, Integer> l2jframeListeners;
	private static Map<String, Map<Integer, Npc>> featureNpcSpawns;
	public static Map<String, IExtension> extensions;
	public static Map<String, INpc> npcImplementors;
	
	public static Map<String, IActor> getActors()
	{
		return actorImplementors;
	}
	
	public static Map<String,IBoat> getBoats()
	{
		return boatImplementors;
	}
	
	public static Map<String,Future<?>> getTasks()
	{
		return tasks;
	}
	
	public static Map<String,Integer> getL2JFrame()
	{
		return l2jframeListeners;
	}
	
	public static Map<String, Map<Integer,Npc>> getNpcs()
	{
		return featureNpcSpawns;
	}
	
	public static void load()
	{
		actorImplementors = new HashMap<>();
		npcImplementors = new HashMap<>();
		boatImplementors = new HashMap<>();
		tasks = new HashMap<>();
		l2jframeListeners = new HashMap<>();
		featureNpcSpawns = new HashMap<>();
		extensions = new HashMap<>();
	}
	
	public static void safeReload()
	{
		for(IExtension i : extensions.values())
			i.onReload();
	}
	
	
	public static void onLogout(PlayerInstance player)
	{
		if(actorImplementors == null)
			return;
		
		for(IActor i: actorImplementors.values())
			i.onLogout(player);
	}
	
	public static void onLogin(PlayerInstance player)
	{
		if(actorImplementors == null)
			return;
		
		for(IActor i: actorImplementors.values())
			i.onLogin(player);
	}
	
	public static void onShutDown()
	{
		for(IExtension i: extensions.values())
			i.onShutDown();
	}
	
	public static void onDie(PlayerInstance killed, PlayerInstance killer)
	{
		if(actorImplementors == null)
			return;
		
		for(IActor i: actorImplementors.values())
			i.onDie(killed, killer);
	}
	
	public static void onNpcDie(Creature killed, Creature killer)
	{
		if(npcImplementors == null)
			return;
		
		for(INpc i: npcImplementors.values())
			i.onDie(killer, killed);
	}
	
	public static void onStopMoving(Creature boat)
	{
		if(boatImplementors == null)
			return;
		
		for(IBoat i: boatImplementors.values())
			i.clientStopMoving(boat);
	}

	/**
	 * @param activeChar
	 * @param tarZ 
	 * @param tarY 
	 * @param tarX 
	 * @param orZ 
	 * @param orY 
	 * @param orX 
	 */
	public static void onMove(PlayerInstance activeChar, int orX, int orY, int orZ, int tarX, int tarY, int tarZ) 
	{
		if(actorImplementors == null)
			return;
		
		for(IActor i: actorImplementors.values())
			i.onMove(activeChar, orX, orY, orZ, tarX,tarY,tarZ);
	}
}