/*
 * Authors: Issle, Howler, Matim
 * File: FastDoubleMap.java
 */
package l2jsaver.Machine.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Issle
 *
 */
public class DoubleMap<E> {

	private Map<Integer,E> map1;
	private Map<Integer,E> map2;
	
	public DoubleMap()
	{
		map1 = new HashMap<Integer,E>();
		map2 = new HashMap<Integer,E>();
	}
	
	public synchronized void put(int key1, int key2 , E value)
	{
		map1.put(key1,value);
		map2.put(key2,value);
	}
	
	public E getByKey1(int key1)
	{
		return map1.get(key1);
	}
	
	public E getByKey2(int key2)
	{
		return map2.get(key2);
	}
	
	
	public boolean containsKey1(int key1)
	{
		if(map1.containsKey(key1))
			return true;
		
		return false;
	}
	
	public boolean containsKey2(int key2)
	{
		if(map2.containsKey(key2))
			return true;
		
		return false;
		
	}
}
