/*
 * Authors: Issle, Howler, David
 * File: HtmlUtils.java
 */
package l2jsaver.Machine.utils;

/**
 * @author Issle
 *
 */
public class HtmlUtils {

	public static String getSkillIcon(int id)
	{
		String skillIconPath = "";
		int skillId = id;
		
		skillIconPath += "icon.skill";
		if(skillId<10)
			skillIconPath+= "000"+String.valueOf(skillId);
		else if(skillId<100)
			skillIconPath+= "00"+String.valueOf(skillId);
		else if(skillId<1000)
			skillIconPath+= "0"+String.valueOf(skillId);
		else
			skillIconPath+= String.valueOf(skillId);
		
		return skillIconPath;
	}
}
