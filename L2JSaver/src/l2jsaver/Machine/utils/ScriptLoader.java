/*
 * Authors: Issle, Howler, Matim
 * File: ScriptLL.java
 */
package l2jsaver.Machine.utils;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.l2jmobius.gameserver.scripting.java.JavaExecutionContext;
import org.l2jmobius.gameserver.scripting.java.JavaScriptingEngine;

/**
 * @author Spunky
 *
 */
public class ScriptLoader 
{
	private static final Logger LOGGER = Logger.getLogger(ScriptLoader.class.getName());
	
	public static final Path SCRIPT_FOLDER = Paths.get(".", "extensions");
	
	private static final JavaExecutionContext _javaExecutionContext = new JavaScriptingEngine().createExecutionContext();
	
	protected ScriptLoader()
	{
		
	}
	
	private void processDirectory(File dir, List<Path> files)
	{
		for (File file : dir.listFiles())
		{
			if (file.isFile())
			{
				final String filePath = file.toURI().getPath();
				if (filePath.endsWith(".java"))
					files.add(file.toPath().toAbsolutePath());
			}
			else if (file.isDirectory())
			{
				processDirectory(file, files);
			}
		}
	}
	
	public void executeScript(Path sourceFile) throws Exception
	{
		if (!sourceFile.isAbsolute())
		{
			sourceFile = SCRIPT_FOLDER.resolve(sourceFile);
		}
		
		sourceFile = sourceFile.toAbsolutePath();
		
		final Entry<Path, Throwable> error = _javaExecutionContext.executeScript(sourceFile);
		if (error != null)
		{
			throw new Exception("ScriptEngine: " + error.getKey() + " failed execution!", error.getValue());
		}
	}
	
	public void executeScriptList(String file, boolean full) throws Exception
	{
		Path reloadMe = Paths.get(SCRIPT_FOLDER.toString(), file);
		
		final List<Path> files = new ArrayList<>();
		
		if (full)
			processDirectory(SCRIPT_FOLDER.toFile(), files);
		else
			processDirectory(reloadMe.toFile(), files);
		
		final Map<Path, Throwable> invokationErrors = _javaExecutionContext.executeScripts(files);
		for (Entry<Path, Throwable> entry : invokationErrors.entrySet())
		{
			LOGGER.log(Level.WARNING, "ScriptEngine: " + entry.getKey() + " failed execution!", entry.getValue());
		}
	}
	
	public Path getCurrentLoadingScript()
	{
		return _javaExecutionContext.getCurrentExecutingScript();
	}
	
	public static ScriptLoader getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	private static class SingletonHolder
	{
		protected static final ScriptLoader INSTANCE = new ScriptLoader();
	}
}
