/**
 * This file is part of Aion X Emu <aionxemu.com>
 *
 *  This is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This software is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser Public License
 *  along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */

package l2jsaver.Machine.utils;

import java.io.File;
import java.util.ArrayList;

/**
 * This class contains utilities that are used when we are working with classes
 *
 * @author SoulKeeper
 */
public class ClassUtils {
    /**
     * Return true if class a is either equivalent to class b, or if class a is a subclass of class b, i.e. if a either
     * "extends" or "implements" b. Note tht either or both "Class" objects may represent interfaces.
     *
     * @param a class
     * @param b class
     * @return true if a == b or a extends b or a implements b
     */
    public static boolean isSubclass(Class<?> a, Class<?> b) {
        // We rely on the fact that for any given java class or
        // primtitive type there is a unqiue Class object, so
        // we can use object equivalence in the comparisons.
        if (a == b) {
            return true;
        }
        if (a == null || b == null) {
            return false;
        }
        for (Class<?> x = a; x != null; x = x.getSuperclass()) {
            if (x == b) {
                return true;
            }
            if (b.isInterface()) {
                Class<?>[] interfaces = x.getInterfaces();
                for (Class<?> anInterface : interfaces) {
                    if (isSubclass(anInterface, b)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Checks if class in member of the package
     *
     * @param clazz       class to check
     * @param packageName package
     * @return true if is member
     */
    public static boolean isPackageMember(Class<?> clazz, String packageName) {
        return isPackageMember(clazz.getName(), packageName);
    }

    /**
     * Checks if classNames belongs to package
     *
     * @param className   class name
     * @param packageName package
     * @return true if belongs
     */
    public static boolean isPackageMember(String className, String packageName) {
        if (!className.contains(".")) {
            return packageName == null || packageName.isEmpty();
        } else {
            String classPackage = className.substring(0, className.lastIndexOf('.'));
            return packageName.equals(classPackage);
        }
    }
    
	public static Class<?>[] getAllClasses(String pckgname) 
	{
	    ArrayList<Class<?>> classes=new ArrayList<Class<?>>(); 
		try
		{
			// Get a File object for the package 
		    File directory=null; 
		    try 
		    { 
		    	directory=new File(Thread.currentThread().getContextClassLoader().getResource(pckgname.replace('.', '/')).getFile()); 
		    } 
		    catch(NullPointerException x) 
		    { 
		    	System.out.println("Nullpointer");
		    	throw new ClassNotFoundException(pckgname+" does not appear to be a valid package"); 
		    } 
		    if(directory.exists()) 
		    { 
		    	// Get the list of the files contained in the package 
		    	String[] files=directory.list(); 
		    	for(int i=0; i<files.length; i++) 
		    	{ 
		    		// we are only interested in .class files 
		    		if(files[i].endsWith(".class")) 
		    		{ 
		    			// removes the .class extension 
		    			classes.add(Class.forName(pckgname+'.'+files[i].substring(0, files[i].length()-6))); 
		    		} 
		    	} 
		    }
		    else 
		    { 
		    	System.out.println("Directory does not exist");
		    	throw new ClassNotFoundException(pckgname+" does not appear to be a valid package"); 
		    } 

		  
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
	    Class<?>[] classesA=new Class[classes.size()]; 
	    classes.toArray(classesA); 
	    return classesA;
	}
}
