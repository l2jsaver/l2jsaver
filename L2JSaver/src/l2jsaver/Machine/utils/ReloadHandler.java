/*
 * Authors: Issle, Howler, David
 * File: ThemistoclesHandler.java
 */
package l2jsaver.Machine.utils;

import org.l2jmobius.gameserver.model.actor.instance.PlayerInstance;

import l2jsaver.ExtensionsAdapter;
import l2jsaver.Machine.Interfaces.IExtension;
import l2jsaver.interfaces.ISaverCommandHandler;


/**
 * @author Issle
 *
 */
public class ReloadHandler implements ISaverCommandHandler
{
	private static final String[] VOICED_COMMANDS =
	{
		"reload", "reloadAll"
	};
	
	@Override
	public boolean useVoicedCommand(String command, PlayerInstance activeChar, String params)
	{
		if(command.equals("reload"))
		{
			if(params == null)
			{
				activeChar.sendMessage("Please specify the extension name to reload.");
				return false;
			}
			
			if (activeChar.isGM())
			{
				try 
				{
					IExtension extension = ExtensionsAdapter.extensions.get(params);
					if(extension != null)
						extension.onReload();
					ScriptLoader.getInstance().executeScriptList(params, false);
					Utils.messagePlayers(params + " has been reloaded", activeChar.getName());
			
				} 
				catch (Exception e) 
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
			}
		}
		else if(command.equals("reloadAll"))
		{
			if (activeChar.isGM())
			{
				ExtensionsAdapter.safeReload();
				try 
				{
					ScriptLoader.getInstance().executeScriptList(params,true);
					Utils.messagePlayers("All extensions have been reloaded", activeChar.getName());
				} 
				catch (Exception e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		}
		return true;
	}

	@Override
	public String[] getVoicedCommandList()
	{
		return VOICED_COMMANDS;
	}

	@Override
	public boolean forGM() 
	{
		return true;
	}
}