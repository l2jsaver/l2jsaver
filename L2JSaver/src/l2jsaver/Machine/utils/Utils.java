package l2jsaver.Machine.utils;

import java.text.DecimalFormat;

import org.l2jmobius.gameserver.model.World;
import org.l2jmobius.gameserver.model.actor.Attackable;
import org.l2jmobius.gameserver.model.actor.Creature;
import org.l2jmobius.gameserver.model.actor.instance.PlayerInstance;

public class Utils 
{
	public static int manhattanDistance2D(Creature player1, Creature player2)
	{
		int x =0;
		
		int x1 = player1.getX();
		int x2 = player2.getX();
		int y1 = player1.getY();
		int y2 = player2.getY();
		
		x = Math.abs(x1-x2 )+ Math.abs(y1-y2);
		return x;
	}

	public static int manhattanDistance2D(Creature defender, int x, int y) {

		int x1 = defender.getX();
		int y1 = defender.getY();
		
		return Math.abs(x1- x)+ Math.abs(y1 - y);
	}
	
	public static int manhattanDistance3D(Creature defender, int x, int y, int z) {

		int x1 = defender.getX();
		int y1 = defender.getY();
		int z1 = defender.getZ();
		
		return Math.abs(x1- x)+ Math.abs(y1 - y)+ Math.abs(z1-z);
	}

	public static int manhattanDistance2D(Creature player1, Attackable l2Attackable)
	{
		int x =0;
		
		int x1 = player1.getX();
		int x2 = l2Attackable.getX();
		int y1 = player1.getY();
		int y2 = l2Attackable.getY();
		
		x = Math.abs(x1-x2 )+ Math.abs(y1-y2);
		return x;
	}
	/**
	 * Misc function to round numbers
	 * @param Rval
	 * @param Rpl
	 * @return
	 */
	public static float round(float Rval, int Rpl)
	{
	  float p = (float)Math.pow(10,Rpl);
	  Rval = Rval * p;
	  float tmp = Math.round(Rval);
	  return (float)tmp/p;
	}
	
	public static long round(long Rval, int Rpl)
	{
		long p = (long)Math.pow(10,Rpl);
	  Rval = Rval * p;
	  long tmp = Math.round(Rval);
	  return (long)tmp/p;
	}
	
	public static boolean isIntNumber(String num)
	{
	    try
	    {
	        Integer.parseInt(num);
	    } 
	    catch(NumberFormatException nfe) 
	    {
	        return false;
	    }
	    return true;
	}
	
	public static double roundTwoDecimals(double d)
	{
    	DecimalFormat twoDForm = new DecimalFormat("#.##");
    	return Double.valueOf(twoDForm.format(d));
	}
	
	public static void messagePlayers(String message, String name)
	{
		for(PlayerInstance player: World.getInstance().getPlayers())
			player.sendMessage(message + " by "+ name);
	}

}