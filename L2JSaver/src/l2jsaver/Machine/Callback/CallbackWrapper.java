/*
 * Authors: Issle, Howler, David
 * File: CallbackWrapper.java
 */
package l2jsaver.Machine.Callback;

import org.l2jmobius.gameserver.model.actor.Creature;
import org.l2jmobius.gameserver.model.actor.instance.PlayerInstance;
import org.l2jmobius.gameserver.network.GameClient;

import l2jsaver.ExtensionsAdapter;
import l2jsaver.SaverAdapter;
import l2jsaver.Model.PcModel;

/**
 * @author Issle
 * 
 * Put in here all the functions that will get
 * hooked in the core. Hooking will happen during
 * class loading by the Javaagent.
 *
 */
public class CallbackWrapper
{
	/**
	 * Called when a character is logout from the game.
	 * @param actor
	 */
	public static boolean onLogout(PlayerInstance actor)
	{
		PcModel model = PcModel.getModel(actor);
		
		if(model != null)
			model.spartanInstance.saveData(actor.getObjectId());
		else
			System.out.println("Something went wrong.");
		
		ExtensionsAdapter.onLogout(actor);
		PcModel.removeModel(actor);
		return false;
	}
	
	/**
	 * Called when a character is login into the game.
	 * @param actor
	 */
	public static boolean onPlayerEnter(PlayerInstance player)
	{
		PcModel.generateModel(player);
		//KillingSpreeController.LoadDb(activeChar);
		ExtensionsAdapter.onLogin(player);
		return false;
	}
	
	public static boolean load()
	{
		System.out.println("Loading L2JSaver");
		SaverAdapter.load();
		return false;
	}
	
	public static boolean onKill(Creature killer, PlayerInstance actor)
	{
		//KillingSpreeController.onKill(killer, actor);
		if(killer instanceof PlayerInstance)
			ExtensionsAdapter.onDie(actor, (PlayerInstance)killer);
		return false;
	}
	
	public static boolean onKillNpc(Creature killed, Creature killer)
	{
		//KillingSpreeController.onKill(killer, actor);
		ExtensionsAdapter.onNpcDie(killed, killer);
		return false;
	}
	
	public static boolean onShutdown()
	{
		System.out.println("Saving L2JSaver Data");
		ExtensionsAdapter.onShutDown();
		return false;
	}
}
