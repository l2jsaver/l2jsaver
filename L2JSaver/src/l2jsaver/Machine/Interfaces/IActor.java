/*
 * Authors: Issle, Howler, David
 * File: IActor.java
 */
package l2jsaver.Machine.Interfaces;

import org.l2jmobius.gameserver.model.actor.instance.PlayerInstance;

/**
 * @author Issle
 *
 */
public interface IActor 
{
	public void onDie(PlayerInstance killed, PlayerInstance killer);
	public void onLogin(PlayerInstance player);
	public void onLogout(PlayerInstance player);
	public void onItemUse(PlayerInstance player);
	public void onMove(PlayerInstance player, int orX, int orY, int orZ, int tarX, int tarY, int tarZ);
}