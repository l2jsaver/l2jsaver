/*
 * Authors: Issle, Howler, Matim
 * File: IExtension.java
 */
package l2jsaver.Machine.Interfaces;

/**
 * @author Issle
 *
 */
public interface IExtension {

	public void onReload();
	
	public void onShutDown();
}
