/*
 * Authors: Issle, Howler, David
 * File: IBoat.java
 */
package l2jsaver.Machine.Interfaces;

import org.l2jmobius.gameserver.model.actor.Creature;

/**
 * @author Issle
 *
 */
public interface IBoat {

	public void clientStopMoving(Creature boat);
	
}
