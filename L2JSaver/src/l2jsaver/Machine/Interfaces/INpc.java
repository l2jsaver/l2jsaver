/*
 * Authors: Issle, Howler, Matim
 * File: INpc.java
 */
package l2jsaver.Machine.Interfaces;

import org.l2jmobius.gameserver.model.actor.Creature;

/**
 * @author Issle
 *
 */
public interface INpc {

	public void onDie(Creature killer, Creature killed);
}
