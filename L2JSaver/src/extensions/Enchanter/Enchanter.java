/*
 * Authors: Issle, Howler, Matim
 * File: Enchanter.java
 */
package extensions.Enchanter;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.l2jmobius.gameserver.model.actor.instance.PlayerInstance;
import org.l2jmobius.gameserver.model.itemcontainer.Inventory;
import org.l2jmobius.gameserver.model.items.instance.ItemInstance;
import org.l2jmobius.gameserver.network.serverpackets.InventoryUpdate;

import extensions.Enchanter.view.Controller;
import extensions.Enchanter.view.EnchanterWindow;
import extensions.Enchanter.view.SelectionWindow;

import l2jsaver.Features.L2JFrame.AbstractController;
import l2jsaver.Features.L2JFrame.L2JFrame;
import l2jsaver.Features.StatSystem.UnlocksHolder;
import l2jsaver.Model.PcModel;

/**
 * @author Issle
 * 
 */
public class Enchanter extends AbstractController {

	public static final int[] fa = { 3, 9, 27, 81, 243, 729, 2187, 6561, 19683,
			59049 };
	public static final int[] solo = { 13, 28, 47, 72, 107, 160, 247, 400, 683,
			1224 };
	public static final int[] team = { 1, 4, 9, 16, 25, 36, 49, 64, 81, 100 };
	public static final int[] pvp = { 1, 16, 81, 256, 625, 1296, 2401, 4096,
			6561, 10000 };
	private static final int MAX_ENCHANT = 10;
	public static final int FA = 6673;
	public static final int SOLO = 1890;
	public static final int TEAM = 9629;

	public Map<String, Integer> itemSlots = new ConcurrentHashMap<String,Integer>();
	
	public void load()
	{
		itemSlots.put("Left-Hand", Inventory.PAPERDOLL_LHAND);
		itemSlots.put("Right-Hand", Inventory.PAPERDOLL_RHAND);
		itemSlots.put("Helmet", Inventory.PAPERDOLL_HEAD);
		itemSlots.put("Chest", Inventory.PAPERDOLL_CHEST);
		itemSlots.put("Pants", Inventory.PAPERDOLL_LEGS);
		itemSlots.put("Boots", Inventory.PAPERDOLL_FEET);
		itemSlots.put("Gloves", Inventory.PAPERDOLL_GLOVES);
		itemSlots.put("Left-Earing", Inventory.PAPERDOLL_LEAR);
		itemSlots.put("Right-Earing", Inventory.PAPERDOLL_REAR);
		itemSlots.put("Left-Ring", Inventory.PAPERDOLL_LFINGER);
		itemSlots.put("Right-Ring", Inventory.PAPERDOLL_RFINGER);
		itemSlots.put("Necklace", Inventory.PAPERDOLL_NECK);
		
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Enchanter.getInstance().load();
		UnlocksHolder.getInstance().registerUnlock(new Controller());
	}

	private volatile static Enchanter singleton;

	private Enchanter() {
		L2JFrame.getInstance().registerListener(this);
	}

	public static Enchanter getInstance() {
		if (singleton == null) {
			synchronized (Enchanter.class) {
				if (singleton == null)
					singleton = new Enchanter();
			}
		}
		return singleton;
	}

	@Override
	public void execute(PlayerInstance activeChar,
			Map<Integer, String> arguments) {

		String type = arguments.get(0);
		
		if(type == null)
			return;
		
		if(type.equals("Pick"))
		{
			String item = arguments.get(1);
			
			if(item == null)
				return;
			
			L2JFrame.getInstance().send(activeChar, new EnchanterWindow(activeChar,item));
		}
		else if(type.equals("Back"))
		{
			L2JFrame.getInstance().send(activeChar, new SelectionWindow(activeChar));
		}
		else if(type.equals("Enchant"))
		{
			String item = arguments.get(1);
			if(item == null)
				return;
			
			if(!itemSlots.containsKey(item))
				return;
			
			ItemInstance itemInst = activeChar.getInventory().getPaperdollItem(itemSlots.get(item));
			
			if(itemInst == null)
				return;
			
			int itemLevel = itemInst.getEnchantLevel();
			
			if(itemLevel > MAX_ENCHANT)
			{
				activeChar.sendMessage("This enchanter cant enchant this item any longer.");
				return;
			}
			
			
			if(pvp[itemLevel] > PcModel.getModel(activeChar).spartanInstance.getSoloPoints())
			{
				activeChar.sendMessage("Not enough pvp experience to do this action.");
				return;
			}
			
			ItemInstance money = activeChar.getInventory().getItemByItemId(FA);
			ItemInstance soloo = activeChar.getInventory().getItemByItemId(SOLO);
			ItemInstance teamm = activeChar.getInventory().getItemByItemId(TEAM);
			
			if(money == null || fa[itemLevel]> money.getCount())
			{
				activeChar.sendMessage("Not enough money to do this action.");
				return;
			}
			
			if(soloo == null || solo[itemLevel]> soloo.getCount())
			{
				activeChar.sendMessage("Not enough items to do this action.");
				return;
			}
			
			if(teamm == null || team[itemLevel]> teamm.getCount())
			{
				activeChar.sendMessage("Not enough items to do this action.");
				return;
			}
			
			activeChar.getInventory().destroyItemByItemId("Enchanter", FA, fa[itemLevel], activeChar, null);
			activeChar.getInventory().destroyItemByItemId("Enchanter", SOLO, solo[itemLevel], activeChar, null);
			activeChar.getInventory().destroyItemByItemId("Enchanter", TEAM, team[itemLevel], activeChar, null);
			
			itemInst.setEnchantLevel(itemLevel+1);
			activeChar.sendMessage("You have sucessfully enchanted your item in slot: "+item+ " to +"+String.valueOf(itemLevel+1));
			activeChar.sendPacket(new InventoryUpdate());
			activeChar.broadcastUserInfo();
			L2JFrame.getInstance().send(activeChar, new SelectionWindow(activeChar));
		}
		
	}

	@Override
	public void execute(PlayerInstance activeChar) {
		// TODO Auto-generated method stub
		
	}

}
