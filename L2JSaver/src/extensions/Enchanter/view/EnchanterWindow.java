/*
 * Authors: Issle, Howler, Matim
 * File: EnchanterWindow.java
 */
package extensions.Enchanter.view;

import org.l2jmobius.gameserver.datatables.ItemTable;
import org.l2jmobius.gameserver.model.actor.instance.PlayerInstance;
import org.l2jmobius.gameserver.model.items.instance.ItemInstance;

import extensions.Enchanter.Enchanter;
import l2jsaver.Features.L2JFrame.models.L2JAlign;
import l2jsaver.Features.L2JFrame.models.primitive.L2JButton;
import l2jsaver.Features.L2JFrame.models.primitive.L2JButton.L2JButtonType;
import l2jsaver.Features.L2JFrame.models.primitive.L2JRootPane;
import l2jsaver.Features.L2JFrame.models.primitive.L2JText;
import l2jsaver.Features.L2JFrame.models.primitive.L2JVerticalPane;
import l2jsaver.Model.PcModel;

/**
 * @author Issle
 *
 */
public class EnchanterWindow extends L2JRootPane {

	
	public EnchanterWindow(PlayerInstance activeChar, String item)
	{
		super();
		L2JVerticalPane items = new L2JVerticalPane();
		items.toPanel(this);
		String fa = ItemTable.getInstance().getTemplate(Enchanter.FA).getIcon();
		String solo = ItemTable.getInstance().getTemplate(Enchanter.SOLO).getIcon();
		String team = ItemTable.getInstance().getTemplate(Enchanter.TEAM).getIcon();
		String exp = "br_cashtex.item.br_cash_rune_of_exp_i00";
		
		
		String fa_name = ItemTable.getInstance().getTemplate(Enchanter.FA).getName();
		String solo_name = ItemTable.getInstance().getTemplate(Enchanter.SOLO).getName();
		String team_name = ItemTable.getInstance().getTemplate(Enchanter.TEAM).getName();
		String pvp_exp = "PvP Experience:";
		
		ItemInstance userItem = activeChar.getInventory().getPaperdollItem(Enchanter.getInstance().itemSlots.get(item));
		
		if(userItem == null)
		{
			new L2JText("You got no item equiped in that slot.").toPanel(items);
			new L2JButton("Back", L2JButtonType.GRACIA, Enchanter.getInstance(), L2JAlign.Center).toPanel(items);
			return;
		}
		int level = userItem.getEnchantLevel();
		String itemName = userItem.getItemName();
		
		String fa_needed = String.valueOf(Enchanter.fa[level]);
		String solo_needed = String.valueOf(Enchanter.solo[level]);
		String team_needed = String.valueOf(Enchanter.team[level]);
		String pvp_needed = String.valueOf(Enchanter.pvp[level]);
		
		String fa_have = "0";
		String solo_have = "0";
		String team_have ="0";
		String pvp_have = "0";
		
		ItemInstance ifa_have = activeChar.getInventory().getItemByItemId(Enchanter.FA);
		ItemInstance isolo_have = activeChar.getInventory().getItemByItemId(Enchanter.SOLO);
		ItemInstance iteam_have = activeChar.getInventory().getItemByItemId(Enchanter.TEAM);

		if(ifa_have != null)
			fa_have = String.valueOf( ifa_have.getCount());
		if(isolo_have != null)
			solo_have = String.valueOf( isolo_have.getCount());
		if(iteam_have != null)
			team_have = String.valueOf( iteam_have.getCount());
		pvp_have = String.valueOf(PcModel.getModel(activeChar).spartanInstance.getSoloPoints());

		String html = "<html><body>";
		html+= "<center>Enchanting:<br1>";
		html+= "<font color=LEVEL>+"+level+" "+itemName+"</font><br></center>";
		html+= "<table width=290><tr><td width=150>Items (from +"+Integer.toString(level)+" to +"+Integer.toString(level+1)+")</td><td width=70>Owned</td><td width=70>Needed</td></tr></table>";

		if (Integer.parseInt(fa_have) > Integer.parseInt(fa_needed))
			html+= "<table width=290 bgcolor=666666><tr><td width=150 align=center><img src="+fa+" width=32 height=32><font color=00FF00>"+fa_name+"</font></td><td width=70><font color=00FF00>"+fa_have+"</font></td><td width=70><font color=00FF00>"+fa_needed+"</font></td></tr></table>";
		else
			html+= "<table width=290 bgcolor=666666><tr><td width=150 align=center><img src="+fa+" width=32 height=32><font color=FF0000>"+fa_name+"</font></td><td width=70><font color=FF0000>"+fa_have+"</font></td><td width=70><font color=FF0000>"+fa_needed+"</font></td></tr></table>";



		if (Integer.parseInt(solo_have) > Integer.parseInt(solo_needed))
			html+= "<table width=290><tr><td width=150 align=center><img src="+solo+" width=32 height=32><font color=00FF00>"+solo_name+"</font></td><td width=70><font color=00FF00>"+solo_have+"</font></td><td width=70><font color=00FF00>"+solo_needed+"</font></td></tr></table>";
		else
			html+= "<table width=290><tr><td width=150 align=center><img src="+solo+" width=32 height=32><font color=FF0000>"+solo_name+"</font></td><td width=70><font color=FF0000>"+solo_have+"</font></td><td width=70><font color=FF0000>"+solo_needed+"</font></td></tr></table>";



		if (Integer.parseInt(team_have) > Integer.parseInt(team_needed))
			html+= "<table width=290 bgcolor=666666><tr><td width=150 align=center><img src="+team+" width=32 height=32><font color=00FF00>"+team_name+"</font></td><td width=70><font color=00FF00>"+team_have+"</font></td><td width=70><font color=00FF00>"+team_needed+"</font></td></tr></table>";
		else
			html+= "<table width=290 bgcolor=666666><tr><td width=150 align=center><img src="+team+" width=32 height=32><font color=FF0000>"+team_name+"</font></td><td width=70><font color=FF0000>"+team_have+"</font></td><td width=70><font color=FF0000>"+team_needed+"</font></td></tr></table>";



		if (Integer.parseInt(pvp_have) > Integer.parseInt(pvp_needed))
			html+= "<table width=290><tr><td width=150 align=center><img src="+exp+" width=32 height=32><font color=00FF00>"+pvp_exp+"</font></td><td width=70><font color=00FF00>"+pvp_have+"</font></td><td width=70><font color=00FF00>"+pvp_needed+"</font></td></tr></table>";
		else
			html+= "<table width=290><tr><td width=150 align=center><img src="+exp+" width=32 height=32><font color=FF0000>"+pvp_exp+"</font></td><td width=70><font color=FF0000>"+pvp_have+"</font></td><td width=70><font color=FF0000>"+pvp_needed+"</font></td></tr></table>";

		html+= "</body></html>";
		
		new L2JText(html).toPanel(items);
		
		/*items.addGraphic(new L2JImage(fa, "32", "32"));
		new L2JText(fa_name +": "+fa_have+"/"+fa_needed).toPanel(items).highlightText(fa_needed,(Integer.parseInt(fa_have) > Integer.parseInt(fa_needed)) ? L2JColor.Green : L2JColor.Red);
		
		items.addGraphic(new L2JImage(solo, "32", "32"));
		new L2JText(solo_name +": "+solo_have+"/"+solo_needed).toPanel(items).highlightText(solo_needed,(Integer.parseInt(solo_have) > Integer.parseInt(solo_needed)) ? L2JColor.Green : L2JColor.Red);
		
		items.addGraphic(new L2JImage(team, "32", "32"));
		new L2JText(team_name +": "+team_have+"/"+team_needed).toPanel(items).highlightText(team_needed,(Integer.parseInt(team_have) > Integer.parseInt(team_needed)) ? L2JColor.Green : L2JColor.Red);
	
		new L2JText(pvp_exp +": "+pvp_have+"/"+pvp_needed).toPanel(items).highlightText(pvp_needed,L2JColor.Green);*/
	
		new L2JButton("Enchant",L2JButtonType.GRACIA, Enchanter.getInstance(), L2JAlign.Center).toPanel(items).addArgument("Enchant").addArgument(item);
	
		
	}

}
