/*
 * Authors: Issle, Howler, Matim
 * File: EnchanterWindow.java
 */
package extensions.Enchanter.view;

import org.l2jmobius.gameserver.model.actor.instance.PlayerInstance;
import org.l2jmobius.gameserver.model.items.instance.ItemInstance;

import extensions.Enchanter.Enchanter;
import l2jsaver.Features.L2JFrame.models.L2JAlign;
import l2jsaver.Features.L2JFrame.models.primitive.L2JButton;
import l2jsaver.Features.L2JFrame.models.primitive.L2JButton.L2JButtonType;
import l2jsaver.Features.L2JFrame.models.primitive.L2JHorizontalPane;
import l2jsaver.Features.L2JFrame.models.primitive.L2JRootPane;
import l2jsaver.Features.L2JFrame.models.primitive.L2JVerticalPane;

/**
 * @author Issle
 *
 */
public class SelectionWindow extends L2JRootPane {

	
	public SelectionWindow(PlayerInstance activeChar)
	{
		super();
		
		L2JVerticalPane items = new L2JVerticalPane();
		
		int place =0;
		L2JHorizontalPane sub = null;
		for(String s : Enchanter.getInstance().itemSlots.keySet())
		{
			if(place == 0)
			{
				place = 1;
				sub = new L2JHorizontalPane(L2JAlign.Center);
				items.addGraphic(sub);
				L2JButton button = new L2JButton(s,L2JButtonType.OTHER, Enchanter.getInstance(), L2JAlign.Center).toPanel(sub).addArgument("Pick").addArgument(s);
				ItemInstance itm = activeChar.getInventory().getPaperdollItem(Enchanter.getInstance().itemSlots.get(s));
				if(itm != null)
					button.setIcon(itm.getItem().getIcon());
			}
			else
			{
				place = 0;
				items.addGraphic(sub);
				L2JButton button = new L2JButton(s,L2JButtonType.OTHER, Enchanter.getInstance(), L2JAlign.Center).toPanel(sub).addArgument("Pick").addArgument(s);
				ItemInstance itm = activeChar.getInventory().getPaperdollItem(Enchanter.getInstance().itemSlots.get(s));
				if(itm != null)
					button.setIcon(itm.getItem().getIcon());
			}
		}
	
		items.toPanel(this);
	}

}
