/*
 * Authors: Issle, Howler, Matim
 * File: Controller.java
 */
package extensions.Enchanter.view;

import org.l2jmobius.gameserver.model.actor.instance.PlayerInstance;

import l2jsaver.Features.L2JFrame.L2JFrame;
import l2jsaver.Features.StatSystem.AbstractUnlock;

/**
 * @author Issle
 *
 */
public class Controller extends AbstractUnlock{

	/**
	 * @param name
	 * @param experience
	 * @param type
	 */
	public Controller() {
		super("Enchanter",2000, TYPE_SOLO);
		description = "This enchant system allows you to safely enchant any armor or weapon part" +
				" up to +10. Each enchant requires a specific amount of materials and pvp experience. " +
				"The difficulty is increasing as the enchant level increases. After +10, the only " +
				"way to enchant is the classic method, by using enchant scrolls. Keep in mind that " +
				"nothing prevents you from using scrolls to enchant from +0 to +10.";
		commands = new String[1];
		commands[0]= "enchanter";
	}

	@Override
	public double getResult(PlayerInstance activeChar) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	@Override
	public boolean useVoicedCommand(String command, PlayerInstance activeChar,String params)
	{
		L2JFrame.getInstance().send(activeChar, new SelectionWindow(activeChar));
		return false;
	}


}
