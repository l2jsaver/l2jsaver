/*
 * Authors: Issle, Howler, Matim
 * File: FarmingZoneDomination.java
 */
package extensions.FarmingZoneDomination;

import l2jsaver.Features.StatSystem.UnlocksHolder;
import extensions.FarmingZoneDomination.base.DatabaseController;
import extensions.FarmingZoneDomination.controllers.DominationController;
import extensions.FarmingZoneDomination.handlers.ClanStatusH;

/**
 * @author Issle
 *
 */
public class FarmingZoneDomination {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		DatabaseController.getInstance();
		DominationController.getInstance();
		DominationController.getInstance().restore();
		UnlocksHolder.getInstance().registerUnlock(new ClanStatusH());

	}

}
