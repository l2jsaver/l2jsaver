/*
 * Authors: Issle, Howler, Matim
 * File: StartTask.java
 */
package extensions.FarmingZoneDomination.models;

import java.util.concurrent.Future;

import org.l2jmobius.commons.concurrent.ThreadPool;

import extensions.FarmingZoneDomination.base.DatabaseController;
import extensions.FarmingZoneDomination.controllers.DominationController;

/**
 * @author Issle
 *
 */
public class StartTask implements Runnable{

	
	@Override
	public void run() {
		DominationController.getInstance().startEvent();
		Future<?> endTask = ThreadPool.schedule(new EndTask(), 180000);
		Future<?> flagTask = ThreadPool.scheduleAtFixedRate(new FlagTask(), 0, 8000);
		DatabaseController.getInstance().setFlagTask(flagTask);
		DatabaseController.getInstance().setEndTask(endTask);
		
	}

}
