/*
 * Authors: Issle, Howler, Matim
 * File: FlagTask.java
 */
package extensions.FarmingZoneDomination.models;

import org.l2jmobius.gameserver.model.World;
import org.l2jmobius.gameserver.model.actor.instance.PlayerInstance;

import l2jsaver.Features.FarmingZoneDomination.Config;

/**
 * @author Issle
 *
 */
public class FlagTask implements Runnable {

	
	@Override
	public void run() {
		for(PlayerInstance player: World.getInstance().getPlayers())
		{
			if(player.isInsideRadius2D(Config.X, Config.Y, 0, Config.RADIUS))
			{
				player.setPvpFlagLasts(System.currentTimeMillis()+30000);
				player.startPvPFlag();
			}
		}

	}

}
