/*
 * Authors: Issle, Howler, Matim
 * File: EndTask.java
 */
package extensions.FarmingZoneDomination.models;

import extensions.FarmingZoneDomination.controllers.DominationController;

/**
 * @author Issle
 *
 */
public class EndTask implements Runnable{

	
	@Override
	public void run() {
		DominationController.getInstance().endEvent();
		DominationController.getInstance().getNextSchedule();
		
	}

}
