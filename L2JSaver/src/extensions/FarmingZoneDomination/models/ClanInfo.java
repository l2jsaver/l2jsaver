/*
 * Authors: Issle, Howler, Matim
 * File: ClanInfo.java
 */
package extensions.FarmingZoneDomination.models;

/**
 * @author Issle
 *
 */
public class ClanInfo {

	/**
	 * @param clanId
	 * @param kills
	 */
	public ClanInfo(int clanId, int kills) {
		super();
		this.clanId = clanId;
		this.kills = kills;
	}
	private int clanId;
	private int kills;
	/**
	 * @param clanId the clanId to set
	 */
	public void setClanId(int clanId) {
		this.clanId = clanId;
	}
	/**
	 * @return the clanId
	 */
	public int getClanId() {
		return clanId;
	}
	/**
	 * @param kills the kills to set
	 */
	public void setKills(int kills) {
		this.kills = kills;
	}
	/**
	 * @return the kills
	 */
	public int getKills() {
		return kills;
	}
	
}
