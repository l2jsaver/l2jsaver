/*
 * Authors: Issle, Howler, Matim
 * File: ClanStatusH.java
 */
package extensions.FarmingZoneDomination.handlers;

import l2jsaver.Features.L2JFrame.L2JFrame;
import l2jsaver.Features.StatSystem.AbstractUnlock;

import org.l2jmobius.gameserver.model.WorldObject;
import org.l2jmobius.gameserver.model.actor.instance.PlayerInstance;

import extensions.FarmingZoneDomination.controllers.DominationController;
import extensions.FarmingZoneDomination.views.HelpWindow;
import extensions.FarmingZoneDomination.views.PlayerWindow;

/**
 * @author Issle
 *
 */
public class ClanStatusH extends AbstractUnlock{


	/**
	 * @param name
	 * @param experience
	 * @param type
	 */
	public ClanStatusH() {
		super("Farming-Domination", 8000, TYPE_TEAM);
		commands = VOICED_COMMANDS;
		description = "The farming zone domination event takes place every night at the same hour. During " +
				"the event the farming zone spawn point is a battleground where you can attack people. The clan " +
				"that gets the most kills during that time, gets bonus stats for the rest of the day, till the " +
				"hext farming zone domination event.";
	}

	private static final String[] VOICED_COMMANDS =
	{
		"clanStatus","clanBonus","gmstart"
	};
	
	@Override
	public boolean useVoicedCommand(String command, PlayerInstance activeChar,
			String params) {

		if(!canUse(activeChar))
			return false;
		if(command.equalsIgnoreCase("clanStatus"))
		{
		if(!DominationController.getInstance().isActive())
		{
			activeChar.sendMessage("The Farming Zone Domination event is not running at the moment.");
			return false;
		}
		PlayerInstance target = activeChar;
		WorldObject potTarget = activeChar.getTarget();
		
		if(potTarget != null && potTarget instanceof PlayerInstance)
			target = (PlayerInstance )potTarget;
		
		if(target.getClan()==null)
		{
			activeChar.sendMessage("This player does not belong to a clan.");
			return false;
		}
		
		L2JFrame.getInstance().send(activeChar, new PlayerWindow(target));
		}
			L2JFrame.getInstance().send(activeChar, new HelpWindow());
		return false;
	}

	@Override
	public String[] getVoicedCommandList() {
		return VOICED_COMMANDS;
	}

	/* (non-Javadoc)
	 * @see l2jsaver.Features.StatSystem.AbstractUnlock#getResult(com.l2jserver.gameserver.model.actor.instance.L2PcInstance)
	 */
	@Override
	public double getResult(PlayerInstance activeChar) {
		// TODO Auto-generated method stub
		return 0;
	}

}
