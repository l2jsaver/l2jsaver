/*
 * Authors: Issle, Howler, Matim
 * File: HelpWindow.java
 */
package extensions.FarmingZoneDomination.views;

import l2jsaver.Features.FarmingZoneDomination.Config;
import l2jsaver.Features.L2JFrame.models.L2JAlign;
import l2jsaver.Features.L2JFrame.models.L2JColor;
import l2jsaver.Features.L2JFrame.models.primitive.L2JRootPane;
import l2jsaver.Features.L2JFrame.models.primitive.L2JText;
import l2jsaver.Features.L2JFrame.models.primitive.L2JVerticalPane;

/**
 * @author Issle
 *
 */
public class HelpWindow extends L2JRootPane{

	private static final String text = "The farming zone domination event takes place every night" +
			"at 21:00 GMT+2. The place around the farming zone spawn point will give all players flag " +
			"and the clan that will do the most kills inside that area, wins the event. " +
			"During the event you can see each clans kills by typing ?clanStatus on a selected player. By winning the event " +
			"you get a special bonus for one day till the next farming zone domination event. This bonus " +
			"applies to every clan member and is :<br>";
	
	public HelpWindow()
	{
		super("Farming Zone Domination");
		L2JVerticalPane panel = new L2JVerticalPane().toPanel(this);
		
		new L2JText(text).toPanel(panel);
		new L2JText(L2JColor.Green, L2JAlign.Center, "PAtk bonus: "+String.valueOf(Config.patkBonus)+"%").toPanel(panel);
		new L2JText(L2JColor.Green, L2JAlign.Center, "PDef bonus: "+String.valueOf(Config.pdefBonus)+"%").toPanel(panel);
		new L2JText(L2JColor.Green, L2JAlign.Center, "Matk bonus: "+String.valueOf(Config.matkBonus)+"%").toPanel(panel);
		new L2JText(L2JColor.Green, L2JAlign.Center, "Mdef bonus: "+String.valueOf(Config.mdefBonus)+"%").toPanel(panel);
		new L2JText(L2JColor.Green, L2JAlign.Center, "Speed bonus: "+String.valueOf(Config.speedBonus)+"%").toPanel(panel);
		
	}
}
