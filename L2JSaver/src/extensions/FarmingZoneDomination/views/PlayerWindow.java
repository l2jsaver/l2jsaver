/*
 * Authors: Issle, Howler, Matim
 * File: PlayerWindow.java
 */
package extensions.FarmingZoneDomination.views;

import l2jsaver.Features.L2JFrame.models.primitive.L2JRootPane;
import l2jsaver.Features.L2JFrame.models.primitive.L2JText;
import l2jsaver.Features.L2JFrame.models.primitive.L2JVerticalPane;

import org.l2jmobius.gameserver.data.sql.impl.ClanTable;
import org.l2jmobius.gameserver.model.actor.instance.PlayerInstance;

import extensions.FarmingZoneDomination.controllers.DominationController;
import extensions.FarmingZoneDomination.models.ClanInfo;

/**
 * @author Issle
 *
 */
public class PlayerWindow extends L2JRootPane{

	public PlayerWindow(PlayerInstance target)
	{
		super(target.getName()+"'s clan");
		L2JVerticalPane panel = new L2JVerticalPane().toPanel(this);
		
		int clanId = target.getClanId();
		ClanInfo cl = DominationController.getInstance().getClanlist().get(clanId);
		if(cl!= null)
		{
			new L2JText("Clan Name: "+ ClanTable.getInstance().getClan(cl.getClanId()).getName()).toPanel(panel);
			new L2JText("Online Count: "+ ClanTable.getInstance().getClan(cl.getClanId()).getMembersCount()).toPanel(panel);
			new L2JText("Current kills: "+ String.valueOf(cl.getKills())).toPanel(panel);
		}
		else
		{
			new L2JText("This players clan has not done any kills yet.").toPanel(panel);
		}
			
	}
}
