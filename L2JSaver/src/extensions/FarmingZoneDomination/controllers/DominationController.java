/*
 * Authors: Issle, Howler, Matim
 * File: DominationController.java
 */
package extensions.FarmingZoneDomination.controllers;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Future;

import org.l2jmobius.commons.concurrent.ThreadPool;
import org.l2jmobius.gameserver.data.sql.impl.ClanTable;
import org.l2jmobius.gameserver.data.xml.impl.NpcData;
import org.l2jmobius.gameserver.datatables.SpawnTable;
import org.l2jmobius.gameserver.geoengine.GeoEngine;
import org.l2jmobius.gameserver.model.Location;
import org.l2jmobius.gameserver.model.Spawn;
import org.l2jmobius.gameserver.model.World;
import org.l2jmobius.gameserver.model.actor.Npc;
import org.l2jmobius.gameserver.model.actor.appearance.PlayerAppearance;
import org.l2jmobius.gameserver.model.actor.instance.PlayerInstance;
import org.l2jmobius.gameserver.model.actor.templates.NpcTemplate;
import org.l2jmobius.gameserver.model.clan.Clan;
import org.l2jmobius.gameserver.model.clan.ClanMember;
import org.l2jmobius.gameserver.network.serverpackets.SocialAction;
import org.l2jmobius.gameserver.util.Broadcast;

import extensions.FarmingZoneDomination.base.DatabaseController;
import extensions.FarmingZoneDomination.models.ClanInfo;
import extensions.FarmingZoneDomination.models.StartTask;
import l2jsaver.ExtensionsAdapter;
import l2jsaver.Features.FarmingZoneDomination.Config;
import l2jsaver.Machine.Interfaces.IActor;
import l2jsaver.Machine.Interfaces.IExtension;

/**
 * @author Issle
 *
 */
public class DominationController implements IActor,IExtension{

	private Map<Integer, ClanInfo> clanlist;
	private List<Npc> npcs = new ArrayList<Npc>();
	private int NPC_ID=32343;
	private int NPC_COUNT=355;
	/**
	 * @return the clanlist
	 */
	public Map<Integer, ClanInfo> getClanlist() {
		return clanlist;
	}
	
	public int spawnNpc(int heading, int x, int y , int z)
	{
		NpcTemplate template1;
		template1 = NpcData.getInstance().getTemplate(NPC_ID);
		int objectId = 0;
		Spawn spawn;
		try {
			spawn = new Spawn(template1);
			
			Location loc = new Location(x, y, z+4);
			
			spawn.setLocation(loc);
			spawn.setAmount(1);
			spawn.setHeading(heading);
			spawn.setRespawnDelay(20000);
			spawn.setInstanceId(0);
			spawn.doSpawn();
			
			SpawnTable.getInstance().addNewSpawn(spawn, false);
			spawn.init();
			Npc _lastNpcSpawn = spawn.getLastSpawn();
			_lastNpcSpawn.setCurrentHp(_lastNpcSpawn.getMaxHp());
			_lastNpcSpawn.setTitle("Farming Domination");
			_lastNpcSpawn.isAggressive();
			_lastNpcSpawn.decayMe();
			_lastNpcSpawn.spawnMe(spawn.getLastSpawn().getX(), spawn.getLastSpawn().getY(), spawn.getLastSpawn().getZ());
			npcs.add(_lastNpcSpawn);
			objectId = _lastNpcSpawn.getObjectId();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return objectId;
		
		
	}

	/**
	 * @param clanlist the clanlist to set
	 */
	public void setClanlist(Map<Integer, ClanInfo> clanlist) {
		this.clanlist = clanlist;
	}

	private volatile static DominationController singleton;
	
	private boolean isActive = false;
	
	public void startEvent()
	{
		isActive=true;
		Broadcast.toAllOnlinePlayers("Farming Zone Domination started and will last for "+String.valueOf(Config.duration/60000)+" minutes.");
		DatabaseController.getInstance().remove(0);
		spawnNpcs();
		Config.clanId = 0;
	}
	
	/**
	 * 
	 */
	private void unspawnNpcs() {
		// TODO Auto-generated method stub
		for (Npc spawn : npcs)
		{
			spawn.deleteMe();
			
			Spawn spawn2 = spawn.getSpawn();
			if (spawn2 != null)
			{
				spawn2.stopRespawn();
				SpawnTable.getInstance().deleteSpawn(spawn2, true);
			}
		}
		npcs.clear();
	}

	/**
	 * 
	 */
	private void spawnNpcs() {
		int count =0;
		
		for(count =0; count < NPC_COUNT; count+=4)
		{
			int[] loc = calculatePoint2(Config.X,Config.Y,count,Config.RADIUS);
			spawnNpc(count*182,loc[0],loc[1],GeoEngine.getInstance().getHeight(loc[0], loc[1], 0)+15);
		}
		
	}
	
	public static int[] calculatePoint2(int x, int y, int rotate, int distance)
	{
		double angle = 0/182.044444 + rotate;
		if(angle > 360)
			angle = angle -360;
		else if(angle < 0)
			angle = 360 + angle;
		
		double angleInRads = Math.toRadians(angle);
		double dx = Math.cos(angleInRads)* distance;
		double dy = Math.sin(angleInRads)* distance;
		
		int[] result = new int[2];
		result[0] = (int) (x + dx);
		result[1] = (int) (y + dy);
		return result;
	}

	public void endEvent()
	{
		Future<?> flagTask = DatabaseController.getInstance().getFlagTask();
		if(flagTask != null)
			flagTask.cancel(true);
		isActive=false;
		rewardClans();
		unspawnNpcs();
		DatabaseController.getInstance().insert(Config.clanId);
		clanlist.clear();
	}

	private void rewardClans() 
	{
		ClanInfo bestClan = null;
		
		for(ClanInfo cl: clanlist.values())
		{
			if(bestClan==null)
				bestClan = cl;
			else if(cl.getKills()>bestClan.getKills())
				bestClan = cl;
		}
		
		for(PlayerInstance player: World.getInstance().getPlayers())
		{
			if(player.getAppearance()!= null && !player.isGM())
				player.getAppearance().setNameColor(255,255,255);
				player.broadcastUserInfo();
		}
		if(bestClan != null)
		{
			Config.clanId = bestClan.getClanId();
			Broadcast.toAllOnlinePlayers("Clan "+ClanTable.getInstance().getClan(bestClan.getClanId()).getName()+" is victorious in this Farming Zone Domination with "+ String.valueOf(bestClan.getKills())+" kills and will get the bonus till tomorrow.");
			for(ClanMember player: ClanTable.getInstance().getClan(bestClan.getClanId()).getMembers())
			{
				PlayerInstance playa = player.getPlayerInstance();
				if(playa == null)
					continue;
				PlayerAppearance ap = player.getPlayerInstance().getAppearance();
				if(ap == null)
					continue;
				
				ap.setNameColor(90, 167, 43);
				player.getPlayerInstance().broadcastPacket( new SocialAction(player.getPlayerInstance().getObjectId(), 17));
				player.getPlayerInstance().broadcastUserInfo();
			}
			
		}
		
		else
			Broadcast.toAllOnlinePlayers("No clan participated in the Farming Zone Domination this time.");
	}

	public void getNextSchedule()
	{
		Calendar c = Calendar.getInstance();
		System.out.println("It was: "+ c.getTime());
		
		c.set(Calendar.HOUR_OF_DAY, 20);
		c.set(Calendar.MINUTE,10);
		
		if(c.getTimeInMillis()<= System.currentTimeMillis())
			c.add(Calendar.DAY_OF_WEEK, 1);
		System.out.println("It is now: "+ c.getTime());
		
		
		Future<?> startTask = ThreadPool.schedule(new StartTask(), c.getTimeInMillis()- System.currentTimeMillis());
		DatabaseController.getInstance().setStartTask(startTask);
	}
	
	/**
	 * @return the isActive
	 */
	public boolean isActive() {
		return isActive;
	}

	/**
	 * @param isActive the isActive to set
	 */
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	private DominationController() {
		clanlist = new ConcurrentHashMap<Integer, ClanInfo>();
		ExtensionsAdapter.getActors().put("FarmingZoneDomination", this);
		ExtensionsAdapter.extensions.put("FarmingZoneDomination", this);
		
		getNextSchedule();
	}
	
	public void restore()
	{
		DatabaseController.getInstance().restore(0);
	}

	public static DominationController getInstance() {
		if (singleton == null) {
			synchronized (DominationController.class) {
				if (singleton == null)
					singleton = new DominationController();
			}
		}
		return singleton;
	}

	@Override
	public void onDie(PlayerInstance killed, PlayerInstance killer) {

		if(killer == null || killed == null)
			return;
		
		if(!isActive)
			return;
		
		if(!killer.isInsideRadius2D(Config.X, Config.Y, 0, Config.RADIUS))
			return;
		
		Clan clan = killer.getClan();
		
		if(clan == null)
			return;
		ClanInfo clanInfo = null;
		
		
		if(clanlist.containsKey(clan.getId()))
			clanInfo = clanlist.get(clan.getId());
		else
		{
			clanInfo = new ClanInfo(clan.getId(),0);
			clanlist.put(clan.getId(), clanInfo);
		}
		
		if(isFakeKill(killer,killed))
			return;
		
		
		clanInfo.setKills(clanInfo.getKills()+1);
		killer.sendMessage("You killed "+killed.getName()+ " and won a point for your clan.");
		killer.broadcastPacket(new SocialAction(killer.getObjectId(), SocialAction.LEVEL_UP));
		
		
	}
	
	public boolean isFakeKill(PlayerInstance killer, PlayerInstance killed)
	{
		return false;
	}

	@Override
	public void onLogin(PlayerInstance player) {
		
		Clan clan = ClanTable.getInstance().getClan(Config.clanId);
		if(clan == null)
			player.sendMessage("No clan has won the farming zone domination today.");
		else
			player.sendMessage("Clan "+ clan.getName()+ " has won todays farming zone domination event.");
		
		if(player.getClan()!= null && player.getClan().getId()==Config.clanId)
		{
			player.getAppearance().setNameColor(90, 167, 43);
			player.broadcastUserInfo();
		}
		
	}


	@Override
	public void onLogout(PlayerInstance player) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onItemUse(PlayerInstance player) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onMove(PlayerInstance player, int orX, int orY, int orZ,
			int tarX, int tarY, int tarZ) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * @param winnerClanId the winnerClanId to set
	 */
	public void setWinnerClanId(int winnerClanId) {
		Config.clanId = winnerClanId;
	}

	/**
	 * @return the winnerClanId
	 */
	public int getWinnerClanId() {
		return Config.clanId;
	}

	
	@Override
	public void onReload() {
		
		Future<?> startTask = DatabaseController.getInstance().getStartTask();
		Future<?> endTask = DatabaseController.getInstance().getEndTask();
		Future<?> flagTask = DatabaseController.getInstance().getFlagTask();
		
		if(startTask != null)
			startTask.cancel(true);
		if(endTask != null)
			endTask.cancel(true);
		if(flagTask != null)
			flagTask.cancel(true);
		
		unspawnNpcs();
		
	}

	@Override
	public void onShutDown() {
		// TODO Auto-generated method stub
		
	}
	
}
