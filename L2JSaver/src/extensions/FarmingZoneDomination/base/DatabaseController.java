/*
 * Authors: Issle, Howler, Matim
 * File: DatabaseController.java
 */
package extensions.FarmingZoneDomination.base;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.Future;

import extensions.FarmingZoneDomination.controllers.DominationController;

/**
 * @author Issle
 *
 */
public class DatabaseController extends AbstractDBController{

	private volatile static DatabaseController singleton;

	private Future<?> startTask;
	private Future<?> endTask;
	private Future<?> flagTask;
	
	private DatabaseController() {
		CREATE = "CREATE TABLE IF NOT EXISTS `FarmingZoneDomination` (" +
		"`clanId`  int(10) NOT NULL DEFAULT 0 ,"+
		"PRIMARY KEY (`clanId`) );";
		
		RESTORE = "SELECT clanId FROM FarmingZoneDomination";
		INSERT = "INSERT INTO FarmingZoneDomination(clanId) VALUES (?)";
		REMOVE = "DELETE FROM FarmingZoneDomination";
		
		migrate();
		
	}

	public static DatabaseController getInstance() {
		if (singleton == null) {
			synchronized (DatabaseController.class) {
				if (singleton == null)
					singleton = new DatabaseController();
			}
		}
		return singleton;
	}
	
	@Override
	protected void restoreImpl(ResultSet rset, Object... obj) {
		
		try {
			while(rset.next())
			{
				int clanId = rset.getInt("clanId");
				DominationController.getInstance().setWinnerClanId(clanId);
			}
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	
	@Override
	protected void restorePrepare(PreparedStatement rset, Object... obj) {
		// TODO Auto-generated method stub
		
	}

	
	@Override
	protected void saveImpl(PreparedStatement st, Object... obj) {
		// TODO Auto-generated method stub
		
	}

	
	@Override
	public void insertImpl(PreparedStatement st, Object... obj) {
		try {
			st.setInt(1, (Integer) obj[0]);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	
	@Override
	public void removeImpl(PreparedStatement st, Object... obj) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * @param startTask the startTask to set
	 */
	public void setStartTask(Future<?> startTask) {
		this.startTask = startTask;
	}

	/**
	 * @return the startTask
	 */
	public Future<?> getStartTask() {
		return startTask;
	}

	/**
	 * @param endTask the endTask to set
	 */
	public void setEndTask(Future<?> endTask) {
		this.endTask = endTask;
	}

	/**
	 * @return the endTask
	 */
	public Future<?> getEndTask() {
		return endTask;
	}

	/**
	 * @param flagTask the flagTask to set
	 */
	public void setFlagTask(Future<?> flagTask) {
		this.flagTask = flagTask;
	}

	/**
	 * @return the flagTask
	 */
	public Future<?> getFlagTask() {
		return flagTask;
	}

}
