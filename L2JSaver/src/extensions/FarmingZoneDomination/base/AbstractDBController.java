/*
 * Authors: Issle, Howler, Matim
 * File: AbstractDBController.java
 */
package extensions.FarmingZoneDomination.base;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.l2jmobius.commons.database.DatabaseFactory;

/**
 * @author Issle
 *
 */
public abstract class AbstractDBController {

	public String featureName;
	public String CREATE;
	
	public String RESTORE;
	public String INSERT;
	public String REMOVE;
	public String UPDATE;
	
	/**
	 * You just call execution from here.
	 * @param rset
	 */
	protected abstract void restoreImpl(ResultSet rset, Object... obj);
	protected abstract void restorePrepare(PreparedStatement rset, Object... obj);
	
	public void restore(Object... obj)
	{
		Connection con = null;
		try
		{
			con = DatabaseFactory.getConnection();
			PreparedStatement restore = con.prepareStatement(RESTORE);
			restorePrepare(restore, obj);
			
			ResultSet rset = restore.executeQuery();
			restoreImpl(rset, obj);
			rset.close();
			restore.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * You just call save execution in here.
	 * @param st
	 */
	protected abstract void saveImpl(PreparedStatement st, Object... obj);
	
	public void save(Object... obj)
	{
		Connection con = null;
		try
		{
			con = DatabaseFactory.getConnection();
			PreparedStatement save = con.prepareStatement(UPDATE);
			
			saveImpl(save, obj);
			
			save.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Just replace ? for now, do not call executeUpdate()
	 * it gets called from the wrapper function.
	 * @param obj
	 */
	public abstract void insertImpl(PreparedStatement st, Object... obj);
	
	public void insert(Object... obj)
	{
		
		Connection con = null;
		try
		{
			con = DatabaseFactory.getConnection();
			PreparedStatement statement = con.prepareStatement(INSERT);
			
			insertImpl(statement,obj);
			
			statement.executeUpdate();
			statement.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try { con.close(); } catch (Exception e) {}
		}
	}
	
	public abstract void removeImpl(PreparedStatement st, Object... obj);
	
	public void remove(Object... obj)
	{
		Connection con = null;
		try
		{
			con = DatabaseFactory.getConnection();
			PreparedStatement remove = con.prepareStatement(REMOVE);


			removeImpl(remove,obj);
			
			remove.execute();
			remove.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void migrate()
	{
		System.out.println("["+featureName+"] Migrating Database.");
		Connection con = null;
		try
		{
			con = DatabaseFactory.getConnection();
			PreparedStatement statement = con.prepareStatement(CREATE);
			statement.execute();
			
			statement.close();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			try { con.close(); } catch (Exception e) {}
		}
	}
}
