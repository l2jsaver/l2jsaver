/*
 * Authors: Issle, Howler, Matim
 * File: ClanReputation.java
 */
package extensions.ClanReputation;

import extensions.ClanReputation.Controller.ClanReputUnlock;
import l2jsaver.Features.StatSystem.UnlocksHolder;

/**
 * @author Issle
 *
 */
public class ClanReputation {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		UnlocksHolder.getInstance().registerUnlock(new ClanReputUnlock());

	}

}
