/*
 * Authors: Issle, Howler, Matim
 * File: ClanReputUnlock.java
 */
package extensions.ClanReputation.Controller;

import org.l2jmobius.gameserver.model.actor.instance.PlayerInstance;

import l2jsaver.Features.StatSystem.AbstractUnlock;
import l2jsaver.Model.PcModel;

/**
 * @author Issle
 *
 */
public class ClanReputUnlock extends AbstractUnlock{

	/**
	 * @param name
	 * @param experience
	 * @param type
	 */
	public ClanReputUnlock() {
		super("Reputation", 30000, TYPE_TEAM);
		description = "You can trade your solo points for clan reputation if you belong to a clan. Use the following" +
				" command to trade 5000 solo points for 500 clan reputation.";
		commands = new String[1];
		commands[0] = "reputation";
	}

	@Override
	public boolean useVoicedCommand(String command, PlayerInstance player, String params)
	{
		if(!canUse(player))
			return false;
		
		if(player.getClan()== null)
		{
			player.sendMessage("You cannot use this unlock because you dont have a clan.");
			return false;
		}
		
		PcModel model = PcModel.getModel(player);
		
		if(model == null)
			return false;
		
		int soloPoints = model.spartanInstance.getSoloPoints();
		
		if(soloPoints < 5000 && !player.isGM())
		{
			player.sendMessage("You do not have enough solo points, come back when you have 5000.");
			return false;
		}
		if(!player.isGM())
			model.spartanInstance.setSoloPoints(model.spartanInstance.getSoloPoints()-5000);
		
		player.getClan().addReputationScore(500, true);
		
		player.sendMessage("5000 solo points have been traded for 500 clan reputation.");
		
		return false;
	}
	@Override
	public double getResult(PlayerInstance activeChar) {
		// TODO Auto-generated method stub
		return 0;
	}

}
