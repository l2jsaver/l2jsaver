package extensions.DragonInvasion.Handlers;

import org.l2jmobius.gameserver.model.actor.instance.PlayerInstance;
import org.l2jmobius.gameserver.util.Broadcast;

import extensions.DragonInvasion.Controllers.DragonManager;
import l2jsaver.interfaces.ISaverCommandHandler;

public class DragonInvasionAdminH implements ISaverCommandHandler
{
	private static final String[] VOICED_COMMANDS =
	{
		"startDragonInvasion","stopDragonInvasion"
	};
	
	public boolean useVoicedCommand(String command, PlayerInstance activeChar, String params)
	{
		if (command.equalsIgnoreCase("startDragonInvasion"))
		{
			DragonManager.spawnDragons();
			Broadcast.toAllOnlinePlayers("Dragon Invasion started by admin.");
		}
		else if(command.equalsIgnoreCase("stopDragonInvasion"))
		{
			DragonManager.unspawnDragons();
			Broadcast.toAllOnlinePlayers("Dragon Invasion stopped by admin.");
		}
		return true;
	}
	
	public String[] getVoicedCommandList()
	{
		return VOICED_COMMANDS;
	}
	
	@Override
	public boolean forGM() {
		// TODO Auto-generated method stub
		return false;
	}
}