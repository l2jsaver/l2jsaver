/*
 * Authors: Issle, Howler, Matim
 * File: DragonManager.java
 */
package extensions.DragonInvasion.Controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;

import org.l2jmobius.commons.concurrent.ThreadPool;
import org.l2jmobius.commons.util.Rnd;
import org.l2jmobius.gameserver.data.xml.impl.NpcData;
import org.l2jmobius.gameserver.datatables.SpawnTable;
import org.l2jmobius.gameserver.geoengine.GeoEngine;
import org.l2jmobius.gameserver.instancemanager.ZoneManager;
import org.l2jmobius.gameserver.model.Location;
import org.l2jmobius.gameserver.model.Spawn;
import org.l2jmobius.gameserver.model.actor.Npc;
import org.l2jmobius.gameserver.model.actor.templates.NpcTemplate;
import org.l2jmobius.gameserver.model.zone.type.PeaceZone;
import org.l2jmobius.gameserver.model.zone.type.WaterZone;
import org.l2jmobius.gameserver.network.serverpackets.Earthquake;
import org.l2jmobius.gameserver.network.serverpackets.ExRedSky;
import org.l2jmobius.gameserver.network.serverpackets.MagicSkillUse;
import org.l2jmobius.gameserver.util.Broadcast;

import extensions.DragonInvasion.Data.Config;
import extensions.DragonInvasion.Data.SpawnTask;
import extensions.DragonInvasion.Data.UnspawnTask;
import l2jsaver.ExtensionsAdapter;
import l2jsaver.Machine.Interfaces.IExtension;

/**
 * @author Issle
 *
 */
public class DragonManager implements IExtension{

	private static List<Npc> dragons;
	private static Future<?> startTask;
	private static Future<?> endTask;
	
	private DragonManager()
	{
		ExtensionsAdapter.extensions.put("DragonInvasion",this);
		dragons = new ArrayList<>();
		spawnDragons();
	}
	
	public static void spawnDragons()
	{
		spawnDragonRandomly(Config.bigDragonId,Config.bigDragonCount);
		spawnDragonRandomly(Config.mediumDragonId,Config.mediumDragonCount);
		spawnDragonRandomly(Config.smallDragonId,Config.smallDragonCount);
		endTask = ThreadPool.schedule(new UnspawnTask(), Config.eventDuration);
		Broadcast.toAllOnlinePlayers(Config.announce_start);
		
		ExRedSky packet = new ExRedSky(10);
		Earthquake eq = new Earthquake(Config.areaX, Config.areaY, Config.areaZ, 14, 10);
		Broadcast.toKnownPlayersInRadius(dragons.get(0), eq, Config.areaRange*2);
		Broadcast.toKnownPlayersInRadius(dragons.get(0), packet, Config.areaRange*2);
	}
	
	private static void spawnDragonRandomly(int id , int count)
	{
		NpcTemplate template1;
		template1 = NpcData.getInstance().getTemplate(id);
		
		Spawn spawn;
		try {
			spawn = new Spawn(template1);
			
			int x = Config.areaX+ Rnd.get(0,Config.areaRange)-Config.areaRange/2;
			int y = Config.areaY+ Rnd.get(0,Config.areaRange)-Config.areaRange/2;
			int z = GeoEngine.getInstance().getHeight(x,y,0);
			int k = 0;
			while(ZoneManager.getInstance().getZone(x, y, z, WaterZone.class) != null && ZoneManager.getInstance().getZone(x, y, z, PeaceZone.class) != null && k<1000)
			{
				x = Config.areaX+ Rnd.get(0,Config.areaRange)-Config.areaRange/2;
				y = Config.areaY+ Rnd.get(0,Config.areaRange)-Config.areaRange/2;
				z = GeoEngine.getInstance().getHeight(x,y,0);
				k++;
			}
			
			Location loc = new Location(x, y, GeoEngine.getInstance().getHeight(x, y, 0)+70);
			spawn.setLocation(loc);
			spawn.setAmount(1);
			spawn.setHeading(0);
			spawn.setRespawnDelay(Config.eventDuration);
			spawn.setInstanceId(0);
			spawn.doSpawn();
			
			SpawnTable.getInstance().addNewSpawn(spawn, false);
			spawn.init();
			Npc _lastNpcSpawn = spawn.getLastSpawn();
			_lastNpcSpawn.setCurrentHp(_lastNpcSpawn.getMaxHp());
			_lastNpcSpawn.setTitle("Dragon Invasion");
			_lastNpcSpawn.isAggressive();
			_lastNpcSpawn.decayMe();
			_lastNpcSpawn.spawnMe(spawn.getLastSpawn().getX(), spawn.getLastSpawn().getY(), spawn.getLastSpawn().getZ());
			_lastNpcSpawn.broadcastPacket(new MagicSkillUse(_lastNpcSpawn, _lastNpcSpawn, 1034, 1, 1, 1));
			dragons.add(_lastNpcSpawn);
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void unspawnDragons()
	{
		startTask = ThreadPool.schedule(new SpawnTask(), Config.eventPeriod + Rnd.get(-Config.eventPeriod/2 , +Config.eventPeriod/2));
		
		for (Npc spawn : dragons)
		{
			spawn.deleteMe();
			
			Spawn spawn2 = spawn.getSpawn();
			if (spawn2 != null)
			{
				spawn2.stopRespawn();
				SpawnTable.getInstance().deleteSpawn(spawn2, true);
			}
		}
		dragons.clear();
		Broadcast.toAllOnlinePlayers(Config.announce_end);
	}


	@Override
	public void onReload() {
		unspawnDragons();
		startTask.cancel(true);
		endTask.cancel(true);
		
		
	}

	@Override
	public void onShutDown() {
		// TODO Auto-generated method stub
		
	}
	
	private volatile static DragonManager singleton;

	public static DragonManager getInstance() {
		if (singleton == null) {
			synchronized (DragonManager.class) {
				if (singleton == null)
					singleton = new DragonManager();
			}
		}
		return singleton;
	}
}
