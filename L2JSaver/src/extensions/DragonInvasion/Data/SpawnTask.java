/*
 * Authors: Issle, Howler, Matim
 * File: SpawnTask.java
 */
package extensions.DragonInvasion.Data;

import extensions.DragonInvasion.Controllers.DragonManager;

/**
 * @author Issle
 *
 */
public class SpawnTask implements Runnable{

	@Override
	public void run() {
		
		DragonManager.spawnDragons();
		
	}

}
