/*
 * Authors: Issle, Howler, Matim
 * File: UnspawnTask.java
 */
package extensions.DragonInvasion.Data;

import extensions.DragonInvasion.Controllers.DragonManager;

/**
 * @author Issle
 *
 */
public class UnspawnTask implements Runnable{

	@Override
	public void run() {

		DragonManager.unspawnDragons();
		
	}

}
