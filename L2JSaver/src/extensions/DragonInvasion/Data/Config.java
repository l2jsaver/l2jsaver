/*
 * Authors: Issle, Howler, Matim
 * File: Config.java
 */
package extensions.DragonInvasion.Data;

/**
 * @author Issle
 *
 */
public class Config {

	public static int areaX=74641;
	public static int areaY=143582;
	public static int areaZ=-3765;
	public static int areaRange=500;
	
	public static int bigDragonId=999991;
	public static int bigDragonCount=1;
	
	public static int mediumDragonId=999992;
	public static int mediumDragonCount=1;
	
	public static int smallDragonId=999993;
	public static int smallDragonCount=1;
	
	public static String announce_start= "Dragon Invasion has started outside Giran.";
	public static String announce_end= "The Dragon Invasion has ended.";
	
	public static int eventPeriod=28800000;
	public static int eventDuration=600000;
}
