/*
 * Authors: Issle, Howler, Matim
 * File: TreasureHuntingManager.java
 */
package extensions.TreasureHunting.Base;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.l2jmobius.commons.database.DatabaseFactory;

import extensions.TreasureHunting.Controller.TreasureController;
import extensions.TreasureHunting.Model.Treasure;

/**
 * @author Howler
 *
 */
public class TreasureHuntingManager 
{
	private String MIGRATE_TH = "CREATE TABLE IF NOT EXISTS `TreasureHunting` (" +
								"`ownerId`  int(10) NOT NULL DEFAULT 0 ," +
								"`ownerName`  varchar(35) NOT NULL DEFAULT 'None' ," +
								"`itemCount`  bigint(20) NOT NULL ," +
								"`x`  mediumint(9) NOT NULL DEFAULT 0 ," +
								"`y`  mediumint(9) NOT NULL DEFAULT 0 ," +
								"`z`  mediumint(9) NOT NULL DEFAULT 0 ," +
								"PRIMARY KEY (`ownerId`) );";	
	
	private String RESTORE_TH = "SELECT ownerId, ownerName, itemCount, x, y, z FROM TreasureHunting";
	private String INSERT_TH = "INSERT INTO TreasureHunting(ownerId, ownerName, itemCount, x, y, z) VALUES (?,?,?,?,?,?)";
	private String REMOVE_TH  = "DELETE FROM TreasureHunting WHERE ownerId=?";
	private String UPDATE_TH = "UPDATE TreasureHunting SET ownerName=?, itemCount=?, x=?, y=?, z=? WHERE ownerId=?";
	
	public void migrate()
	{
		System.out.println("[TreasureHunting] Migrating Database.");
		Connection con = null;
		try
		{
			con = DatabaseFactory.getConnection();
			PreparedStatement statement = con.prepareStatement(MIGRATE_TH);
			statement.execute();
			
			statement.close();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			try { con.close(); } catch (Exception e) {}
		}
	}
	
	public void restoreTh()
	{
		Connection con = null;
		try
		{
			con = DatabaseFactory.getConnection();
			PreparedStatement restore = con.prepareStatement(RESTORE_TH);
			
			ResultSet rset = restore.executeQuery();
			while(rset.next())
			{
				Treasure t = new Treasure(rset.getInt("ownerId"), rset.getLong("itemCount"), rset.getString("ownerName"));
				t.setX(rset.getInt("x"));
				t.setY(rset.getInt("y"));
				t.setZ(rset.getInt("z"));
				TreasureController.getInstance().addTreasure(TreasureController.getInstance().spawnNpc(0, t.getX(), t.getY(), t.getZ()), t.getOwnerId(), t);// 1 Should be object id of monster - call spawn on each loop. and then add it.
			}
			rset.close();
			restore.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try { con.close(); } catch (Exception e) {}
		}
	}
	
	public void saveTh()
	{
		Connection con = null;
		try
		{
			con = DatabaseFactory.getConnection();
			PreparedStatement save = con.prepareStatement(UPDATE_TH);
			
			for (Treasure t : TreasureController.getInstance().getTreasures())
			{
				save.setString(1, t.getOwnerName());
				save.setLong(2, t.getItemCount());
				save.setInt(3, t.getX());
				save.setInt(4, t.getY());
				save.setInt(5, t.getZ());
				save.setInt(6, t.getOwnerId());
				
				save.execute();
			}
			
			save.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try { con.close(); } catch (Exception e) {}
		}
	}
	
	public synchronized void insertTp(int ownerId, String name, long itemCount, int x, int y, int z)
	{
		
		Connection con = null;
		try
		{
			con = DatabaseFactory.getConnection();
			PreparedStatement statement = con.prepareStatement(INSERT_TH);
			
			statement.setInt(1, ownerId);
			statement.setString(2, name);
			statement.setLong(3, itemCount);
			statement.setInt(4, x);
			statement.setInt(5, y);
			statement.setInt(6, z);
			
			
			statement.executeUpdate();
			statement.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try { con.close(); } catch (Exception e) {}
		}
	}
	
	public synchronized void removeTp(int ownerId)
	{
		Connection con = null;
		try
		{
			con = DatabaseFactory.getConnection();
			PreparedStatement remove = con.prepareStatement(REMOVE_TH);
			remove.setInt(1, ownerId);
			
			remove.execute();
			remove.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try { con.close(); } catch (Exception e) {}
		}
	}
	private volatile static TreasureHuntingManager singleton;

	private TreasureHuntingManager() {
	}

	public static TreasureHuntingManager getInstance() {
		if (singleton == null) {
			synchronized (TreasureHuntingManager.class) {
				if (singleton == null)
					singleton = new TreasureHuntingManager();
			}
		}
		return singleton;
	}
}
