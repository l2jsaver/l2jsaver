/*
 * Authors: Issle, Howler, Matim
 * File: TreasureUpdateTask.java
 */
package extensions.TreasureHunting.Model;

import org.l2jmobius.gameserver.model.World;
import org.l2jmobius.gameserver.model.actor.instance.PlayerInstance;

import extensions.TreasureHunting.Controller.TreasureController;

/**
 * @author Issle
 *
 */
public class TreasureUpdateTask implements Runnable{

	public static double multiplier = 1.001;
	@Override
	public void run() {
		
		TreasureController.antispam.clear();
		for(Treasure t: TreasureController.getInstance().getTreasures())
		{
			int playerId = t.getOwnerId();
			long count = t.getItemCount();
			
			count*=multiplier;
			count++;
			
			if(count <=0 || count >= TreasureController.MAX_COUNT)
				continue;
			
			t.setItemCount((int) count);
			
			PlayerInstance player = World.getInstance().getPlayer(playerId);
			if(player != null)
				player.sendMessage("Your treasure is now "+String.valueOf(count)+" Festival Adena");
			
			
		}
		
	}

}
