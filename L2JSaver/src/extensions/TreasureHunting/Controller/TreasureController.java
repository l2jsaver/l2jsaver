/*
 * Authors: Issle, Howler, Matim
 * File: TreasureController.java
 */
package extensions.TreasureHunting.Controller;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Future;

import org.l2jmobius.commons.concurrent.ThreadPool;
import org.l2jmobius.gameserver.data.xml.impl.NpcData;
import org.l2jmobius.gameserver.datatables.SpawnTable;
import org.l2jmobius.gameserver.model.Location;
import org.l2jmobius.gameserver.model.Spawn;
import org.l2jmobius.gameserver.model.World;
import org.l2jmobius.gameserver.model.actor.Creature;
import org.l2jmobius.gameserver.model.actor.Npc;
import org.l2jmobius.gameserver.model.actor.Summon;
import org.l2jmobius.gameserver.model.actor.instance.PlayerInstance;
import org.l2jmobius.gameserver.model.actor.templates.NpcTemplate;
import org.l2jmobius.gameserver.model.items.instance.ItemInstance;
import org.l2jmobius.gameserver.util.Broadcast;

import extensions.TreasureHunting.Base.TreasureHuntingManager;
import extensions.TreasureHunting.Model.Treasure;
import extensions.TreasureHunting.Model.TreasureUpdateTask;
import l2jsaver.ExtensionsAdapter;
import l2jsaver.Machine.Interfaces.IExtension;
import l2jsaver.Machine.Interfaces.INpc;

/**
 * @author Issle
 *
 */
public class TreasureController implements INpc, IExtension {
	
	public static final long MAX_COUNT = 99900000000L;
	private static final int ITEM_ID = 6673;
	private static final int NPC_ID = 18265;
	private Map<Integer, Treasure> byMob = new ConcurrentHashMap<Integer,Treasure>();
	private Map<Integer, Treasure> byPlayer = new ConcurrentHashMap<Integer,Treasure>();
	public static Map<Integer,Integer> antispam = new ConcurrentHashMap<Integer,Integer>();
	private Future<?> task;
	
	public Collection<Treasure> getTreasures()
	{
		return byPlayer.values();
	}
	
	public Treasure getTreasureByMob(int mobId)
	{
		return byMob.get(mobId);
	}
	
	public Treasure getTreasureByPlayer(int playerId)
	{
		return byPlayer.get(playerId);
	}
	
	public void addTreasure(int mobId, int playerId, Treasure t)
	{
		byMob.put(mobId, t);
		byPlayer.put(playerId,t);
	}
	
	public void spawnTreasures()
	{
		int count = 0;
		for(Treasure t: byMob.values())
		{
			spawnNpc(0, t.getX(), t.getY(), t.getZ());
			count++;
		}
		System.out.println("Spawned "+ count+ " treasures.");
	}
	
	public void generateTreasure(PlayerInstance player, long count)
	{
		if(byPlayer.containsKey(player.getObjectId()))
		{
			player.sendMessage("You have already spawned your treasure.");
			return;
		}
		
		if(antispam.containsKey(player.getObjectId()))
		{
			player.sendMessage("You are spawning treasures way too fast. Wait 5 minutes.");
			if(!player.isGM())
				return;
		}
		Treasure t = new Treasure(player.getObjectId(),count, player.getName());
		
		ItemInstance item = player.getInventory().getItemByItemId(ITEM_ID);
		
		if(item == null || item.getCount()<count)
		{
			player.sendMessage("You dont have that amount.");
			return;
		}
		
		t.setItemCount(count);
		player.destroyItem("Treasure Hunting", item, count, null, true);
		
		t.setX(player.getX());
		t.setY(player.getY());
		t.setZ(player.getZ());
		t.setOwnerName(player.getName());
		t.setOwnerId(player.getObjectId());
		addTreasure(spawnNpc(player.getHeading(),player.getX(), player.getY(), player.getZ()), player.getObjectId(),t);
		antispam.put(player.getObjectId(), 0);
		TreasureHuntingManager.getInstance().insertTp(player.getObjectId(), player.getName(), count, player.getX(), player.getY(), player.getZ());
	}
	
	public int spawnNpc(int heading, int x, int y , int z)
	{
		NpcTemplate template1;
		template1 = NpcData.getInstance().getTemplate(NPC_ID);
		int objectId = 0;
		Spawn spawn;
		try {
			spawn = new Spawn(template1);
		
			Location loc = new Location(x, y, z+4);
			
			spawn.setLocation(loc);
			spawn.setAmount(1);
			spawn.setHeading(heading);
			spawn.setRespawnDelay(20000);
			spawn.setInstanceId(0);
			spawn.doSpawn();
			
			SpawnTable.getInstance().addNewSpawn(spawn, false);
			spawn.init();
			Npc _lastNpcSpawn = spawn.getLastSpawn();
			_lastNpcSpawn.setCurrentHp(_lastNpcSpawn.getMaxHp());
			_lastNpcSpawn.setTitle("Treasure Hunting");
			_lastNpcSpawn.isAggressive();
			_lastNpcSpawn.decayMe();
			_lastNpcSpawn.spawnMe(spawn.getLastSpawn().getX(), spawn.getLastSpawn().getY(), spawn.getLastSpawn().getZ());
			
			objectId = _lastNpcSpawn.getObjectId();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return objectId;
		
		
	}
	
	private volatile static TreasureController singleton;

	private TreasureController() {
		ExtensionsAdapter.npcImplementors.put("treasure hunting", this);
		ExtensionsAdapter.extensions.put("TreasureHunting", this);
		task = ThreadPool.scheduleAtFixedRate(new TreasureUpdateTask(),0, 600000);
	}

	public static TreasureController getInstance() {
		if (singleton == null) {
			synchronized (TreasureController.class) {
				if (singleton == null)
					singleton = new TreasureController();
			}
		}
		return singleton;
	}

	/* (non-Javadoc)
	 * @see l2jsaver.Machine.Interfaces.IExtension#onReload()
	 */
	@Override
	public void onReload() {
		TreasureHuntingManager.getInstance().saveTh();
		if(task != null)
			task.cancel(true);
		unSpawnNpcs();
	}
	
	public void unSpawnNpcs()
	{
		int count =0;
		for (int objectId : byMob.keySet())
		{
			Npc npc = (Npc) World.getInstance().findObject(objectId);
			
			if(npc == null)
				continue;
			Spawn spawn = npc.getSpawn();
			if (spawn != null)
			{
				spawn.stopRespawn();
				SpawnTable.getInstance().deleteSpawn(spawn, true);
				count++;
			}
			
			npc.deleteMe();
			
		}
		System.out.println("Treasure hunting: Unspawned "+ count+" NPC chests.");
	}

	@Override
	public void onDie(Creature killer, Creature killed) {

		if(!byMob.containsKey(killed.getObjectId()))
			return;
		
		Treasure t = byMob.get(killed.getObjectId());
		
		PlayerInstance player = null;
		
		if(killer instanceof PlayerInstance)
		{
			player = (PlayerInstance)killer;
		}
		else if(killer instanceof Summon)
		{
			player = ((Summon)killer).getOwner();
		}
		
		if(player == null)
		{
			System.out.println("Something went wrong with treasure hunting.");
			return;
		}
		
		player.addItem("Treasure Hunting", ITEM_ID, t.getItemCount(), null, true);
		if(!player.getName().equals(t.getOwnerName()))
			Broadcast.toAllOnlinePlayers(player.getName()+" found the treasure of "+t.getOwnerName());
		byMob.remove(killed.getObjectId());
		byPlayer.remove(t.getOwnerId());
		TreasureHuntingManager.getInstance().removeTp(t.getOwnerId());
	}

	@Override
	public void onShutDown() {
		TreasureHuntingManager.getInstance().saveTh();
		
		if(task != null)
			task.cancel(true);
	}
}
