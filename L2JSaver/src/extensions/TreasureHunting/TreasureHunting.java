/*
 * Authors: Issle, Howler, Matim
 * File: TreasureHunting.java
 */
package extensions.TreasureHunting;

import l2jsaver.Features.StatSystem.UnlocksHolder;
import extensions.TreasureHunting.Base.TreasureHuntingManager;
import extensions.TreasureHunting.Handlers.TreasureHandler;

/**
 * @author Issle
 *
 */
public class TreasureHunting {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		TreasureHuntingManager.getInstance().migrate();
		TreasureHuntingManager.getInstance().restoreTh();
		UnlocksHolder.getInstance().registerUnlock(new TreasureHandler());
	}

}
