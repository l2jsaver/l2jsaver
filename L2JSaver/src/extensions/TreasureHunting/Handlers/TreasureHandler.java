package extensions.TreasureHunting.Handlers;

import l2jsaver.Features.StatSystem.AbstractUnlock;

import org.l2jmobius.gameserver.model.actor.instance.PlayerInstance;

import extensions.TreasureHunting.Controller.TreasureController;
import extensions.TreasureHunting.Model.Treasure;

public class TreasureHandler extends AbstractUnlock
{
	
	/**
	 * @param name
	 * @param experience
	 * @param type
	 */
	public TreasureHandler() {
		super("Treasure-Hunt", 1000, AbstractUnlock.TYPE_SOLO);
		description = "This system allows you to transform some of your money into a treasure chest. " +
				"When you use the unlock the money will get increased by themselves each 5 minutes. You can " +
				"get the increased money back by killing the treasure chest. Beware that others can find your chest " +
				"if you dont hide it well in some isolated area.";
		
		commands = VOICED_COMMANDS;
	}


	private static final String[] VOICED_COMMANDS =
	{
		"hide", "treasure"
	};
	
	
	public boolean useVoicedCommand(String command, PlayerInstance activeChar, String params)
	{
		if(!canUse(activeChar))
			return false;
		
		if(command.equalsIgnoreCase("hide"))
		{
			if(params == null)
			{
				activeChar.sendMessage("Specify the amount you want to convert to a treasure. For example ?hide 5000");
				return false;
			}
			
			long count = 0;
			try{ count = Long.parseLong(params);}catch(Exception e){activeChar.sendMessage("That is not a valid number.");return false;}
			
			if(count <=0 || count >= TreasureController.MAX_COUNT)
			{
				activeChar.sendMessage("Dont try to cheat ... ");
				return false;
			}
			TreasureController.getInstance().generateTreasure(activeChar, count);
		}
		else if(command.equalsIgnoreCase("treasure"))
		{
			Treasure t = TreasureController.getInstance().getTreasureByPlayer(activeChar.getObjectId());
			if(t != null)
			{
				activeChar.sendMessage("Your treasure has not been found by anyone yet and it is : "+String.valueOf(t.getItemCount()));
			}
			else 
			{
				activeChar.sendMessage("You do not have a treasure or your treasure has been found by some player.");
			}
			activeChar.sendMessage("There are "+TreasureController.getInstance().getTreasures().size() +" treasures hidden in the map.");
		}
		
		return false;
	}
	
	
	public String[] getVoicedCommandList()
	{
		return VOICED_COMMANDS;
	}


	@Override
	public double getResult(PlayerInstance activeChar) {
		// TODO Auto-generated method stub
		return 0;
	}
}
