/*
 * Authors: Issle, Howler, David
 * File: EventManagementHeader.java
 */
package extensions.Admin.Windows;

import extensions.Admin.Controllers.AdminController;
import l2jsaver.Features.L2JFrame.models.L2JAlign;
import l2jsaver.Features.L2JFrame.models.primitive.L2JButton;
import l2jsaver.Features.L2JFrame.models.primitive.L2JHorizontalPane;
import l2jsaver.Features.L2JFrame.models.primitive.L2JVerticalPane;
import l2jsaver.Features.L2JFrame.models.primitive.L2JButton.L2JButtonType;

/**
 * @author Matim
 */
public class AdminHeader extends L2JVerticalPane
{	
	private AdminHeader()
	{
		super();
		
		L2JHorizontalPane hp = new L2JHorizontalPane(L2JAlign.Center).toPanel(this);
		L2JButton main = new L2JButton("Main", L2JButtonType.GRACIA_SMALL2, AdminController.getInstance(), L2JAlign.Center);
		L2JButton events = new L2JButton("Events", L2JButtonType.GRACIA_SMALL2, AdminController.getInstance(), L2JAlign.Center);
		L2JButton admin2 = new L2JButton("Admin", L2JButtonType.GRACIA_SMALL2, null, L2JAlign.Center);
		L2JButton admin3 = new L2JButton("Admin", L2JButtonType.GRACIA_SMALL2, null, L2JAlign.Center);
		
		main.addArgument("mainWindow");
		events.addArgument("eventManagment");
		
		hp.addGraphic(main);
		hp.addGraphic(events);
		hp.addGraphic(admin2);
		hp.addGraphic(admin3);
		newLine();
	}
	
	private static class SingletonHolder
	{
		public static final AdminHeader INSTANCE = new AdminHeader();
	}

	public static AdminHeader getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
}