/*
 * Authors: Issle, Howler, David
 * File: AdminEventConfig.java
 */
package extensions.Admin.Windows;

import l2jsaver.Features.L2JFrame.models.L2JAlign;
import l2jsaver.Features.L2JFrame.models.primitive.L2JHorizontalPane;
import l2jsaver.Features.L2JFrame.models.primitive.L2JRootPane;
import l2jsaver.Features.L2JFrame.models.primitive.L2JText;
import l2jsaver.Features.L2JFrame.models.primitive.L2JTextBox;
import l2jsaver.Features.L2JFrame.models.primitive.L2JVerticalPane;

/**
 * @author Administrator
 *
 */
public class AdminEventConfig extends L2JRootPane
{
	public AdminEventConfig()
	{
		super("Event Config");
		
		L2JVerticalPane panel = new L2JVerticalPane().toPanel(this);
		L2JHorizontalPane hpanel = new L2JHorizontalPane(L2JAlign.Center).toPanel(panel);
		L2JText text1 = new L2JText("Minimum Players:");
		L2JTextBox tb = new L2JTextBox("minPlayers");
		
		hpanel.addGraphic(text1);
		hpanel.addGraphic(tb);
		
		L2JVerticalPane panel2 = new L2JVerticalPane().toPanel(this);
		L2JHorizontalPane hpanel2 = new L2JHorizontalPane(L2JAlign.Center).toPanel(panel2);
		L2JText text2 = new L2JText("Minimum Players:");
		L2JTextBox tb2 = new L2JTextBox("minPlayers");
		
		hpanel2.addGraphic(text2);
		hpanel2.addGraphic(tb2);
		
		L2JVerticalPane panel3 = new L2JVerticalPane().toPanel(this);
		L2JHorizontalPane hpanel3 = new L2JHorizontalPane(L2JAlign.Center).toPanel(panel3);
		L2JText text3 = new L2JText("Minimum Players:");
		L2JTextBox tb3 = new L2JTextBox("minPlayers");
		
		hpanel3.addGraphic(text3);
		hpanel3.addGraphic(tb3);
	}
}