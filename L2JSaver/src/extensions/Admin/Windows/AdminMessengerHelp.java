/*
 * Authors: Issle, Howler, David
 * File: AdminMessengerHelp.java
 */
package extensions.Admin.Windows;

import l2jsaver.Features.L2JFrame.models.L2JAlign;
import l2jsaver.Features.L2JFrame.models.L2JColor;
import l2jsaver.Features.L2JFrame.models.primitive.L2JRootPane;
import l2jsaver.Features.L2JFrame.models.primitive.L2JText;

/**
 * @author Matim
 */
public class AdminMessengerHelp  extends L2JRootPane
{
	public AdminMessengerHelp()
	{
		super("L2JSaver Admin");
		
		String description = "Admin Messenger feature allow you to send global, different types of message to online players. Simply type your message in text box, select message type and press send button!";
		
		addGraphic(AdminHeader.getInstance());
		new L2JText(L2JColor.Yellow, L2JAlign.Center, "Messenger Help: ").toPanel(this);
		newLine();
		new L2JText(description).toPanel(this);
	}
}