/*
 * Authors: Issle, Howler, David
 * File: AdminWindow.java
 */
package extensions.Admin.Windows;

import extensions.Admin.Controllers.AdminController;
import extensions.Admin.Controllers.AdminMessengerController;
import l2jsaver.Features.L2JFrame.models.L2JAlign;
import l2jsaver.Features.L2JFrame.models.L2JColor;
import l2jsaver.Features.L2JFrame.models.primitive.L2JButton;
import l2jsaver.Features.L2JFrame.models.primitive.L2JComboBox;
import l2jsaver.Features.L2JFrame.models.primitive.L2JHorizontalPane;
import l2jsaver.Features.L2JFrame.models.primitive.L2JMultiTextBox;
import l2jsaver.Features.L2JFrame.models.primitive.L2JRootPane;
import l2jsaver.Features.L2JFrame.models.primitive.L2JText;
import l2jsaver.Features.L2JFrame.models.primitive.L2JVerticalPane;
import l2jsaver.Features.L2JFrame.models.primitive.L2JButton.L2JButtonType;

/**
 * @author Matim
 */
public class AdminMessenger extends L2JRootPane
{
	public AdminMessenger()
	{
		super("L2JSaver Admin");
		
		addGraphic(AdminHeader.getInstance());
		L2JVerticalPane buttonPanel = new L2JVerticalPane();
		L2JHorizontalPane hp = new L2JHorizontalPane(L2JAlign.Center);
		L2JMultiTextBox textBox = new L2JMultiTextBox("elo", 270, 70);
		L2JComboBox msgType = new L2JComboBox("msgType");
		L2JButton send = new L2JButton("Send Global Msg", L2JButtonType.GRACIA, AdminMessengerController.getInstance(), L2JAlign.Center);
		L2JText text1 = new L2JText(L2JColor.Yellow, L2JAlign.Center, "Enter Message:");
		L2JText text2 = new L2JText(L2JColor.Yellow, L2JAlign.Center, "Choice Message Type:");
		L2JButton help = new L2JButton("", L2JButtonType.QUESTION_MARK_ICON, AdminController.getInstance(), L2JAlign.Center);
		
		msgType.addChoice("BIGWHITE");
		msgType.addChoice("PARTY");
		msgType.addChoice("ANNOUNCE");
		msgType.addChoice("COMMANDER");
		msgType.addChoice("CRITICAL");
		msgType.addChoice("SCREEN");
		msgType.addChoice("SHOUT");
		msgType.addChoice("BATTLEFIELD");
		msgType.addChoice("CLAN");
		msgType.addChoice("ALLIANCE");
		msgType.addChoice("HERO");
		
		send.addRegistrable(msgType);
		send.addRegistrable(textBox);
		
		help.addArgument("messengerHelpWindow");
		
		hp.addGraphic(msgType);
		hp.addGraphic(send);
		hp.addGraphic(help);
		buttonPanel.addGraphic(hp);
		addGraphic(text1);
		addGraphic(textBox);
		newLine();
		addGraphic(text2);
		addGraphic(buttonPanel);
	}
}