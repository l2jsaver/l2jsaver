/*
 * Authors: Issle, Howler, David
 * File: AdminReload.java
 */
package extensions.Admin.Windows;

import java.io.File;

import l2jsaver.Features.L2JFrame.models.L2JAlign;
import l2jsaver.Features.L2JFrame.models.L2JColor;
import l2jsaver.Features.L2JFrame.models.primitive.L2JLink;
import l2jsaver.Features.L2JFrame.models.primitive.L2JRootPane;
import l2jsaver.Features.L2JFrame.models.primitive.L2JText;
import extensions.Admin.Controllers.AdminController;

/**
 * @author Matim
 */
public class AdminReload extends L2JRootPane
{
	public AdminReload()
	{
		super("L2JSaver Admin");
		
		addGraphic(AdminHeader.getInstance());
		new L2JText(L2JColor.Yellow, L2JAlign.Center, "Reload Extension:").toPanel(this);
		newLine();
		
		for (File f : AdminController.getInstance().getDirectory())
		{
			addGraphic(createLink(f.getName()));
		}
	}
	
	public L2JLink createLink(String dir)
	{
		L2JLink _dir = new L2JLink(dir, AdminController.getInstance(), L2JAlign.Center);
		_dir.addArgument("reload");
		_dir.addArgument(dir);
		
		return _dir;
	}
}