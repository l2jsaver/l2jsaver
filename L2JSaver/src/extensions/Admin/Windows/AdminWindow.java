/*
 * Authors: Issle, Howler, David
 * File: AdminWindow.java
 */
package extensions.Admin.Windows;

import l2jsaver.Features.L2JFrame.models.L2JAlign;
import l2jsaver.Features.L2JFrame.models.L2JColor;
import l2jsaver.Features.L2JFrame.models.primitive.L2JButton;
import l2jsaver.Features.L2JFrame.models.primitive.L2JHorizontalPane;
import l2jsaver.Features.L2JFrame.models.primitive.L2JRootPane;
import l2jsaver.Features.L2JFrame.models.primitive.L2JVerticalPane;
import l2jsaver.Features.L2JFrame.models.primitive.L2JButton.L2JButtonType;
import extensions.Admin.Controllers.AdminController;

/**
 * @author Matim
 */
public class AdminWindow extends L2JRootPane
{
	public AdminWindow()
	{
		super("L2JSaver Admin");
		
		addGraphic(AdminHeader.getInstance());
		L2JVerticalPane panel = new L2JVerticalPane(L2JColor.Grey).toPanel(this);
		L2JHorizontalPane hpanel = new L2JHorizontalPane(L2JAlign.Center).toPanel(panel);
		L2JHorizontalPane hpanel2 = new L2JHorizontalPane(L2JAlign.Center).toPanel(panel);
		
		L2JButton reload = new L2JButton("Reload Extensions", L2JButtonType.GRACIA, AdminController.getInstance(), L2JAlign.Center).toPanel(hpanel);
		L2JButton messenger = new L2JButton("Messenger", L2JButtonType.GRACIA, AdminController.getInstance(), L2JAlign.Center).toPanel(hpanel);
		reload.addArgument("reloadWindow");
		messenger.addArgument("messengerWindow");
		
		L2JButton gatekeeper = new L2JButton("Gatekeeper", L2JButtonType.GRACIA, AdminController.getInstance(), L2JAlign.Center).toPanel(hpanel2);
		
		gatekeeper.addArgument("gatekeeper");
	}
}