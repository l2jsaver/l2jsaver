/*
 * Authors: Issle, Howler, David
 * File: EventManagementHeader.java
 */
package extensions.Admin.Handlers;

import java.util.StringTokenizer;
import java.util.logging.Logger;

import org.l2jmobius.gameserver.datatables.SpawnTable;
import org.l2jmobius.gameserver.model.Spawn;
import org.l2jmobius.gameserver.model.World;
import org.l2jmobius.gameserver.model.actor.Npc;
import org.l2jmobius.gameserver.model.actor.instance.PlayerInstance;

import l2jsaver.Features.L2JFrame.L2JFrame;
import l2jsaver.Machine.utils.Utils;
import l2jsaver.interfaces.ISaverCommandHandler;
import extensions.Admin.Windows.AdminMessenger;
import extensions.Admin.Windows.AdminWindow;

/**
 * @author Matim
 */
public class AdminHandler implements ISaverCommandHandler
{
	private static final Logger LOGGER = Logger.getLogger(AdminHandler.class.getName());
	
	private static final String[] VOICED_COMMANDS =
	{
		"admin", "test", "msg", "delete", "dist"
	};
	
	@Override
	public boolean useVoicedCommand(String command, PlayerInstance activeChar, String params)
	{
		if (command.equals("admin"))
		{
			AdminWindow aw = new AdminWindow();
			LOGGER.info(aw.draw());
			if (activeChar.isGM())
				L2JFrame.getInstance().send(activeChar, aw);
		}
		else if (command.equals("msg"))
		{
			if (activeChar.isGM())
				L2JFrame.getInstance().send(activeChar, new AdminMessenger());
		}
		else if (command.equals("delete"))
		{
			if (activeChar.isGM())
				return handleDelete(activeChar, params);
		}
		else if (command.equals("dist"))
		{
			if (params == null || params.isEmpty() || params.isBlank())
			{
				activeChar.sendMessage("Usage: ?dist <Player Name>");
				return false;
			}
			
			StringTokenizer st = new StringTokenizer(params);
			if (!st.hasMoreTokens())
			{
				activeChar.sendMessage("Usage: ?dist <Player Name>");
				return false;
			}
			PlayerInstance player = World.getInstance().getPlayer(st.nextToken());
			if (player == null)
			{
				activeChar.sendMessage("This character name is not exist.");
				return false;
			}
			int dist = Utils.manhattanDistance2D(activeChar, player);
			activeChar.sendMessage("Your Distance is: " + dist);
		}
		return true;
	}
	
	private boolean handleDelete(PlayerInstance player, String params)
	{
		StringTokenizer st = new StringTokenizer(params);
		
		String val = "";
		
		if (!st.hasMoreTokens())
		{
			player.sendMessage("Usage: ?delete <NPC Id>");
			return false;
		}
		val = st.nextToken();
		int id = Integer.parseInt(val);
		int count = 0;
		for (Spawn spawn : SpawnTable.getInstance().getSpawns(id))
		{
			if (spawn.getId() == id)
			{
				Npc npc = spawn.getLastSpawn();
				npc.deleteMe();
				spawn.stopRespawn();
				
				SpawnTable.getInstance().deleteSpawn(spawn, true);
				count++;
			}
		}
		player.sendMessage(count + " npc deleted.");
		return false;
	}
	
	public String[] getVoicedCommandList()
	{
		return VOICED_COMMANDS;
	}
	
	public boolean forGM() 
	{
		return true;
	}
}