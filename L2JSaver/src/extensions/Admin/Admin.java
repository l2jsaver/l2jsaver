/*
 * Authors: Issle, Howler, David
 * File: Admin.java
 */
package extensions.Admin;

import l2jsaver.SaverCommandHandler;
import l2jsaver.Features.L2JFrame.L2JFrame;
import extensions.Admin.Controllers.AdminController;
import extensions.Admin.Handlers.AdminHandler;

/**
 * @author Matim
 */
public class Admin 
{
	public static void main(String[] args) 
	{

		L2JFrame.getInstance().registerListener(AdminController.getInstance());
		SaverCommandHandler.getInstance().registerHandler(new AdminHandler());
	}
}