/*
 * Authors: Issle, Howler, David
 * File: Controllers.java
 */
package extensions.Admin.Controllers;

import l2jsaver.Features.L2JFrame.AbstractController;
import l2jsaver.Features.L2JFrame.L2JFrame;

import java.util.Map;

import org.l2jmobius.gameserver.enums.ChatType;
import org.l2jmobius.gameserver.model.World;
import org.l2jmobius.gameserver.model.actor.instance.PlayerInstance;
import org.l2jmobius.gameserver.network.serverpackets.CreatureSay;
import org.l2jmobius.gameserver.network.serverpackets.ExShowScreenMessage;

import extensions.Admin.Windows.AdminMessenger;

/**
 * @author Matim
 */
public class AdminMessengerController extends AbstractController
{
	private Map<Integer, String> a;
	
	@Override
	public void execute(PlayerInstance activeChar, Map<Integer, String> arguments)
	{
		String type = arguments.get(0);
		ChatType msgType = ChatType.GENERAL;
		
		a = arguments;
				
		if (type.equals("BIGWHITE"))
		{
			for (PlayerInstance p: World.getInstance().getPlayers())
			{
				p.sendPacket(new ExShowScreenMessage(getFullText(), 3000));
			}
		}
		else
		{
			if (type.equals("PARTY"))
				msgType = ChatType.PARTY;
			else if (type.equals("ANNOUNCE"))
				msgType = ChatType.ANNOUNCEMENT;
			else if (type.equals("COMMANDER"))
				msgType = ChatType.PARTYROOM_COMMANDER;
			else if (type.equals("CRITICAL"))
				msgType = ChatType.CRITICAL_ANNOUNCE;
			else if (type.equals("SCREEN"))
				msgType = ChatType.SCREEN_ANNOUNCE;
			else if (type.equals("SHOUT"))
				msgType = ChatType.SHOUT;
			else if (type.equals("BATTLEFIELD"))
				msgType = ChatType.BATTLEFIELD;
			else if (type.equals("CLAN"))
				msgType = ChatType.CLAN;
			else if (type.equals("ALLIANCE"))
				msgType = ChatType.ALLIANCE;
			else if (type.equals("HERO"))
				msgType = ChatType.HERO_VOICE;
			
			sendMessageToAll(getFullText(), msgType);
		}
		L2JFrame.getInstance().send(activeChar, new AdminMessenger());
	}

	@Override
	public void execute(PlayerInstance activeChar)
	{
		
	}
	
	/**
	 * @param msg
	 * @param type
	 * <br><br>
	 * Send Message to each online player.
	 */
	private void sendMessageToAll(String msg, ChatType type)
	{
		for (PlayerInstance p: World.getInstance().getPlayers())
		{
			p.sendPacket(new CreatureSay(0, type, "Server", msg));
		}
	}
	
	private AdminMessengerController() 
	{
		L2JFrame.getInstance().registerListener(this);
	}
	
	private String getFullText()
	{
		String s = "";
		
		for (String t: a.values())
		{
			String firstArg = a.get(0);
			
			if (t.equals(firstArg))
				continue;
			
			s = s + " ";
			s += t;
		}
		
		return s;
	}
	
	private volatile static AdminMessengerController singleton;
	
	public static AdminMessengerController getInstance()
	{
		if (singleton == null) 
		{
			synchronized (AdminMessengerController.class) 
			{
				if (singleton == null)
					singleton = new AdminMessengerController();
			}
		}
		return singleton;
	}
}