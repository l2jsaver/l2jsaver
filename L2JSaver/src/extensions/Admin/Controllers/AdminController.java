/*
 * Authors: Issle, Howler, David
 * File: Controllers.java
 */
package extensions.Admin.Controllers;

import java.io.File;
import java.io.FileFilter;
import java.util.Map;
import java.util.logging.Logger;

import org.l2jmobius.gameserver.model.actor.instance.PlayerInstance;

import l2jsaver.Features.L2JFrame.AbstractController;
import l2jsaver.Features.L2JFrame.L2JFrame;
import l2jsaver.Machine.utils.ScriptLoader;
import extensions.Admin.Windows.AdminMessenger;
import extensions.Admin.Windows.AdminMessengerHelp;
import extensions.Admin.Windows.AdminReload;
import extensions.Admin.Windows.AdminWindow;

/**
 * @author Matim
 */
public class AdminController extends AbstractController
{
	private static final Logger LOGGER = Logger.getLogger(AdminController.class.getName());
	@Override
	public void execute(PlayerInstance activeChar, Map<Integer, String> arguments)
	{
		for (String cmd : arguments.values())
			LOGGER.info(cmd);
		
		String command = arguments.get(0);
		
		if (command.equals("mainWindow"))
			L2JFrame.getInstance().send(activeChar, new AdminWindow());	
		else if (command.equals("reloadWindow"))
			L2JFrame.getInstance().send(activeChar, new AdminReload());	
		else if (command.equals("messengerWindow"))
			L2JFrame.getInstance().send(activeChar, new AdminMessenger());
		else if (command.equals("messengerHelpWindow"))
			L2JFrame.getInstance().send(activeChar, new AdminMessengerHelp());
	}

	
	@Override
	public void execute(PlayerInstance activeChar)
	{
		
	}
	
	private AdminController() 
	{
		super.setAdmin();
	}
	
	public File[] getDirectory()
	{
		File dir = new File(ScriptLoader.SCRIPT_FOLDER.toString());
		File[] result;
		FileFilter filter = new FileFilter()
		{
			public boolean accept(File dir)
			{
				return dir.isDirectory();
			}
		};
		result = dir.listFiles(filter);
		
		return result;
	}
	
	private volatile static AdminController singleton;
	
	public static AdminController getInstance()
	{
		if (singleton == null) 
		{
			synchronized (AdminController.class) 
			{
				if (singleton == null)
					singleton = new AdminController();
			}
		}
		return singleton;
	}
}