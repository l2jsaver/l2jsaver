/*
 * Authors: Issle, Howler, Matim
 * File: BufferDataController.java
 */
package extensions.Buffer.Controllers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.l2jmobius.commons.database.DatabaseFactory;
import org.l2jmobius.gameserver.model.actor.instance.PlayerInstance;
import org.l2jmobius.gameserver.model.holders.SkillHolder;

import extensions.Buffer.Data.BufferXmlData;
import extensions.Buffer.Models.Scheme;
import l2jsaver.Machine.Interfaces.IActor;
import l2jsaver.Machine.Interfaces.IExtension;

/**
 * @author Spunky
 *
 */
public class BufferDataController implements IExtension,IActor
{
	private static final Logger LOGGER = Logger.getLogger(BufferDataController.class.getName());
	
	private String MIGRATE_BUFFER = "CREATE TABLE IF NOT EXISTS `BufferSchemes` (" +
			  "`schemeId` BIGINT(20) NOT NULL AUTO_INCREMENT," +
			  "`ownerId` INT NOT NULL," +
			  "`schemeName` VARCHAR(35) NULL," +
			  "`buffs` MEDIUMTEXT NOT NULL," +
			  "PRIMARY KEY (`schemeId`, `ownerId`));";
	
	private String RESTORE_BUFFER = "SELECT schemeId, ownerId, schemeName, buffs FROM BufferSchemes";
	private String UPDATE_BUFFER = "UPDATE BufferSchemes SET schemeName=?, buffs=? WHERE ownerId=? AND schemeId=?";
	private String DELETE_SCHEME  = "DELETE FROM BufferSchemes WHERE ownerId=? AND schemeId=?";
	private String INSERT_SCHEME = "INSERT INTO BufferSchemes(ownerId, schemename, buffs) VALUES (?,?,?)";

	private Map<String, List<SkillHolder>> _buffSkills;
	private Map<Integer, Map<Integer, Scheme>> _schemes;
	
	public BufferDataController()
	{
		_buffSkills = new HashMap<>();
		_schemes = new HashMap<>();
		createDatabase();
		restoreDatabase();
		BufferXmlData.getInstance();
	}
	
	private void createDatabase()
	{
		LOGGER.info("Checking Database...");
		Connection con = null;
		try
		{
			con = DatabaseFactory.getConnection();
			PreparedStatement statement = con.prepareStatement(MIGRATE_BUFFER);
			statement.execute();
			
			statement.close();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			try { con.close(); } catch (Exception e) {}
		}
	}
	
	public void restoreDatabase()
	{
		Connection con = null;
		try
		{
			con = DatabaseFactory.getConnection();
			PreparedStatement restore = con.prepareStatement(RESTORE_BUFFER);
			
			ResultSet rset = restore.executeQuery();
			while(rset.next())
			{
				int ownerId = rset.getInt("ownerId");
				int schemeId = rset.getInt("schemeId");
				String schemeName = rset.getString("schemeName");
				Map<Integer, Scheme> schemes;
				
				if (getSchemeByPlayerId(ownerId) != null)
				{
					schemes = getSchemeByPlayerId(ownerId);
				}
				else
				{
					schemes = new HashMap<>();
				}
				
				String cacheBuffs = rset.getString("buffs");
				List<SkillHolder> _buffs = new ArrayList<>();
				
				for (String cachedBuff : cacheBuffs.split(";"))
				{
					String[] splittedBuff = cachedBuff.split(",");
					SkillHolder buff = new SkillHolder(Integer.parseInt(splittedBuff[0]), Integer.parseInt(splittedBuff[1]));
					_buffs.add(buff);
				}
				
				Scheme scheme = new Scheme(ownerId, schemeId, schemeName, _buffs);
				schemes.put(scheme.getSchemeId(), scheme);
				
				getSchemes().put(ownerId, schemes);
			}
			
			rset.close();
			restore.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try { con.close(); } catch (Exception e) {}
		}
	}
	
	public void insertScheme(Scheme scheme)
	{
		Connection con = null;
		try
		{
			con = DatabaseFactory.getConnection();
			PreparedStatement statement = con.prepareStatement(INSERT_SCHEME);
			
			statement.setInt(1, scheme.getOwnerId());
			statement.setString(2, scheme.getSchemeName());
			
			String buffList = "";
			
			for (SkillHolder _skill : scheme.getBuffs())
			{
				String buff = _skill.getSkillId() + "," + _skill.getSkillLevel() + ";";
				buffList += buff;
			}
			
			statement.setString(3, buffList);
			
			
			statement.executeUpdate();
			statement.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try { con.close(); } catch (Exception e) {}
		}
	}
	
	public void deleteScheme(Scheme scheme)
	{
		if (getSchemeByPlayerId(scheme.getOwnerId()).get(scheme.getSchemeId()) == null)
		{
			LOGGER.warning("Tried to delete a scheme that doesn't exist");
			return;
		}
		
		getSchemeByPlayerId(scheme.getOwnerId()).remove(scheme.getSchemeId());
		
		Connection con = null;
		try
		{
			con = DatabaseFactory.getConnection();
			PreparedStatement remove = con.prepareStatement(DELETE_SCHEME);
			remove.setInt(1, scheme.getOwnerId());
			remove.setInt(2, scheme.getSchemeId());
			
			remove.execute();
			remove.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try { con.close(); } catch (Exception e) {}
		}
	}
	
	public void updateIndividualScheme(Scheme scheme)
	{
		Connection con = null;
		try
		{
			con = DatabaseFactory.getConnection();
			PreparedStatement save = con.prepareStatement(UPDATE_BUFFER);
	
			save.setString(1, scheme.getSchemeName());
					
			String buffList = "";
			
			for (SkillHolder _skill : scheme.getBuffs())
			{
				String buff = _skill.getSkillId() + "," + _skill.getSkillLevel() + ";";
				buffList += buff;
			}
					
			save.setString(2, buffList);
			save.setInt(3, scheme.getOwnerId());
			save.setInt(4, scheme.getSchemeId());
					
			save.execute();
			
			save.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try { con.close(); } catch (Exception e) {}
		}
	}
	
	public void saveBuffer()
	{
		Connection con = null;
		try
		{
			con = DatabaseFactory.getConnection();
			PreparedStatement save = con.prepareStatement(UPDATE_BUFFER);
			
			for (Map<Integer, Scheme> schemes : getSchemes().values())
			{
				for (Scheme scheme : schemes.values())
				{
					save.setString(1, scheme.getSchemeName());
					
					String buffList = "";
					
					for (SkillHolder _skill : scheme.getBuffs())
					{
						String buff = _skill.getSkillId() + "," + _skill.getSkillLevel() + ";";
						buffList += buff;
					}
					
					save.setString(2, buffList);
					save.setInt(3, scheme.getOwnerId());
					save.setInt(4, scheme.getSchemeId());
					
					save.execute();
				}
			}
			
			save.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try { con.close(); } catch (Exception e) {}
		}
	}
	
	public Map<String, List<SkillHolder>> getBuffSkills()
	{
		if (_buffSkills == null)
			return _buffSkills = new HashMap<>();
		
		return _buffSkills;
	}
	
	public Map<Integer, Map<Integer, Scheme>> getSchemes()
	{
		return _schemes;
	}
	
	public List<SkillHolder> getBuffSkillsByType(String buffType)
	{
		return _buffSkills.get(buffType);
	}
	
	public Map<Integer, Scheme> getSchemeByPlayerId(int objectId)
	{
		return _schemes.get(objectId);
	}

	@Override
	public void onReload() 
	{
		
	}

	@Override
	public void onShutDown() 
	{
		
	}

	@Override
	public void onLogin(PlayerInstance player) 
	{
		
	}
	
	@Override
	public void onLogout(PlayerInstance player) 
	{
		
	}

	@Override
	public void onItemUse(PlayerInstance player) {}

	@Override
	public void onMove(PlayerInstance player, int orX, int orY, int orZ, int tarX, int tarY, int tarZ) {}

	@Override
	public void onDie(PlayerInstance killed, PlayerInstance killer) {}
	
	public static BufferDataController getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	private static class SingletonHolder
	{
		protected static final BufferDataController INSTANCE = new BufferDataController();
	}

}
