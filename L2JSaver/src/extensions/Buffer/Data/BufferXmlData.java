/*
 * Authors: Issle, Howler, Matim
 * File: BufferXmlData.java
 */
package extensions.Buffer.Data;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.l2jmobius.commons.util.IXmlReader;
import org.l2jmobius.gameserver.model.StatsSet;
import org.l2jmobius.gameserver.model.holders.SkillHolder;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import extensions.Buffer.Controllers.BufferDataController;

/**
 * @author Spunky
 *
 */
public class BufferXmlData implements IXmlReader
{
	private byte maxSkills;
	
	protected BufferXmlData()
	{
		load();
	}
	
	@Override
	public void load()
	{
		BufferDataController.getInstance().getBuffSkills().clear();
		parseDatapackFile("data/BufferData.xml");
		LOGGER.info(getClass().getSimpleName() + ": Loaded " + maxSkills + " Skills.");
	}
	
	@Override
	public void parseDocument(Document doc, File f)
	{
		NamedNodeMap attrs;
		Node att;
		StatsSet set = null;
		for (Node a = doc.getFirstChild(); a != null; a = a.getNextSibling())
		{
			if ("list".equalsIgnoreCase(a.getNodeName()))
			{
				for (Node b = a.getFirstChild(); b != null; b = b.getNextSibling())
				{
					if ("buff".equalsIgnoreCase(b.getNodeName()))
					{
						attrs = b.getAttributes();
						set = new StatsSet();
						for (int i = 0; i < attrs.getLength(); i++)
						{
							att = attrs.item(i);
							set.set(att.getNodeName(), att.getNodeValue());
						}
						
						String skillType = set.getString("cat");
						
						List<SkillHolder> _buffs = BufferDataController.getInstance().getBuffSkillsByType(skillType);
						
						if (_buffs == null)
						{
							_buffs = new ArrayList<>();
							BufferDataController.getInstance().getBuffSkills().put(skillType, _buffs);
						}
						
						_buffs.add(new SkillHolder(set.getInt("skillId"), set.getInt("skillLvl")));
						maxSkills++;
					}
				}
			}
		}
	}
	
	public static BufferXmlData getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	private static class SingletonHolder
	{
		protected static final BufferXmlData INSTANCE = new BufferXmlData();
	}
}
