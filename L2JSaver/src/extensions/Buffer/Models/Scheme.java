/*
 * Authors: Issle, Howler, Matim
 * File: Scheme.java
 */
package extensions.Buffer.Models;

import java.util.List;

import org.l2jmobius.gameserver.model.holders.SkillHolder;

/**
 * @author Spunky
 *
 */
public class Scheme 
{
	private int _ownerId;
	private String _schemeName;
	private int _schemeId;
	private List<SkillHolder> _buffs;
	
	public Scheme(int ownerId, int schemeId, String schemeName, List<SkillHolder> buffs)
	{
		_ownerId = ownerId;
		_schemeName = schemeName;
		_schemeId = schemeId;
		_buffs = buffs;
	}
	
	public int getOwnerId()
	{
		return _ownerId;
	}
	
	public List<SkillHolder> getBuffs()
	{
		return _buffs;
	}
	
	public int getSchemeId()
	{
		return _schemeId;
	}
	
	public String getSchemeName()
	{
		return _schemeName;
	}
}
