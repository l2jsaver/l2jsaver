/*
Navicat MySQL Data Transfer

Source Server         : Casa
Source Server Version : 50512
Source Host           : localhost:3306
Source Database       : test_l2jdb

Target Server Type    : MYSQL
Target Server Version : 50512
File Encoding         : 65001

Date: 2011-05-18 13:44:40
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `custom_notspawned`
-- ----------------------------
DROP TABLE IF EXISTS `custom_notspawned`;
CREATE TABLE `custom_notspawned` (
  `locx` mediumint(6) NOT NULL DEFAULT '0',
  `locy` mediumint(6) NOT NULL DEFAULT '0',
  `locz` mediumint(6) NOT NULL DEFAULT '0',
  `heading` mediumint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`locx`,`locy`,`locz`,`heading`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of custom_notspawned
-- ----------------------------
