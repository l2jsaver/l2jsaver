
SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `custom_notdropped`
-- ----------------------------
DROP TABLE IF EXISTS `custom_notdropped`;
CREATE TABLE `custom_notdropped` (
  `mobId` mediumint(5) unsigned NOT NULL DEFAULT '0',
  `itemId` smallint(5) unsigned NOT NULL DEFAULT '0',
  `category` smallint(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`mobId`,`itemId`,`category`),
  KEY `key_mobId` (`mobId`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

