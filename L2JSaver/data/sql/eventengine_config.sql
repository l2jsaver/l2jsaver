/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50512
Source Host           : localhost:3306
Source Database       : l2jdb

Target Server Type    : MYSQL
Target Server Version : 50512
File Encoding         : 65001

Date: 2011-05-29 15:23:21
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `eventengine_config`
-- ----------------------------
DROP TABLE IF EXISTS `eventengine_config`;
CREATE TABLE `eventengine_config` (
  `configName` varchar(40) NOT NULL DEFAULT '',
  `configValue` varchar(40) DEFAULT 'None',
  PRIMARY KEY (`configName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of eventengine_config
-- ----------------------------
INSERT INTO `eventengine_config` VALUES ('enableScheduler', 'false');
INSERT INTO `eventengine_config` VALUES ('schedulerTime', '60');
INSERT INTO `eventengine_config` VALUES ('TvTGameTime', '900');
INSERT INTO `eventengine_config` VALUES ('TvTMaxPlayers', '50');
INSERT INTO `eventengine_config` VALUES ('TvTMinPlayers', '2');
INSERT INTO `eventengine_config` VALUES ('TvTRegisterTime', '600');
INSERT INTO `eventengine_config` VALUES ('TvTReviveTime', '15');
INSERT INTO `eventengine_config` VALUES ('TvTWaitTime', '10');
