###############makes skill autolearn################################
UPDATE skill_trees set is_autoget='true';
UPDATE skill_trees set learned_by_fs='false';
UPDATE skill_trees set learned_by_npc='true';
################################################################################################

###############delete adamantine drop from all mobs################################
delete from droplist where itemId=9629;
################################################################################################

###############Delete all epic bosses drops and re-add jewels: also make them lvl 85############
DELETE FROM droplist where 
mobId='29019' or
mobId='29066' or
mobId='29067' or
mobId='29068' or
mobId='29011' or
mobId='29028' or
mobId='29020' or
mobId='29022' or
mobId='29047';
UPDATE npc SET level=85 where type='L2GrandBoss';
REPLACE INTO custom_droplist(mobId,itemId,min,max,category,chance) VALUES
(29019, 6656, 1, 1, 50, 1000000), #antharas old
(29066, 6656, 1, 1, 50, 1000000), #antharas weak
(29067, 6656, 1, 1, 50, 1000000), #antharas normal
(29068, 6656, 1, 1, 50, 1000000), #antharas strong
(29001, 6660, 1, 1, 50, 1000000), #Queen Ant
(29028, 6657, 1, 1, 50, 1000000), #Valakas
(29020, 6658, 1, 1, 50, 1000000), #baium
(29022, 6659, 1, 1, 50, 1000000), #zaken
(29047, 8191, 1, 1, 50, 1000000); #frintezza
################################################################################################

################delete all drops from raidbosses################################################
DELETE from droplist where droplist.mobId in 
(select npc.id from npc where type='L2RaidBoss');
################################################################################################

################deleting mobs in custom zones################################
DELETE from spawnlist WHERE 
#delete in easy zone
npc_templateid = 20673 OR #falibati
npc_templateid = 20672 OR #trives
npc_templateid = 20674 OR #doom knight
#delete in normal zone
npc_templateid = 18001 OR #bloody queen
npc_templateid = 20670 OR #crimson drake
npc_templateid = 20671 OR #kadios
#delete in medium zone
npc_templateid = 20668 OR #grave guard
npc_templateid = 20669 OR #taic orc supply leader
npc_templateid = 18276 OR #treasure chest
npc_templateid = 21000 OR #soul of ruins
npc_templateid = 20678 OR #tortured undead
npc_templateid = 20999 OR #roving soul
#hard zone
npc_templateid = 20998 OR #cruel punisher
npc_templateid = 20997 OR #soldier of grief
npc_templateid = 20666 OR #taic orc seeker
npc_templateid = 18275 OR #treasure chest
npc_templateid = 27181 OR #imperial gatekeeper
#talking island
npc_templateid = 31032 OR #guard
npc_templateid = 20481 OR #bearded keltir
npc_templateid = 20544 OR #elder keltir
npc_templateid = 20432 OR #elpy
npc_templateid = 20120 OR #wolf
npc_templateid = 20545 OR #young keltir
npc_templateid = 20326 OR #goblin scout
npc_templateid = 20131 OR #orc grunt
npc_templateid = 20006 OR #orc archer
npc_templateid = 20093 OR #orc fighter
npc_templateid = 20132 OR #werewolf
npc_templateid = 20442 OR #elder wolf
npc_templateid = 20130 OR #orc
npc_templateid = 20096 OR #orc lieutenant
npc_templateid = 20098 OR #orc fighter leader
npc_templateid = 30733 OR #guard
npc_templateid = 20343 OR #werewolf hunter
npc_templateid = 20016 OR #stone golem
npc_templateid = 20101 OR #crasher
npc_templateid = 20103 OR #giant spider
npc_templateid = 20106 OR #talon spider
npc_templateid = 20108 OR #blade spider
npc_templateid = 20121 OR #giant toad
npc_templateid = 20342 OR #werewolf chieftain
#leveling zone
npc_templateid = 32293 OR #jirrone (npc)
npc_templateid = 20357 OR #langk lizardman lieutenant
#towns
npc_templateid = 32632; #gracia survivor
################################################################################################

############makes all raids level 85################################
UPDATE npc set `level`='85' where type='L2RaidBoss' or title='Raid Fighter';
UPDATE npc set `level`='85' where type='L2GrandBoss';
################################################################################################

############modify drop of KE from guards in fortresses################################
UPDATE droplist set min=15,max=25,chance=1000000 WHERE
(
mobId= 35674 OR #guard (archer)
mobId= 35682 OR #healer
mobId= 35681 OR #wizard
mobId= 35673 OR #court guard
mobId= 35679 OR #guard (with swords protecting captain)
mobId= 35672 OR #rebel private
mobId= 35684 OR #archer
mobId= 35671;    #decorated rebel soldier
)
AND itemId=9912; 
#mini boss
UPDATE droplist set min=200,max=350,chance=1000000 WHERE
(
mobId= 35677 OR #guard captain
mobId= 35683 OR #archer captain
mobId= 35680 #support unit captain
)
AND itemId=9912;
#boss
UPDATE droplist set min=350,max=500,chance=1000000 WHERE mobId= 35670 AND itemId=9912; #rebel commander
################################################################################################

###########set stats of raidbosses######################################################
UPDATE npc set
hp = (SELECT hp from custom_npc where id=999998),
mp = (SELECT mp from custom_npc where id=999998),
str = (SELECT str from custom_npc where id=999998),
con = (SELECT con from custom_npc where id=999998),
dex = (SELECT dex from custom_npc where id=999998),
int = (SELECT int from custom_npc where id=999998),
wit = (SELECT wit from custom_npc where id=999998),
men = (SELECT men from custom_npc where id=999998),
patk = (SELECT patk from custom_npc where id=999998),
pdef = (SELECT pdef from custom_npc where id=999998),
matk = (SELECT matk from custom_npc where id=999998),
mdef = (SELECT mdef from custom_npc where id=999998),
atkspd = (SELECT atkspd from custom_npc where id=999998),
matkspd = (SELECT matkspd from custom_npc where id=999998),
runspd = (SELECT runspd from custom_npc where id=999998),
serverSideTitle=1
WHERE type='L2RaidBoss';
#########################################################################################
