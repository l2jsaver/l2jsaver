# IMPORTS
import sys
from com.l2jserver.gameserver.model import L2ItemInstance
from com.l2jserver.gameserver.model.quest import State
from com.l2jserver.gameserver.model.quest import QuestState
from com.l2jserver.gameserver.model.quest.jython import QuestJython as JQuest
from com.l2jserver.gameserver.model.actor.instance import L2PcInstance
from com.l2jserver.gameserver.model.itemcontainer import Inventory
from com.l2jserver.gameserver.network.serverpackets import InventoryUpdate
from com.l2jserver.gameserver.network.serverpackets import UserInfo
from com.l2jserver.gameserver.network.serverpackets import CharInfo
from com.l2jserver.gameserver.network.serverpackets import SystemMessage 
from com.l2jserver.gameserver.datatables import SkillTable
from l2jsaver.Model import PcModel
from com.l2jserver.gameserver.datatables import ItemTable

print "Loading Quest - 9999917_NPCEnchanter"


# HEADER
QUESTNAME = "9999917_NPCEnchanter"    #Quest Name
QUESTID = 9999917                     #Quest ID
QUESTTITLE = "NPCEnchanter"           #Quest Title
QUESTDESC = "custom"                  #Quest Description
QUESTNPC = [9999917]                  #NPC ID; Enchantus

MINLVL = 40                           #Minimum level
ENCINC = 1                            #Enchant increment
SAFEENC = 6                           #Safe enchant
MAXENC = 13                         #Maximum enchant
team_material = 9629
solo_material = 4048
base_solo_cost =1
base_team_cost =1
base_pvp_cost =0  
# BODY
doll = {
############### ACURR ## AMULT ## AMAXENC ## ASLOT
"head"       : Inventory.PAPERDOLL_HEAD,
"gloves"     : Inventory.PAPERDOLL_GLOVES,
"chest"      : Inventory.PAPERDOLL_CHEST,
"under"      : Inventory.PAPERDOLL_UNDER,
"rhand"      : Inventory.PAPERDOLL_RHAND,
"legs"       : Inventory.PAPERDOLL_LEGS,
"lhand"      : Inventory.PAPERDOLL_LHAND,
"feet"       : Inventory.PAPERDOLL_FEET,
"rear"       : Inventory.PAPERDOLL_REAR,
"neck"       : Inventory.PAPERDOLL_NECK,
"lear"       : Inventory.PAPERDOLL_LEAR,
"rfinger"    : Inventory.PAPERDOLL_RFINGER,
"lfinger"    : Inventory.PAPERDOLL_LFINGER,
}

enchant_table = {
1 : 5,
2 : 10,
3 : 20,
4 : 40,
5 : 80,
6 : 120,
7 : 300,
8 : 500,
9 : 800,
10 : 1000,
11 : 1500,
12 : 2000,
13 : 2500,
}

## RESTRICTED ITEMS
CURSED = [8190, 8689]
NOGRADE = [1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 13, 14, 15, 16, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 41, 42, 43, 44, 48, 49, 50, 51, 66, 67, 68, 87, 99, 100, 102, 112, 113, 114, 115, 116, 118, 122, 152, 153, 154, 155, 176, 177, 215, 216, 218, 219, 256, 257, 271, 272, 273, 308, 310, 311, 390, 390, 412, 412, 426, 428, 429, 462, 463, 464, 625, 845, 846, 875, 876, 877, 878, 906, 907, 908, 909, 1101, 1101, 1102, 1103, 1104, 1104, 1105, 1119, 1121, 1122, 1129, 1148, 1333, 2386, 5284]
HEROITEMS = [6611, 6612, 6613, 6614, 6615, 6616, 6617, 6618, 6619, 6620, 6621, 9388, 9389, 9390]
COMMWEAPONS = [11854, 11814, 12001, 11758, 12000, 11701, 11938, 11903, 11643, 11705, 11723, 11773, 11722, 11704, 11745, 12025, 11617, 11732, 11731, 12171, 11608, 11780, 11781, 11979, 11997, 11635, 11690, 11688, 11719, 11770, 11718, 11687, 11689, 11743, 11750, 11782, 11981, 11933, 11843, 11691, 11765, 11808, 11809, 11810, 11811, 11812, 11678, 11950, 11951, 11749, 11610, 11727, 11944, 11612, 11970, 12063, 12013, 11991, 11821, 11914, 11928, 11911, 11910, 11912, 11913, 11955, 12096, 11763, 11784, 11785, 11786, 11787, 11736, 11804, 11942, 12126, 11672, 11644, 11613, 11735, 11675, 11741, 11795, 11740, 11774, 11674, 11709, 11863, 11764, 11759, 11762, 11706, 11793, 11662, 11978, 11963, 12215, 11629, 11747, 11896, 11964, 11803, 11946, 11935, 11842, 11939, 11940, 11715, 11993, 11855, 11958, 11931, 11959, 11637, 11966, 11630, 12183, 11947, 11994, 11967, 11995, 11968, 11982, 12102, 11840, 11661, 11631, 11777, 11987, 11859, 11850, 12091, 11790, 11952, 11640, 11775, 11733, 11798, 11670, 11791, 11739, 11772, 11974, 11857, 11702, 11668, 11669, 11624, 11677, 11827, 11766, 11969, 11648, 11649, 11650, 11651, 12004, 12143, 11659, 11734, 11737, 11838, 11725, 11693, 11800, 11992, 11724, 11890, 11839, 11889, 11929, 11960, 11623, 11652, 11653, 11654, 11817, 12005, 11713, 11767, 11768, 11626, 11625, 11918, 11919, 11945, 11829, 12051, 11792, 11904, 11818, 12308, 12002, 11954, 11819, 11895, 11941, 11822, 11794, 11888, 11862, 11860, 11923, 11861, 11885, 11884, 11886, 11887, 11707, 11916, 11961, 11956, 12069, 12107, 11628, 11717, 11769, 11716, 11684, 11742, 11776, 11915, 11673, 11962, 11996, 11932, 11728, 11632, 11638, 11641, 11627, 11806, 11685, 11949, 11907, 11639, 11726, 11686, 11748, 11664, 11634, 11977, 11802, 11760, 11858, 11833, 11824, 11813, 11825, 11917, 11647, 12078, 11826, 11646, 11692, 11611, 11609, 11778, 11873, 11841, 11920, 11870, 11869, 11871, 11872, 11607, 11972, 11614, 11849, 11682, 11680, 11695, 11752, 11694, 11679, 11658, 11681, 11738, 11828, 11999, 11853, 11937, 11998, 11730, 11615, 11754, 11830, 11980, 11619, 11751, 12225, 12302, 12268, 11783, 11878, 11848, 11845, 11921, 11846, 11847, 11875, 11874, 11876, 11877, 11844, 12279, 11660, 11983, 11761, 11642, 11986, 11697, 11755, 11985, 11753, 11975, 11973, 11779, 11902, 11666, 11636, 11700, 11721, 11771, 11720, 11698, 11699, 11744, 11789, 11883, 11851, 11922, 11852, 11880, 11879, 11881, 11882, 11953, 11908, 12073, 11934, 11729, 11663, 11633, 11936, 11605, 11799, 11757, 11788, 11756, 11868, 11837, 11834, 11901, 11835, 11836, 11831, 11865, 11864, 11866, 11867, 11606, 11683, 11930, 11807, 11925, 11897, 11898, 11899, 11984, 11801, 11924, 11892, 11891, 11893, 11894, 11655, 11696, 11971, 11656, 11815, 11926, 11905, 11906, 11657, 11714, 11797, 11796, 11900, 11805, 11957, 11989, 12003, 11976, 11620, 11621, 11708, 11823, 11616, 11990, 11988, 11711, 11710, 11645, 11676, 12129, 11622, 11665, 11820, 11927, 11909, 11667, 11965, 11816, 11671, 11712, 11948, 11832, 11703, 11943, 11746, 11618, 11856, 12159]
COMMARMOR = [12338, 12339, 12340, 12327, 12328, 12270, 12071, 12168, 12167, 12169, 12173, 12170, 12164, 12163, 12162, 12165, 12172, 12166, 12219, 12319, 12044, 12029, 12196, 12197, 12195, 12204, 12193, 12198, 12200, 12199, 12202, 12194, 12203, 12201, 12009, 12045, 12277, 12276, 12278, 12070, 12089, 12059, 12062, 12060, 12061, 12064, 12028, 12093, 12094, 12092, 12090, 12095, 12128, 12125, 12127, 12049, 12032, 12007, 12118, 12011, 12010, 12212, 12213, 12214, 12217, 12206, 12208, 12207, 12209, 12216, 12210, 12218, 12211, 12017, 12121, 12122, 12124, 12123, 12137, 12139, 12138, 12181, 12182, 12180, 12179, 12177, 12178, 12185, 12184, 12284, 12283, 12282, 12285, 12135, 12136, 12134, 12088, 12109, 12108, 12110, 12039, 12030, 12332, 12341, 12329, 12322, 12066, 12065, 12320, 12321, 12027, 12026, 12314, 12315, 12316, 12144, 12142, 12141, 12145, 12038, 12271, 12272, 12273, 12046, 12116, 12106, 12081, 12280, 12050, 12307, 12310, 12305, 12306, 12309, 12033, 12012, 12098, 12097, 12100, 12099, 12186, 12006, 12016, 12040, 12020, 12021, 12254, 12252, 12253, 12255, 12355, 12248, 12247, 12249, 12250, 12353, 12256, 12354, 12251, 12288, 12289, 12286, 12287, 12053, 12052, 12055, 12054, 12334, 12042, 12043, 12031, 12041, 12119, 12072, 12323, 12056, 12290, 12058, 12057, 12326, 12019, 12018, 12335, 12317, 12336, 12342, 12324, 12312, 12330, 12275, 12274, 12076, 12325, 12349, 12347, 12348, 12077, 12080, 12079, 12105, 12101, 12103, 12104, 12014, 12015, 12311, 12035, 12034, 12037, 12036, 12087, 12113, 12114, 12111, 12112, 12008, 12333, 12337, 12343, 12313, 12331, 12068, 12115, 12083, 12082, 12075, 12074, 12023, 12024, 12263, 12149, 12148, 12189, 12190, 12267, 12224, 12227, 12220, 12221, 12226, 12222, 12228, 12223, 12188, 12187, 12293, 12292, 12291, 12294, 12264, 12269, 12301, 12304, 12299, 12300, 12303, 12265, 12260, 12261, 12352, 12257, 12258, 12350, 12262, 12351, 12259, 12297, 12298, 12295, 12296, 12266, 12345, 12344, 12346, 12231, 12229, 12234, 12230, 12233, 12235, 12232, 12358, 12356, 12357, 12147, 12146, 12120, 12117, 12205, 12048, 12176, 12281, 12140, 12067, 12240, 12242, 12241, 12236, 12237, 12238, 12245, 12239, 12244, 12246, 12243, 12361, 12359, 12360, 12085, 12084, 12086, 12133, 12132, 12130, 12131, 12174, 12191, 12318, 12192, 12047, 12175, 12022, 12158, 12156, 12157, 12161, 12152, 12154, 12155, 12153, 12160, 12150, 12151]

class Quest (JQuest):

    def __init__(self, id, name, descr): 
        JQuest.__init__(self, id, name, descr)

    def onFirstTalk(self, npc, player):
        st = player.getQuestState(QUESTNAME)
        if not st:
            st = self.newQuestState(player)
        return self.onAdvEvent("showindex", npc, player)

    def onAdvEvent(self, event, npc, player):
        st = player.getQuestState(QUESTNAME)
        if not st:
            return
        st.playSound("ItemSound.quest_accept")
        st.getPlayer().setTarget(st.getPlayer())

        ## SHOW INDEX PAGE
        if event == "showindex":
            TEXTHEAD = "<button action=\"bypass -h Quest 9999917_NPCEnchanter head\" width=32 height=32 back=icon.armor_helmet_i00 fore=icon.armor_helmet_i00>"
            TEXTGLOVES = "<button action=\"bypass -h Quest 9999917_NPCEnchanter gloves\" width=32 height=32 back=icon.armor_t94_g_i01 fore=icon.armor_t94_g_i01>"
            TEXTCHEST = "<button action=\"bypass -h Quest 9999917_NPCEnchanter chest\" width=32 height=32 back=icon.armor_t94_u_i01 fore=icon.armor_t94_u_i01>"
            TEXTUNDER = "<button action=\"bypass -h Quest 9999917_NPCEnchanter under\" width=32 height=32 back=icon.etc_stripe_shirts_s_i05 fore=icon.etc_stripe_shirts_s_i05>"
            TEXTRHAND = "<button action=\"bypass -h Quest 9999917_NPCEnchanter rhand\" width=32 height=32 back=icon.weapon_vesper_slasher_i00 fore=icon.weapon_vesper_slasher_i00>"
            TEXTLEGS = "<button action=\"bypass -h Quest 9999917_NPCEnchanter legs\" width=32 height=32 back=icon.armor_t94_l_i01 fore=icon.armor_t94_l_i01>"
            TEXTLHAND = "<button action=\"bypass -h Quest 9999917_NPCEnchanter lhand\" width=32 height=32 back=icon.weapon_vesper_verteidiger_i01 fore=icon.weapon_vesper_verteidiger_i01>"
            TEXTRFINGER = "<button action=\"bypass -h Quest 9999917_NPCEnchanter rfinger\" width=32 height=32 back=icon.accessory_tateossian_ring_i00 fore=icon.accessory_tateossian_ring_i00>"
            TEXTFEET = "<button action=\"bypass -h Quest 9999917_NPCEnchanter feet\" width=32 height=32 back=icon.armor_t94_b_i01 fore=icon.armor_t94_b_i01>"
            TEXTLFINGER = "<button action=\"bypass -h Quest 9999917_NPCEnchanter lfinger\" width=32 height=32 back=icon.accessory_tateossian_ring_i00 fore=icon.accessory_tateossian_ring_i00>"
            TEXTREAR = "<button action=\"bypass -h Quest 9999917_NPCEnchanter rear\" width=32 height=32 back=icon.accessory_tateossian_earring_i00 fore=icon.accessory_tateossian_earring_i00>"
            TEXTNECK = "<button action=\"bypass -h Quest 9999917_NPCEnchanter neck\" width=32 height=32 back=icon.accessory_tateossian_necklace_i00 fore=icon.accessory_tateossian_necklace_i00>"
            TEXTLEAR = "<button action=\"bypass -h Quest 9999917_NPCEnchanter lear\" width=32 height=32 back=icon.accessory_tateossian_earring_i00 fore=icon.accessory_tateossian_earring_i00>"

            if st.getPlayer().getInventory().getPaperdollItemId(Inventory.PAPERDOLL_HEAD) == 0:
                TEXTHEAD = "<button width=32 height=32 back=icon.etc_alphabet_h_i00 fore=icon.etc_alphabet_h_i00>"
            if st.getPlayer().getInventory().getPaperdollItemId(Inventory.PAPERDOLL_GLOVES) == 0:
                TEXTGLOVES = "<button width=32 height=32 back=icon.etc_alphabet_h_i00 fore=icon.etc_alphabet_h_i00>"
            if st.getPlayer().getInventory().getPaperdollItemId(Inventory.PAPERDOLL_CHEST) == 0:
                TEXTCHEST = "<button width=32 height=32 back=icon.etc_alphabet_h_i00 fore=icon.etc_alphabet_h_i00>"
            if st.getPlayer().getInventory().getPaperdollItemId(Inventory.PAPERDOLL_UNDER) == 0:
                TEXTUNDER = "<button width=32 height=32 back=icon.etc_alphabet_h_i00 fore=icon.etc_alphabet_h_i00>"
            if st.getPlayer().getInventory().getPaperdollItemId(Inventory.PAPERDOLL_RHAND) == 0:
                TEXTRHAND = "<button width=32 height=32 back=icon.etc_alphabet_h_i00 fore=icon.etc_alphabet_h_i00>"
            if st.getPlayer().getInventory().getPaperdollItemId(Inventory.PAPERDOLL_LEGS) == 0:
                TEXTLEGS = "<button width=32 height=32 back=icon.etc_alphabet_h_i00 fore=icon.etc_alphabet_h_i00>"
            if st.getPlayer().getInventory().getPaperdollItemId(Inventory.PAPERDOLL_LHAND) == 0:
                TEXTLHAND = "<button width=32 height=32 back=icon.etc_alphabet_h_i00 fore=icon.etc_alphabet_h_i00>"
            if st.getPlayer().getInventory().getPaperdollItemId(Inventory.PAPERDOLL_RFINGER) == 0:
                TEXTRFINGER = "<button width=32 height=32 back=icon.etc_alphabet_h_i00 fore=icon.etc_alphabet_h_i00>"
            if st.getPlayer().getInventory().getPaperdollItemId(Inventory.PAPERDOLL_FEET) == 0:
                TEXTFEET = "<button width=32 height=32 back=icon.etc_alphabet_h_i00 fore=icon.etc_alphabet_h_i00>"
            if st.getPlayer().getInventory().getPaperdollItemId(Inventory.PAPERDOLL_LFINGER) == 0:
                TEXTLFINGER = "<button width=32 height=32 back=icon.etc_alphabet_h_i00 fore=icon.etc_alphabet_h_i00>"
            if st.getPlayer().getInventory().getPaperdollItemId(Inventory.PAPERDOLL_REAR) == 0:
                TEXTREAR = "<button width=32 height=32 back=icon.etc_alphabet_h_i00 fore=icon.etc_alphabet_h_i00>"
            if st.getPlayer().getInventory().getPaperdollItemId(Inventory.PAPERDOLL_NECK) == 0:
                TEXTNECK = "<button width=32 height=32 back=icon.etc_alphabet_h_i00 fore=icon.etc_alphabet_h_i00>"
            if st.getPlayer().getInventory().getPaperdollItemId(Inventory.PAPERDOLL_LEAR) == 0:
                TEXTLEAR = "<button width=32 height=32 back=icon.etc_alphabet_h_i00 fore=icon.etc_alphabet_h_i00>"

            htmltext = st.showHtmlFile("index.htm").replace("%TEXTHEAD%", TEXTHEAD).\
                                                    replace("%TEXTGLOVES%", TEXTGLOVES).\
                                                    replace("%TEXTCHEST%", TEXTCHEST).\
                                                    replace("%TEXTUNDER%", TEXTUNDER).\
                                                    replace("%TEXTRHAND%", TEXTRHAND).\
                                                    replace("%TEXTLEGS%", TEXTLEGS).\
                                                    replace("%TEXTLHAND%", TEXTLHAND).\
                                                    replace("%TEXTRFINGER%", TEXTRFINGER).\
                                                    replace("%TEXTFEET%", TEXTFEET).\
                                                    replace("%TEXTLFINGER%", TEXTLFINGER).\
                                                    replace("%TEXTREAR%", TEXTREAR).\
                                                    replace("%TEXTNECK%", TEXTNECK).\
                                                    replace("%TEXTLEAR%", TEXTLEAR)
            return htmltext

        ## PREPARE BASIC VARIABLES
        if event in doll.keys():
            global  ASLOT, ITEM, EVENT
            ASLOT = doll[event]
            ITEM = st.getPlayer().getInventory().getPaperdollItemId(ASLOT)
            EVENT = event
            return self.onAdvEvent("checkitem", npc, player)

        ## CHECK ITEM
        if event == "checkitem":
            if ITEM == 0:
                htmltext = st.showHtmlFile("security.htm").replace("%TEXTACC%", st.getPlayer().getAccountName()).\
                                                           replace("%TEXTCHAR%", st.getPlayer().getName())
                return htmltext
            elif ITEM in CURSED + NOGRADE + HEROITEMS + COMMWEAPONS + COMMARMOR:
                htmltext = "erroritem.htm"
                return htmltext
            else:
                return self.onAdvEvent("prepvars", npc, player)

        ## PREPARE ALL OTHER VARIABLES
        if event == "prepvars":
            global CURENC, ENCHANT, SOLOFUNDS,TEAMFUNDS,PVPFUNDS, SOLOCOST,TEAMCOST,PVPCOST
            CURENC = st.getPlayer().getInventory().getPaperdollItem(ASLOT).getEnchantLevel()
            ENCHANT = CURENC + ENCINC
            SOLOFUNDS = st.getQuestItemsCount(solo_material)
            TEAMFUNDS = st.getQuestItemsCount(team_material)
            PVPFUNDS = PcModel.getModel(player).spartanInstance.getSoloPoints()
            if ENCHANT >= MAXENC:
                SOLOCOST = base_solo_cost * enchant_table[MAXENC-1]
                TEAMCOST = base_team_cost * enchant_table[MAXENC-1]
                PVPCOST = base_pvp_cost * enchant_table[MAXENC-1]
            else:
                SOLOCOST = base_solo_cost * enchant_table[ENCHANT]
                TEAMCOST = base_team_cost * enchant_table[ENCHANT]
                PVPCOST = base_pvp_cost * enchant_table[ENCHANT]
            return self.onAdvEvent("showenc", npc, player)

        ## SHOW ENCHANT PAGE
        if event == "showenc":
            TEXTCURENC = "<font color=009900>" + str(CURENC) + "</font>"
            TEXTMAXENC = "<font color=ffff00>" + str(MAXENC) + "</font>"
            
            TEXTSOLOFUNDS = "<font color=009900>" + str(SOLOFUNDS) + "</font>"
            TEXTSOLOCOST = "<font color=ffff00>" + str(SOLOCOST) + "</font>"
            TEXTTEAMFUNDS = "<font color=009900>" + str(TEAMFUNDS) + "</font>"
            TEXTTEAMCOST = "<font color=ffff00>" + str(TEAMCOST) + "</font>"
            TEXTPVPFUNDS = "<font color=009900>" + str(PVPFUNDS) + "</font>"
            TEXTPVPCOST = "<font color=ffff00>" + str(PVPCOST) + "</font>"
            
            if CURENC >= MAXENC:
                TEXTCURENC = "<font color=ff0000>" + str(CURENC) + "</font>"

            htmltext = st.showHtmlFile("enchant.htm").replace("%TEXTCURENC%", TEXTCURENC).\
                                                      replace("%TEXTMAXENC%", TEXTMAXENC).\
                                                      replace("%solo_funds%", TEXTSOLOFUNDS).\
                                                      replace("%solo_cost%", TEXTSOLOCOST).\
                                                      replace("%team_funds%", TEXTTEAMFUNDS).\
                                                      replace("%team_cost%", TEXTTEAMCOST).\
                                                      replace("%pvp_funds%", TEXTPVPFUNDS).\
                                                      replace("%pvp_cost%", TEXTPVPCOST).\
                                                      replace("%solomat%", "<img src=" + ItemTable.getInstance().getTemplate(solo_material).getIcon() + " width=32 height=32>").\
                                                      replace("%partymat%", "<img src=" + ItemTable.getInstance().getTemplate(team_material).getIcon() + " width=32 height=32>")
            return htmltext

        ## RECHECK PARAMETERS
        if event == "recheck":
            CHECKITEM = st.getPlayer().getInventory().getPaperdollItemId(ASLOT)
            global CHECKITEMID
            CHECKITEMID = st.getPlayer().getInventory().getPaperdollItem(ASLOT).getObjectId()
            if CHECKITEM == 0:
                htmltext = st.showHtmlFile("security.htm").replace("%TEXTACC%", st.getPlayer().getAccountName()).\
                                                           replace("%TEXTCHAR%", st.getPlayer().getName())
                return htmltext
            CHECKCURENC = st.getPlayer().getInventory().getPaperdollItem(ASLOT).getEnchantLevel()
            CHECKSOLOFUNDS = st.getQuestItemsCount(solo_material)
            CHECKTEAMFUNDS = st.getQuestItemsCount(team_material)
            CHECKPVPFUNDS = PcModel.getModel(st.getPlayer()).spartanInstance.getSoloPoints()
            if CHECKITEM == 0 or CHECKITEM != ITEM or CHECKCURENC != CURENC:
                htmltext = st.showHtmlFile("security.htm").replace("%TEXTACC%", st.getPlayer().getAccountName()).\
                                                           replace("%TEXTCHAR%", st.getPlayer().getName())
                return htmltext
            elif CHECKCURENC >= MAXENC:
                htmltext = "errorenc.htm"
                return htmltext
            elif CHECKSOLOFUNDS < SOLOCOST or CHECKTEAMFUNDS < TEAMCOST or CHECKPVPFUNDS < PVPCOST:
                htmltext = "errorfunds.htm"
                return htmltext
            else:
                return self.onAdvEvent("enchant", npc, player)

        ## ENCHANT
        if event == "enchant":
                st.takeItems(solo_material, SOLOCOST)
                st.takeItems(team_material, TEAMCOST)
                CHECKITEMID2 = st.getPlayer().getInventory().getPaperdollItem(ASLOT).getObjectId()
                if(CHECKITEMID2 == CHECKITEMID):
                    st.getPlayer().getInventory().getPaperdollItem(ASLOT).setEnchantLevel(ENCHANT)
                    st.getPlayer().broadcastUserInfo()
                return self.onAdvEvent(EVENT, npc, player)


# FOOTER
QUEST = Quest(QUESTID, str(QUESTID) + "_" + QUESTTITLE, QUESTDESC)

for QUESTNPCID in QUESTNPC:
    QUEST.addStartNpc(QUESTNPCID)
    QUEST.addTalkId(QUESTNPCID)
    QUEST.addFirstTalkId(QUESTNPCID)
