import sys
from java.util import Iterator
from com.l2jserver.gameserver.model.quest import State
from com.l2jserver.gameserver.model.quest import QuestState
from com.l2jserver.gameserver.model.quest.jython import QuestJython as JQuest
from com.l2jserver.gameserver.datatables import ItemTable

SERVER_NAME = "http://titangracia.site11.com"
NPC=440457
QUEST_DATA = [8204,"NoblesseManager","custom"]
ITEMS = [3470,1,7694,1]
QN = str(QUEST_DATA[0]) + "_" + QUEST_DATA[1]

class NoblesseManager (JQuest) :

    def __init__(self,id,name,descr):
        JQuest.__init__(self,id,name,descr)

    def onAdvEvent (self,event,npc,player):
        st = player.getQuestState(QN)
        htmltext = event

        def html(TEXT) :
            return "<html><head><title>" + SERVER_NAME + "</title></head><body><center><img src=\"L2UI_CH3.herotower_deco\" width=256 height=32><br>" + TEXT + "</center></body></html>"
        
        if event == "make_noblesse":
            if player.getLevel() < 80 :
                return html("Your level is too low!")

            elif player.isNoble() == 1 :
                return html("You are already a noblesse!")

            elif st.getQuestItemsCount(ITEMS[0]) < ITEMS[1] :
                return html("You don't have enough <font color=\"LEVEL\">" + ItemTable.getInstance().getTemplate(int(ITEMS[0])).getName() + " </font>!")

            else:
                st.takeItems(ITEMS[0],ITEMS[1])
                player.setNoble(True)
                st.giveItems(ITEMS[2],ITEMS[3])
                st.playSound("ItemSound.quest_finish")
                return html("You are now a noblesse!<br>Congratulations!")

    def onFirstTalk (self,npc,player):
        st = player.getQuestState(QN)
        if not st :
            st = self.newQuestState(player)
        s = "<html><head><title>"+SERVER_NAME+"</title></head><body><center><img src=\"L2UI_CH3.herotower_deco\" width=256 height=32><br>"
        s += "I'm here to help you to become a noblesse!<br>All you need is <font color=\"LEVEL\">" + str(ITEMS[1]) + " " + ItemTable.getInstance().getTemplate(int(ITEMS[0])).getName() + "</font>!<br>"
        s += "<button value=\"Make me noblesse\" action=\"bypass -h Quest "+QN+" make_noblesse\" width=200 height=25 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\">"
        s += "</center></body></html>"
        return s

QUEST = NoblesseManager(QUEST_DATA[0],QN,QUEST_DATA[2])
QUEST.addStartNpc(NPC)
QUEST.addFirstTalkId(NPC)
QUEST.addTalkId(NPC)
